
$(document).ready(function () {

    $.ajaxSetup({
	    headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf_token_name"]').attr('content')
	    }
    });
    
    tableID = $("#data-table-book");

    var dataTableBook = tableID.DataTable({
		lengthMenu: [10, 20, 50, 100],
		pageLength: 10,
		language: {
			'lengthMenu': 'Display _MENU_',
		},
		searchDelay: 500,
        responsive: true,
        autoWidth: false,
        serverSide : true,
        processing: true,
        order: [[1, 'asc']],
        columnDefs: [{
            orderable: false,
            targets: [0,5]
        },{ responsivePriority: 3, targets: -1 }],

        ajax : {
            url: base_url + "/blog/datatables",
            method : 'POST'
        }
    });

    dataTableBook.on('draw.dt', function() {
        var PageInfo = tableID.DataTable().page.info();
        dataTableBook.column(0, {
            page: 'current'
        }).nodes().each(function(cell, i) {
            cell.innerHTML = i + 1 + PageInfo.start;
        });
    });

    $(document).on('click', '#btn-save-book', function () {
        $('.text-danger').remove();
        $('.is-invalid').removeClass('is-invalid');
        var createForm = $('#form-create-book');
        
        $.ajax({
            url: base_url + '/blog/resources',
            method: 'POST',
            data: createForm.serialize()
        }).done((data, textStatus) => {
            Toast.fire({
                icon: 'success',
                title: textStatus
            })
            dataTableBook.ajax.reload();
            $("#form-create-book").trigger("reset");
            $("#modal-create-book").modal('hide');

        }).fail((xhr, status, error) => {
            if (xhr.responseJSON.message) {
                Toast.fire({
                    icon: 'error',
                    title: xhr.responseJSON.message,
                });
            }

            $.each(xhr.responseJSON.messages, (elem, messages) => {
                createForm.find('select[name="' + elem + '"]').after('<p class="text-danger">' + messages + '</p>');
                createForm.find('input[name="' + elem + '"]').addClass('is-invalid').after('<p class="text-danger">' + messages + '</p>');
                createForm.find('textarea[name="' + elem + '"]').addClass('is-invalid').after('<p class="text-danger">' + messages + '</p>');
            });
        })
    })

    $(document).on('click', '.btn-edit', function (e) {
        e.preventDefault();
        $.ajax({
            url: base_url + "/blog/resources/" + $(this).attr('data-id') + "/edit",
            method: 'GET',
            
        }).done((response) => {
            var editForm = $('#form-edit-book');
            editForm.find('input[name="title"]').val(response.data.title);
            editForm.find('input[name="author"]').val(response.data.author);
            editForm.find('textarea[name="description"]').val(response.data.description);
            editForm.find('select[name="status_id"]').val(response.data.status_id);
            $("#book_id").val(response.data.id);
            $("#modal-edit-book").modal('show');
        }).fail((error) => {
            Toast.fire({
                icon: 'error',
                title: error.responseJSON.messages.error,
            });
        })
    });

    $(document).on('click', '#btn-update-book', function (e) {
        e.preventDefault();
        $('.text-danger').remove();
        var editForm = $('#form-edit-book');

        $.ajax({
            url: base_url + "/blog/resources/"+$('#book_id').val(),
            method: 'PUT',
            data: editForm.serialize()
            
        }).done((data, textStatus) => {
            Toast.fire({
                icon: 'success',
                title: textStatus
            })
            dataTableBook.ajax.reload();
            $("#form-edit-book").trigger("reset");
            $("#modal-edit-book").modal('hide');

        }).fail((xhr, status, error) => {
            $.each(xhr.responseJSON.messages, (elem, messages) => {
                editForm.find('select[name="' + elem + '"]').after('<p class="text-danger">' + messages + '</p>');
                editForm.find('input[name="' + elem + '"]').addClass('is-invalid').after('<p class="text-danger">' + messages + '</p>');
                editForm.find('textarea[name="' + elem + '"]').addClass('is-invalid').after('<p class="text-danger">' + messages + '</p>');
            });
        })
    });

    $(document).on('click', '.btn-delete', function (e) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        })
        .then((result) => {
            if (result.value) {
                $.ajax({
                    url: base_url + "/blog/resources/"+$(this).attr('data-id'),
                    method: 'DELETE',
                }).done((data, textStatus) => {
                    Toast.fire({
                        icon: 'success',
                        title: textStatus,
                    });
                    dataTableBook.ajax.reload();
                }).fail((error) => {
                    Toast.fire({
                        icon: 'error',
                        title: error.responseJSON.messages.error,
                    });
                })
            }
        })
    });

    $('#modal-create-book').on('hidden.bs.modal', function() {
        $(this).find('#form-create-book')[0].reset();
        $('.text-danger').remove();
        $('.is-invalid').removeClass('is-invalid');
    });

    $('#modal-create-book-test').on('hidden.bs.modal', function() {
        $(this).find('#form-create-book-test')[0].reset();
        $('.text-danger').remove();
        $('.is-invalid').removeClass('is-invalid');
    });

    $('#modal-edit-book').on('hidden.bs.modal', function() {
        $(this).find('#form-edit-permission')[0].reset();
        $('.text-danger').remove();
        $('.is-invalid').removeClass('is-invalid');
    });

    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        onOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })
});