
var loadData = base_url + "/menu/data",
tableID = $("#data-table-book"),
Page = function () {
      return {
         init: function () {
            Page.main();
            Page.tableAjax();
            
         },
         tableAjax: function () {
            $.ajaxSetup({
                  headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf_token_name"]').attr('content')
                  }
            });

            //datatables
            tableID.DataTable({
                  lengthMenu: [10, 20, 50, 100],
                  pageLength: 10,
                  language: {
                     'lengthMenu': 'Display _MENU_',
                  },
                  searchDelay: 500,
                  responsive: true,
                  autoWidth: false,
                  serverSide : true,
                  processing: true,
                  order: [[1, 'asc']],
                  columnDefs: [{
                     orderable: false,
                     targets: [0]
                  },{ responsivePriority: 3, targets: -1 }],

                  ajax : {
                     url: loadData,
                     method : 'POST'
                  }

            });


            
         },
         main: function () {
            
            $("#modal-create-book").on("hidden.bs.modal", function() {
                  $(this).find("#form-create-book")[0].reset();
                  $(".text-danger").remove();
                  $(".is-invalid").removeClass("is-invalid");
            });

            $(document).on("click", "#create", function () {
                  Page.add()
            });

            $(document).on("submit", "#form-add", function () {
                  return Page.submitForm($("#form-add"), base_url + "/menu/addnew/"), !1
            });

            $(document).on("click", ".btn-edit", function () {
                  var e = $(this).attr("data-id");
                  Page.edit(e)
            });

            $(document).on("submit", "#form-edit", function () {
                  var id = $("#book_id").val();
                  return Page.submitForm($("#form-edit"), base_url + "/menu/updated/"), !1
            });

            $(document).on("click", ".btn-delete", function (e) {
                  Swal.fire({
                     title: "Are you sure?",
                     text: "You won't be able to revert this!",
                     icon: "warning",
                     showCancelButton: true,
                     confirmButtonColor: "#3085d6",
                     cancelButtonColor: "#d33",
                     confirmButtonText: "Yes, delete it!"
                  })
                  .then((result) => {
                     if (result.value) {
                        $.ajax({
                              url: base_url + "/menu/resources/"+$(this).attr("data-id"),
                              method: "DELETE",
                        }).done((data, textStatus) => {
                              swal.fire({
                                 icon: "success",
                                 title: textStatus,
                              });
                              tableID.DataTable().ajax.reload();
                        }).fail((error) => {
                              swal.fire({
                                 icon: "error",
                                 title: error.responseJSON.messages.error,
                              });
                        })
                     }
                  })
            });
         },
         add: function () {
            var a = Helper.loadModal("lg"),
                  i = a.find(".modal-body"),
                  t = a.find(".modal-title");
            t.text("Add Data"), 
            Helper.blockElement($(i)), 
            $.ajax({
                  url: base_url + "/menu/add",
                  method: "GET",
            })
            .done(function (e) {
                  i.html(e);
                    $('#convert_example_1').iconpicker({
                        arrowClass: 'btn-danger',
                        arrowPrevIconClass: 'fas fa-angle-left',
                        arrowNextIconClass: 'fas fa-angle-right',
                        cols: 8,
                        footer: true,
                        header: true,
                        icon: 'fas fa-bomb',
                        iconset: 'fontawesome5',
                        labelHeader: '{0} of {1} pages',
                        labelFooter: '{0} - {1} of {2} icons',
                        placement: 'bottom',
                        rows: 3,
                        search: true,
                        searchText: 'Search',
                        selectedClass: 'btn-success',
                        unselectedClass: ''
                    }).on('change', function(e) {
                        $("#console").val(e.icon);
                    });
                  Helper.unblockElement($(i));
            })
         },
         edit: function (e) {
            var a = Helper.loadModal("lg"),
                  i = a.find(".modal-body"),
                  t = a.find(".modal-title");
            t.text("Edit Data"), 
            Helper.blockElement($(i)), 
            $.ajax({
                  url: base_url + "/menu/edit",
                  method: "GET",
                  data: {id:e}, 
            })
            .done(function (e) {
                  i.html(e);
                  $('#convert_example_1').iconpicker({
                        arrowClass: 'btn-danger',
                        arrowPrevIconClass: 'fas fa-angle-left',
                        arrowNextIconClass: 'fas fa-angle-right',
                        cols: 8,
                        footer: true,
                        header: true,
                        icon: 'fas fa-bomb',
                        iconset: 'fontawesome5',
                        labelHeader: '{0} of {1} pages',
                        labelFooter: '{0} - {1} of {2} icons',
                        placement: 'bottom',
                        rows: 3,
                        search: true,
                        searchText: 'Search',
                        selectedClass: 'btn-success',
                        unselectedClass: ''
                    }).on('change', function(e) {
                        $("#console").val(e.icon);
                    });
                  Helper.unblockElement($(i));
            })
         },
         submitForm: function (e, url) {
            var i = e.find(".submit");
            $.ajax({
                  url: url,
                  type: "POST", //form method
                  data: e.serialize()
            }).done((data, textStatus) => {
                  swal.fire({
                     icon: "success",
                     title: textStatus
                  })
                  $(".closed").click();
                  $(tableID).DataTable().ajax.reload();
                  e.trigger("reset");
                  Helper.blockElement(e.parent()), i.attr("disabled", !0);
            }).fail((xhr, status, error) => {
                  swal.fire({
                     icon: "warning",
                     title: xhr.responseJSON.messages
                  });
            });
         }
      }
}();