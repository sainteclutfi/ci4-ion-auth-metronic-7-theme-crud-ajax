var Helper = function () {
    return {
        baseUrl: function (a) {
            return window.base_url + a
        },
        ajax: function (url, type, datatype, n) {
            return $.ajax({
                // async: true,
                // cache: false,
                url: url,
                method: type,
                // datatype: datatype,
                // data: n,
                error: function (xhr) {
                    // swal.fire(xhr.status.toString(), xhr.statusText, "error");
                    // KTApp.unblock('#kt_content');
                    // KTApp.unblock('#container');
                    Helper.unblockElement('#kt_content');
                    Helper.unblockElement('.container');
                    $('.modal-header').find(".close").click();
                }
            })
        },
        ajaxFile: function (a, e, t, n) {
            return $.ajax({
                url: a,
                type: e,
                datatype: t,
                enctype: 'multipart/form-data',
                data: n,
                error: function (a) {
                    swal(a.status.toString(), a.statusText, 'error');
                },
                contentType: false,
                processData: false
            });
        },
        ajaxdropdown: function ($element, url, data, defaultvalue) {
            $.ajax({
                url: window.base_url + url,
                type: "POST",
                dataType: "JSON",
                data: data,
                success: function (data) {
                    var html = '';
                    var i;
                    for (i = 0; i < data.rows.length; i++) {
                        html += '<option value="' + data.rows[i].id + '">' + data.rows[i].text + '</option>';
                    }
                    $element.html(html);
                    if (defaultvalue) {
                        $element.val(defaultvalue);
                        $element.trigger('change');
                    }
                }
            });
        },
        serializeForm: function (a) {
            var e = a.serializeArray(),
                t = a.find(".submit"),
                n = t.attr("name"),
                o = t.val();
            return n && o && e.push({
                name: n,
                value: o
            }), e
        },
        loadModal: function (a) {
            var e = "modal-" + (new Date).getTime(),
                t = '<div id="' + e + '" class="modal fade" tabindex="-1" data-backdrop="static" role="dialog" aria-labelledby="exampleModalCenterTitle" padding-right: 15px; aria-modal="true" style="display: block;">\
                        <div class="modal-dialog modal-dialog-centered modal-' + a + '" role="document">\
                            <div class="modal-content">\
                                <div class="modal-header">\
                                    <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>\
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">\
																			<i aria-hidden="true" class="ki ki-close"></i>\
																		</button>\
                                </div>\
                                <div class="modal-body">\
                                </div>\
                            </div>\
                        </div>\
                    </div>';
            return $("body").append(t),
                $("#" + e).modal("show").on("hidden.bs.modal", function (a) {
                    $(this).remove()
                }), $("#" + e);
        },
        blockElement: function (a) {
            KTApp.block(a, {
                overlayColor: '#000000',
                type: 'v2',
                state: 'primary',
                message: 'Please wait...'
            });
        },
        unblockElement: function (a) {
            KTApp.unblock(a);
        },
        selectField: function (a, e) {
            $.fn.select2.defaults.set("theme", "bootstrap"), $(a).select2({
                placeholder: e,
                allowClear: !0,
                width: null
            })
        },
        validateForm: function (a, e) {
            var t = $(".alert-danger", a),
                n = $(".alert-success", a);
            return $.extend($.validator.messages, {
                required: "Tidak boleh kosong.",
                remote: "Silahkan periksa kembali.",
                email: "Masukkan alamat email yang valid.",
                url: "Masukkan alamat url yang valid.",
                date: "Masukkan format tanggal yang valid.",
                dateISO: "Please enter a valid date (ISO).",
                number: "Masukkan nomor yang benar.",
                digits: "Please enter only digits.",
                creditcard: "Please enter a valid credit card number.",
                equalTo: "Masukkan kembali nilai yang sama.",
                accept: "Please enter a value with a valid extension.",
                maxlength: $.validator.format("Maksimal {0} karakter."),
                minlength: $.validator.format("Minimal {0} karakter."),
                rangelength: $.validator.format("Please enter a value between {0} and {1} characters long."),
                range: $.validator.format("Please enter a value between {0} and {1}."),
                max: $.validator.format("Tidak boleh lebih dari {0}."),
                min: $.validator.format("Tidak boleh kurang dari {0}.")
            }), a.validate({
                errorElement: "span",
                errorClass: "error invalid-feedback",
                focusInvalid: !0,
                ignore: "",
                rules: e,
                invalidHandler: function (a, e) {
                    // n.hide(), t.show(), App.scrollTo(t, -200)
                    swal.fire({
                        "title": "",
                        "text": "There are some errors in your submission. Please correct them.",
                        "type": "error",
                        "confirmButtonClass": "btn btn-secondary",
                        "onClose": function (e) {
                            // console.log('on close event fired!');
                        }
                    });
                    event.preventDefault();
                },
                errorPlacement: function (a, e) {
                    e.is(":checkbox") ? a.insertAfter(e.closest(".md-checkbox-list, .md-checkbox-inline, .checkbox-list, .checkbox-inline")) : e.is(":radio") ? a.insertAfter(e.closest(".md-radio-list, .md-radio-inline, .radio-list, .radio-inline")) : a.insertAfter(e);

                },
                highlight: function (a) {
                    $(a).closest(".form-group").addClass("validate is-invalid")
                    $(a).closest(".form-control").addClass("is-invalid")
                },
                unhighlight: function (a) {
                    $(a).closest(".form-group").removeClass("validate is-invalid")
                    $(a).closest(".form-control").removeClass("is-invalid")
                },
                success: function (a) {
                    a.closest(".form-group").removeClass("validate is-invalid")
                    a.closest(".form-control").removeClass("is-invalid")
                }
            })
        },
        currencyInput: function () {
            $(".money").autoNumeric("init", {
                aSep: ".",
                aDec: ",",
                aSign: "",
                wEmpty: "zero",
                mDec: "0"
            });
        },
        datePicker: function (a) {
            var arrows;
            if (KTUtil.isRTL()) {
                arrows = {
                    leftArrow: '<i class="la la-angle-right"></i>',
                    rightArrow: '<i class="la la-angle-left"></i>'
                }
            } else {
                arrows = {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            }
            // minimum setup for modal demo
            $(a).datepicker({
                rtl: KTUtil.isRTL(),
                todayHighlight: true,
                orientation: "bottom left",
                templates: arrows,
                autoclose: true
            });

        }
    }
}();