<?= $this->extend('layout/main') ?>

<?= $this->section('content') ?>
    <?php echo view('Modules\Blog\Views\parts\modals'); ?>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
					<h3 class="card-title"> <?php echo lang('Auth.index_heading');?></h3>
					<p><?php echo lang('Auth.index_subheading');?></p>
					<div class="fv-plugins-message-container">
						<div class="fv-help-block"><?php echo $message;?></div>
					</div>
                    <div class="float-right">
                        <!-- <div class="btn-group"> -->
							<?php echo anchor('auth/create_user', lang('Auth.index_create_user_link'),['class' => 'btn btn-sm btn-block btn-info'])?>
							<?php echo anchor('auth/create_group', lang('Auth.index_create_group_link'),['class' => 'btn btn-sm btn-block btn-primary'])?>
                        <!-- </div> -->
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="dataTables_wrapper dt-bootstrap4">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="table-responsive">
									<table class="table table-striped dataTable" cellpadding=0 cellspacing=10>
										<tr>
											<th><?php echo lang('Auth.index_fname_th');?></th>
											<th><?php echo lang('Auth.index_lname_th');?></th>
											<th><?php echo lang('Auth.index_email_th');?></th>
											<th><?php echo lang('Auth.index_groups_th');?></th>
											<th><?php echo lang('Auth.index_status_th');?></th>
											<th><?php echo lang('Auth.index_action_th');?></th>
										</tr>
										<?php foreach ($users as $user):?>
											<tr>
												<td><?php echo htmlspecialchars($user->first_name,ENT_QUOTES,'UTF-8');?></td>
												<td><?php echo htmlspecialchars($user->last_name,ENT_QUOTES,'UTF-8');?></td>
												<td><?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?></td>
												<td>
													<?php foreach ($user->groups as $group):?>
														<?php echo anchor('auth/edit_group/' . $group->id, htmlspecialchars($group->name, ENT_QUOTES, 'UTF-8')); ?><br />
													<?php endforeach?>
												</td>
												<td><?php echo ($user->active) ? anchor('auth/deactivate/' . $user->id, lang('Auth.index_active_link')) : anchor("auth/activate/". $user->id, lang('Auth.index_inactive_link'));?></td>
												<td><?php echo anchor('auth/edit_user/' . $user->id, lang('Auth.index_edit_link')) ;?></td>
											</tr>
										<?php endforeach;?>
									</table>

									

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
        <!-- /.col -->
    </div>
<!-- /.row -->
<?= $this->endSection() ?>

<!--begin::Page Vendors Styles(used by this page)-->
<?= $this->section('page-vendors-styles-js') ?>
<link href="<?= base_url() ?>/theme/assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
<?= $this->endSection() ?>

<!--begin::Page Vendors(used by this page)-->
<?= $this->section('page-vendors-js') ?>
<script src="<?= base_url() ?>/theme/assets/plugins/custom/datatables/datatables.bundle.js"></script>
<?= $this->endSection() ?>

<!--begin::Page Scripts(used by this page)-->
<?= $this->section('extra-js') ?>
<script src="<?= base_url() ?>/theme/assets/js/pages/crud/datatables/search-options/advanced-search.js"></script>
<script src="<?= base_url() ?>/js/blog/blog.js"></script>
<?= $this->endSection() ?>