<?php
namespace IonAuth\Database\Migrations;

/**
 * CodeIgniter IonAuth
 *
 * @package CodeIgniter-Ion-Auth
 * @author  Benoit VRIGNAUD <benoit.vrignaud@zaclys.net>
 * @license https://opensource.org/licenses/MIT	MIT License
 * @link    http://github.com/benedmunds/CodeIgniter-Ion-Auth
 */

/**
 * Migration class
 *
 * @package CodeIgniter-Ion-Auth
 */
class Migration_Install_ion_auth extends \CodeIgniter\Database\Migration
{
	/**
	 * Tables
	 *
	 * @var array
	 */
	private $tables;

	/**
	 * Construct
	 *
	 * @return void
	 */
	public function __construct()
	{
		$config = config('IonAuth');

		// initialize the database
		$this->DBGroup = empty($config->databaseGroupName) ? '' : $config->databaseGroupName;

		parent::__construct();

		$this->tables = $config->tables;
	}

	/**
	 * Up
	 *
	 * @return void
	 */
	public function up()
	{
		// Drop table 'groups' if it exists
		$this->forge->dropTable($this->tables['groups'], true);

		// Table structure for table 'groups'
		$this->forge->addField([
			'id' => [
				'type'           => 'MEDIUMINT',
				'constraint'     => '8',
				'unsigned'       => true,
				'auto_increment' => true,
			],
			'name' => [
				'type'       => 'VARCHAR',
				'constraint' => '20',
			],
			'description' => [
				'type'       => 'VARCHAR',
				'constraint' => '100',
			],
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable($this->tables['groups']);

		// Drop table 'users' if it exists
		$this->forge->dropTable($this->tables['users'], true);

		// Table structure for table 'users'
		$this->forge->addField([
			'id' => [
				'type'           => 'MEDIUMINT',
				'constraint'     => '8',
				'unsigned'       => true,
				'auto_increment' => true,
			],
			'ip_address' => [
				'type'       => 'VARCHAR',
				'constraint' => '45',
			],
			'username' => [
				'type'       => 'VARCHAR',
				'constraint' => '100',
			],
			'password' => [
				'type'       => 'VARCHAR',
				'constraint' => '80',
			],
			'email' => [
				'type'       => 'VARCHAR',
				'constraint' => '254',
				'unique'     => true,
			],
			'activation_selector' => [
				'type'       => 'VARCHAR',
				'constraint' => '255',
				'null'       => true,
				'unique'     => true,
			],
			'activation_code' => [
				'type'       => 'VARCHAR',
				'constraint' => '255',
				'null'       => true,
			],
			'forgotten_password_selector' => [
				'type'       => 'VARCHAR',
				'constraint' => '255',
				'null'       => true,
				'unique'     => true,
			],
			'forgotten_password_code' => [
				'type'       => 'VARCHAR',
				'constraint' => '255',
				'null'       => true,
			],
			'forgotten_password_time' => [
				'type'       => 'INT',
				'constraint' => '11',
				'unsigned'   => true,
				'null'       => true,
			],
			'remember_selector' => [
				'type'       => 'VARCHAR',
				'constraint' => '255',
				'null'       => true,
				'unique'     => true,
			],
			'remember_code' => [
				'type'       => 'VARCHAR',
				'constraint' => '255',
				'null'       => true,
			],
			'created_on' => [
				'type'       => 'INT',
				'constraint' => '11',
				'unsigned'   => true,
			],
			'last_login' => [
				'type'       => 'INT',
				'constraint' => '11',
				'unsigned'   => true,
				'null'       => true,
			],
			'active' => [
				'type'       => 'TINYINT',
				'constraint' => '1',
				'unsigned'   => true,
				'null'       => true,
			],
			'first_name' => [
				'type'       => 'VARCHAR',
				'constraint' => '50',
				'null'       => true,
			],
			'last_name' => [
				'type'       => 'VARCHAR',
				'constraint' => '50',
				'null'       => true,
			],
			'company' => [
				'type'       => 'VARCHAR',
				'constraint' => '100',
				'null'       => true,
			],
			'phone' => [
				'type'       => 'VARCHAR',
				'constraint' => '20',
				'null'       => true,
			],

			'EmployeeNo' => [
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			],
			'Salutation' => [
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			],
			// 'FirstName' => [
			// 	'type' => 'VARCHAR',
			// 	'constraint' => 100,
			// 	'null' => TRUE,

			// ],
			// 'MiddleName' => [
			// 	'type' => 'VARCHAR',
			// 	'constraint' => 100,
			// 	'null' => TRUE,

			// ],
			// 'LastName' => [
			// 	'type' => 'VARCHAR',
			// 	'constraint' => 100,
			// 	'null' => TRUE,

			// ],
			'NickName' => [
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			],
			'Initial' => [
				'type' => 'VARCHAR',
				'constraint' => 3,
				'null' => TRUE,

			],
			'PreTitle' => [
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			],
			'PostTitle' => [
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			],
			'ClothingSize' => [
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			],
			'Height' => [
				'type' => 'DOUBLE',
				'null' => TRUE,

			],
			'Weight' => [
				'type' => 'DOUBLE',
				'null' => TRUE,

			],
			'PassportNo' => [
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			],
			'IssuedAt' => [
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			],
			'ExpiryDate' => [
				'type' => 'DATETIME',
				'null' => TRUE,

			],
			'IdentificationNo' => [
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			],
			'IdentificationExpiryDate' => [
				'type' => 'DATETIME',
				'null' => TRUE,

			],
			'TaxRegistrationNo' => [
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			],
			'PlaceOfBirth' => [
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			],
			'DateOfBirth' => [
				'type' => 'DATETIME',
				'null' => TRUE,

			],
			'Gender' => [
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			],
			'Ethnic' => [
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			],
			'BloodType' => [
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			],
			'MaritalStatus' => [
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			],
			'MaritalDate' => [
				'type' => 'DATETIME',
				'null' => TRUE,

			],
			'NoOfChildren' => [
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,

			],
			'StatusOnTax' => [
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			],
			'DrivingLicenseNo' => [
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			],
			'DrivingLicenseType' => [
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			],
			'Hobby' => [
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			],
			'Nationality' => [
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			],
			'Religion' => [
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			],
			'Address1' => [
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			],
			'PhoneNo1' => [
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			],
			'Address2' => [
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			],
			'PhoneNo2' => [
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			],
			'MobileNo1' => [
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			],
			'MobileNo2' => [
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			],
			'Email1' => [
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			],
			'Email2' => [
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			],
			'MotherName' => [
				'type' => 'VARCHAR',
				'constraint' => 50,
				'null' => TRUE,

			],
			'EmergencyContactName1' => [
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			],
			'EmergencyContactRelation1' => [
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			],
			'EmergencyAddress1' => [
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			],
			'EmergencyPhoneNo1' => [
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			],
			'EmergencyContactName2' => [
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			],
			'EmergencyContactRelation2' => [
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			],
			'EmergencyAddress2' => [
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			],
			'EmergencyPhoneNo2' => [
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			],
			'JoinedDate' => [
				'type' => 'DATETIME',
				'null' => TRUE,

			],
			'ConfirmationDate' => [
				'type' => 'DATETIME',
				'null' => TRUE,

			],
			'EmploymentPeriod' => [
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			],
			'ResignDate' => [
				'type' => 'DATETIME',
				'null' => TRUE,

			],
			'LastPromotionDate' => [
				'type' => 'DATETIME',
				'null' => TRUE,

			],
			'SalaryPaymentMethod' => [
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			],
			'TaxCalculationMode' => [
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			],
			'OrganizationalUnit' => [
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			],
			'Branch' => [
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			],
			'WorkStatus' => [
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			],
			'Workgroup' => [
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			],
			'Grade' => [
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			],
			'NonEmployee' => [
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			],
			'AutomaticOvertime' => [
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			],
			'PhoneExt' => [
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			],
			'PaymentPeriod' => [
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			],
			'EmployeeEducation' => [
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			],
			'Qualification' => [
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			],
			'GPA' => [
				'type' => 'VARCHAR',
				'constraint' => 10,
				'null' => TRUE,

			],
			'BankAccountNo' => [
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			],
			'AccountOwner' => [
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			],
			'Photo' => [
				'type' => 'LONGBLOB',
				'null' => TRUE,

			],
			'PrivateKey' => [
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			],
			'IsActive' => [
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			],
			'UserName' => [
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			],
			'ChangePasswordOnFirstLogon' => [
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			],
			'StoredPassword' => [
				'type' => 'LONGTEXT',
				'null' => TRUE,

			],
			'SalesForce' => [
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			],
			'Technician' => [
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			],
			'Sequence' => [
				'type' => 'BIGINT',
				'constraint' => 20,
				'null' => TRUE,

			],
			'EnrollNo' => [
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			],
			'OptimisticLockField' => [
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			],
			'GCRecord' => [
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			],
			'BankName' => [
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			],
			'PersonalGrade' => [
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			],
			'Homebase' => [
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			],
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable($this->tables['users'], false);

		// Drop table 'users_groups' if it exists
		$this->forge->dropTable($this->tables['users_groups'], true);

		// Table structure for table 'users_groups'
		$this->forge->addField([
			'id' => [
				'type'           => 'MEDIUMINT',
				'constraint'     => '8',
				'unsigned'       => true,
				'auto_increment' => true,
			],
			'user_id' => [
				'type'       => 'MEDIUMINT',
				'constraint' => '8',
				'unsigned'   => true,
			],
			'group_id' => [
				'type'       => 'MEDIUMINT',
				'constraint' => '8',
				'unsigned'   => true,
			],
		]);
		$this->forge->addKey('id', true);

		$this->forge->addForeignKey('user_id', $this->tables['users'], 'id', 'NO ACTION', 'CASCADE');
		$this->forge->addForeignKey('group_id', $this->tables['groups'], 'id', 'NO ACTION', 'CASCADE');

		$this->forge->createTable($this->tables['users_groups']);

		// Drop table 'login_attempts' if it exists
		$this->forge->dropTable($this->tables['login_attempts'], true);

		// Table structure for table 'login_attempts'
		$this->forge->addField([
			'id' => [
				'type'           => 'MEDIUMINT',
				'constraint'     => '8',
				'unsigned'       => true,
				'auto_increment' => true,
			],
			'ip_address' => [
				'type'       => 'VARCHAR',
				'constraint' => '45',
			],
			'login' => [
				'type'       => 'VARCHAR',
				'constraint' => '100',
				'null'       => true,
			],
			'time' => [
				'type'       => 'INT',
				'constraint' => '11',
				'unsigned'   => true,
				'null'       => true,
			]
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable($this->tables['login_attempts']);
	}

	/**
	 * Down
	 *
	 * @return void
	 */
	public function down()
	{
		$this->forge->dropTable($this->tables['users'], true);
		$this->forge->dropTable($this->tables['groups'], true);
		$this->forge->dropTable($this->tables['users_groups'], true);
		$this->forge->dropTable($this->tables['login_attempts'], true);
	}
}
