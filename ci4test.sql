/*
 Navicat Premium Data Transfer

 Source Server         : lokal
 Source Server Type    : MySQL
 Source Server Version : 50719
 Source Host           : localhost:3306
 Source Schema         : ci4test

 Target Server Type    : MySQL
 Target Server Version : 50719
 File Encoding         : 65001

 Date: 09/04/2021 09:35:12
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for blogs
-- ----------------------------
DROP TABLE IF EXISTS `blogs`;
CREATE TABLE `blogs`  (
  `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `status_id` int(5) UNSIGNED NULL DEFAULT NULL,
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `author` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'King of Town',
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `status_id`(`status_id`) USING BTREE,
  CONSTRAINT `blogs_status_id_foreign` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of blogs
-- ----------------------------

-- ----------------------------
-- Table structure for common_bank
-- ----------------------------
DROP TABLE IF EXISTS `common_bank`;
CREATE TABLE `common_bank`  (
  `Oid` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `BankCode` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `BankName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `BranchName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `BankAddress` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PhoneNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Recommended` tinyint(4) NULL DEFAULT NULL,
  `OptimisticLockField` int(11) NULL DEFAULT NULL,
  `GCRecord` int(11) NULL DEFAULT NULL,
  `BranchCode` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Oid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of common_bank
-- ----------------------------

-- ----------------------------
-- Table structure for common_branch
-- ----------------------------
DROP TABLE IF EXISTS `common_branch`;
CREATE TABLE `common_branch`  (
  `BranchId` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `BranchName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ShortBranchName2` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `BranchType` int(11) NULL DEFAULT NULL,
  `LocationStatus` int(11) NULL DEFAULT NULL,
  `BranchAddress` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `PhoneNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PhoneNoSMSGateway` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `IsSPLD` tinyint(4) NULL DEFAULT NULL,
  `ReceptionLeadTime` double NULL DEFAULT NULL,
  `ServiceLeadTime` double NULL DEFAULT NULL,
  `PartPickingLeadTime` double NULL DEFAULT NULL,
  `FinalInspectionLeadTime` double NULL DEFAULT NULL,
  `DocumentCheckingLeadTime` double NULL DEFAULT NULL,
  `InvoicingLeadTime` double NULL DEFAULT NULL,
  `CallCustomerNotificationLeadTime` double NULL DEFAULT NULL,
  `WashingLeadTime` double NULL DEFAULT NULL,
  `DeliveryLeadTime` double NULL DEFAULT NULL,
  `ClockOnLeadTime` double NULL DEFAULT NULL,
  `ClockOffLeadTime` double NULL DEFAULT NULL,
  `DADDataRange` int(11) NULL DEFAULT NULL,
  `DeadStock` int(11) NULL DEFAULT NULL,
  `DemandFrequency` int(11) NULL DEFAULT NULL,
  `DemandDuration` int(11) NULL DEFAULT NULL,
  `StockPolicyPriceLimit` decimal(19, 0) NULL DEFAULT NULL,
  `ReturnDaysLimit` int(11) NULL DEFAULT NULL,
  `ReturnPriceLimit` decimal(19, 0) NULL DEFAULT NULL,
  `BinningLimitDays` int(11) NULL DEFAULT NULL,
  `Region` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `TaxCodeOnSales` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `TaxCodeOnPurchase` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `WithHoldingTaxCode` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `IsRootBranch` tinyint(4) NULL DEFAULT NULL,
  `DefaultPettyCashAccount` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CashierStartingAmount` decimal(19, 0) NULL DEFAULT NULL,
  `CashierBalanceClear` tinyint(4) NULL DEFAULT NULL,
  `DeadlinePeriodicService` int(11) NULL DEFAULT NULL,
  `MaximumReturnDownPayment` double NULL DEFAULT NULL,
  `ParentBranch` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `OptimisticLockField` int(11) NULL DEFAULT NULL,
  `GCRecord` int(11) NULL DEFAULT NULL,
  `DeliveryCode` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PartInTransitAccount` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ShortBranchNameDataTransfer` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ShortBranchNameDataTransferOracle` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ShortBranchName` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ToyotaBranchCode` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ToyotaBranchCodeAFI` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ToyotaAuthorized` tinyint(4) NULL DEFAULT NULL,
  `ParentToyotaBranchCode` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PrincipalAuthorized` tinyint(4) NULL DEFAULT NULL,
  `PrincipalBranchCode` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ParentPrincipalBranchCode` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DefaultLocation` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DefaultIntransitLocation` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `OrganizationalUnitTypeForTransferRequest` int(11) NULL DEFAULT NULL,
  `Province` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `City` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `MatriksDR` int(2) NULL DEFAULT NULL,
  `OutlateCode` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`BranchId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of common_branch
-- ----------------------------

-- ----------------------------
-- Table structure for common_city
-- ----------------------------
DROP TABLE IF EXISTS `common_city`;
CREATE TABLE `common_city`  (
  `Oid` int(11) NOT NULL,
  `CityType` int(11) NULL DEFAULT NULL,
  `CityName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Province` int(11) NULL DEFAULT NULL,
  `OptimisticLockField` int(11) NULL DEFAULT NULL,
  `GCRecord` int(11) NULL DEFAULT NULL,
  `rowguid` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Oid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of common_city
-- ----------------------------

-- ----------------------------
-- Table structure for common_country
-- ----------------------------
DROP TABLE IF EXISTS `common_country`;
CREATE TABLE `common_country`  (
  `Oid` varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `CountryName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PhoneCode` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `OptimisticLockField` int(11) NULL DEFAULT NULL,
  `GCRecord` int(11) NULL DEFAULT NULL,
  `rowguid` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`Oid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of common_country
-- ----------------------------

-- ----------------------------
-- Table structure for common_customer
-- ----------------------------
DROP TABLE IF EXISTS `common_customer`;
CREATE TABLE `common_customer`  (
  `Oid` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `Salutation` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FirstName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `LastName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CustomerId` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `TaxRegistrationNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Branch` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CustomerAddress` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `OfficeAddress` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `PhoneNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `MobileNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DateOfBirth` date NULL DEFAULT NULL,
  `PlaceOfBirth` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `IdentificationType` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `IdentificationNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Religion` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ContactSource` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Occupation` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CustomerType` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `GCRecord` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Gender` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `RetailFleet` enum('0','1') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Country` char(5) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Province` int(10) NULL DEFAULT NULL,
  `City` int(10) NULL DEFAULT NULL,
  `District` int(10) NULL DEFAULT NULL,
  `SubDistrict` int(10) NULL DEFAULT NULL,
  `PostalCode` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `RT` char(4) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `RW` char(4) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SIMCardNo` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `MarriedStatus` char(3) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `IncomePerMonth` decimal(11, 0) NULL DEFAULT NULL,
  `ExpensesPerMonth` decimal(11, 0) NULL DEFAULT NULL,
  `Hobby` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Referensi` enum('0','1') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FavoriteFood` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FavoriteDrink` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `BusinessFields` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `RegisteredDate` datetime(0) NULL DEFAULT NULL,
  `VehiclePhoneNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `VehicleRelationship` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `VehicleUsers` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `InActive` tinyint(1) NULL DEFAULT NULL,
  `ReasonActivation` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AppInActive` tinyint(2) NULL DEFAULT NULL,
  PRIMARY KEY (`Oid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of common_customer
-- ----------------------------

-- ----------------------------
-- Table structure for common_district
-- ----------------------------
DROP TABLE IF EXISTS `common_district`;
CREATE TABLE `common_district`  (
  `Oid` int(11) NOT NULL AUTO_INCREMENT,
  `DistrictName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `City` int(11) NULL DEFAULT NULL,
  `OptimisticLockField` int(11) NULL DEFAULT NULL,
  `GCRecord` int(11) NULL DEFAULT NULL,
  `rowguid` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`Oid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of common_district
-- ----------------------------

-- ----------------------------
-- Table structure for common_province
-- ----------------------------
DROP TABLE IF EXISTS `common_province`;
CREATE TABLE `common_province`  (
  `Oid` int(11) NOT NULL,
  `ProvinceName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Country` varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `OptimisticLockField` int(11) NULL DEFAULT NULL,
  `GCRecord` int(11) NULL DEFAULT NULL,
  `rowguid` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`Oid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of common_province
-- ----------------------------

-- ----------------------------
-- Table structure for depo_part
-- ----------------------------
DROP TABLE IF EXISTS `depo_part`;
CREATE TABLE `depo_part`  (
  `Oid` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `PartNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PartCode` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PartName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CostingMethod` int(11) NULL DEFAULT NULL,
  `RegisteredDate` datetime(0) NULL DEFAULT NULL,
  `SerialNoRequired` tinyint(4) NULL DEFAULT NULL,
  `PartGroup` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `OldPart` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NewPart` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Discontinue` tinyint(4) NULL DEFAULT NULL,
  `StopSalesCode` int(11) NULL DEFAULT NULL,
  `PartFlag` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Length` double NULL DEFAULT NULL,
  `Width` double NULL DEFAULT NULL,
  `Height` double NULL DEFAULT NULL,
  `Weight` double NULL DEFAULT NULL,
  `IsPWC` tinyint(4) NULL DEFAULT NULL,
  `MaximumSupply` int(11) NULL DEFAULT NULL,
  `VehicleModel` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `UnitPrice` decimal(19, 0) NULL DEFAULT NULL,
  `RetailPrice` decimal(19, 0) NULL DEFAULT NULL,
  `QuantityPerSet` int(11) NULL DEFAULT NULL,
  `Unit` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Program` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `WarrantyPeriod` int(11) NULL DEFAULT NULL,
  `MileageWarranty` int(11) NULL DEFAULT NULL,
  `OptimisticLockField` int(11) NULL DEFAULT NULL,
  `GCRecord` int(11) NULL DEFAULT NULL,
  `VehicleFullModel` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `MarginDiscount` decimal(19, 0) NULL DEFAULT NULL,
  `MaxRetailDiscount` decimal(19, 0) NULL DEFAULT NULL,
  `ModelCode` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `LandedCost` decimal(19, 0) NULL DEFAULT NULL,
  `PriceList` decimal(19, 0) NULL DEFAULT NULL,
  `AutoUpdatePrice` tinyint(4) NULL DEFAULT NULL,
  PRIMARY KEY (`Oid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of depo_part
-- ----------------------------

-- ----------------------------
-- Table structure for depo_stockcard
-- ----------------------------
DROP TABLE IF EXISTS `depo_stockcard`;
CREATE TABLE `depo_stockcard`  (
  `Oid` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `IdentificationNo` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ReferenceType` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ReferenceId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ReferenceNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `TransactionDate` datetime(0) NULL DEFAULT NULL,
  `Part` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Warehouse` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Branch` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `InQuantity` int(11) NULL DEFAULT NULL,
  `InUnitPrice` decimal(19, 0) NULL DEFAULT NULL,
  `OutQuantity` int(11) NULL DEFAULT NULL,
  `OutUnitPrice` decimal(19, 0) NULL DEFAULT NULL,
  `BalanceQuantity` int(11) NULL DEFAULT NULL,
  `BalanceStockValue` decimal(19, 0) NULL DEFAULT NULL,
  `PreviousStockCard` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `IsActive` tinyint(4) NULL DEFAULT NULL,
  `Marker` datetime(0) NULL DEFAULT NULL,
  `OptimisticLockField` int(11) NULL DEFAULT NULL,
  `GCRecord` int(11) NULL DEFAULT NULL,
  `Remark` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`Oid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of depo_stockcard
-- ----------------------------

-- ----------------------------
-- Table structure for groups
-- ----------------------------
DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups`  (
  `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of groups
-- ----------------------------
INSERT INTO `groups` VALUES (1, 'admin', 'Administrator');
INSERT INTO `groups` VALUES (2, 'members', 'General User');
INSERT INTO `groups` VALUES (3, 'tes', 'tes');
INSERT INTO `groups` VALUES (4, 'sa', 'sa');

-- ----------------------------
-- Table structure for hr_bloodtype
-- ----------------------------
DROP TABLE IF EXISTS `hr_bloodtype`;
CREATE TABLE `hr_bloodtype`  (
  `Oid` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `Description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `OptimisticLockField` int(11) NULL DEFAULT NULL,
  `GCRecord` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`Oid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hr_bloodtype
-- ----------------------------

-- ----------------------------
-- Table structure for hr_gender
-- ----------------------------
DROP TABLE IF EXISTS `hr_gender`;
CREATE TABLE `hr_gender`  (
  `Oid` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `Description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `OptimisticLockField` int(11) NULL DEFAULT NULL,
  `GCRecord` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`Oid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hr_gender
-- ----------------------------

-- ----------------------------
-- Table structure for hr_maritalstatus
-- ----------------------------
DROP TABLE IF EXISTS `hr_maritalstatus`;
CREATE TABLE `hr_maritalstatus`  (
  `Oid` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `Description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `OptimisticLockField` int(11) NULL DEFAULT NULL,
  `GCRecord` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`Oid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hr_maritalstatus
-- ----------------------------

-- ----------------------------
-- Table structure for hr_position
-- ----------------------------
DROP TABLE IF EXISTS `hr_position`;
CREATE TABLE `hr_position`  (
  `Oid` int(36) NOT NULL AUTO_INCREMENT,
  `Description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NoOfNeeds` int(11) NULL DEFAULT NULL,
  `CalculateInMPP` tinyint(4) NULL DEFAULT NULL,
  `CompetencyPercentage` double NULL DEFAULT NULL,
  `KeyPerformanceIndexPercentage` double NULL DEFAULT NULL,
  `OptimisticLockField` int(11) NULL DEFAULT NULL,
  `GCRecord` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`Oid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hr_position
-- ----------------------------

-- ----------------------------
-- Table structure for hr_religion
-- ----------------------------
DROP TABLE IF EXISTS `hr_religion`;
CREATE TABLE `hr_religion`  (
  `Oid` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `Description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `OptimisticLockField` int(11) NULL DEFAULT NULL,
  `GCRecord` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`Oid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hr_religion
-- ----------------------------

-- ----------------------------
-- Table structure for hr_salutation
-- ----------------------------
DROP TABLE IF EXISTS `hr_salutation`;
CREATE TABLE `hr_salutation`  (
  `Oid` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `Description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `OptimisticLockField` int(11) NULL DEFAULT NULL,
  `GCRecord` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`Oid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hr_salutation
-- ----------------------------

-- ----------------------------
-- Table structure for hr_unitgroup
-- ----------------------------
DROP TABLE IF EXISTS `hr_unitgroup`;
CREATE TABLE `hr_unitgroup`  (
  `Oid` int(11) NOT NULL AUTO_INCREMENT,
  `UnitGroupId` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `UnitGroupName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `OptimisticLockField` int(11) NULL DEFAULT NULL,
  `GCRecord` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`Oid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hr_unitgroup
-- ----------------------------

-- ----------------------------
-- Table structure for login_attempts
-- ----------------------------
DROP TABLE IF EXISTS `login_attempts`;
CREATE TABLE `login_attempts`  (
  `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `login` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `time` int(11) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of login_attempts
-- ----------------------------

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `icon` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `position` int(11) NULL DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `category` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 184 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES (2, 'Stock', 'Dasboard Stock', 'fa fa-arrow-circle-right', 3, 'dashboard_stock', 'DSB', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (3, 'Sales', 'Dasboard Sales', 'fa fa-arrow-circle-right', 4, 'dashboard_sales', 'DSB', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (4, 'Finance AR', 'Dasboard Finance AR', 'fa fa-arrow-circle-right', 8, 'dashboard_finance', 'DSB', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (5, 'Market Share', 'Dashboard Market Share', 'fa fa-arrow-circle-right', 5, 'dashboard_market', 'DSB', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (6, 'Revenue Sales', 'Dashboard Revenue', 'fa fa-arrow-circle-right', 6, 'dashboard_revenue', 'DSB', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (7, 'Gross Margin', 'Dashboard Gross Margin', 'fa fa-arrow-circle-right', 7, 'dashboard_gross_margin', 'DSB', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (8, 'Leads', 'Leads', 'fa fa-arrow-circle-right', 8, 'leads', 'LDS', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (9, 'Leads Distribution', 'Leads Distribution', 'fa fa-arrow-circle-right', 9, 'leads_distribution', 'LDS', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (10, 'Prospect', 'Prospect', 'fa fa-arrow-circle-right', 10, 'prospect', 'PRS', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (11, 'Prospect List', 'Prospect List', 'fa fa-arrow-circle-right', 11, 'prospect_list', 'PRS', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (12, 'Test Drive', 'Test Drive', 'fa fa-arrow-circle-right', 12, 'sales_testdrive', 'PRS', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (13, 'SPK Verification', 'SPK', 'fa fa-arrow-circle-right', 13, 'vehicle_order', 'SPK', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (14, 'SPK Outstanding', 'SPK Outstanding', 'fa fa-arrow-circle-right', 15, 'vehicle_order_list', 'SPK', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (15, 'SPK Expired', 'SPK Expired', 'fa fa-arrow-circle-right', 18, 'vehicle_order_exp', 'SPK', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (16, 'SPK Cancel List', 'SPK Cancel', 'fa fa-arrow-circle-right', 19, 'vehicle_order_cancel', 'SPK', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (17, 'SPK Update', 'SPK Update', 'fa fa-arrow-circle-right', 16, 'vehicle_order_edit', 'SPK', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (18, 'SPK Approval', 'SPK Approval', 'fa fa-arrow-circle-right', 17, 'vehicle_order_approval', 'SPK', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (19, 'Referensi Number', 'Referensi Number', 'fa fa-arrow-circle-right', 138, 'sales_vehicleorderregister', 'SPK', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (20, 'Billing  Integration', 'Billing  Integration', 'fa fa-arrow-circle-right', 20, 'kasir', 'BL', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (21, 'Credit Agreement', 'Perjanjian Kredit', 'fa fa-arrow-circle-right', 21, 'vehicle_ordermatch', 'PK', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (22, 'Credit Approval', 'Persetujuan Kredit', 'fa fa-arrow-circle-right', 22, 'vehicle_ordercredit', 'PK', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (23, 'Cover Note', 'Cover Note', 'fa fa-arrow-circle-right', 23, 'vehicle_ordercovernote', 'PK', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (24, 'Upload Sales Program', 'Upload Sales Program', 'fa fa-arrow-circle-right', 24, 'sales_vehiclediscount', 'SPG', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (25, 'Sales Program', 'Pengajuan Sales Program', 'fa fa-arrow-circle-right', 25, 'sales_proposeddiscount', 'SPG', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (26, 'Appv Sales Program', 'Approval Sales Program', 'fa fa-arrow-circle-right', 26, 'sales_proposeddisc_app', 'SPG', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (27, 'AFI List', 'AFI List', 'fa fa-arrow-circle-right', 27, 'vehicle_orderafi_list', 'RGS', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (28, 'AFI + SOLD', 'AJU AFI dan Pengakuan Penjualan', 'fa fa-arrow-circle-right', 28, 'vehicle_orderafi', 'RGS', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (29, 'AFI Verification', 'AFI Verification', 'fa fa-arrow-circle-right', 29, 'vehicle_orderafi_tam', 'RGS', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (30, 'AFI Batch', 'AFI Batch', 'fa fa-arrow-circle-right', 30, 'vehicle_orderafi_forbatch', 'RGS', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (31, 'AFI Release', 'AFI Bank', 'fa fa-arrow-circle-right', 31, 'vehicle_orderafi_batch', 'RGS', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (32, 'AFI From TAM', 'AFI From TAM', 'fa fa-arrow-circle-right', 32, 'vehicle_orderafi_fromtam', 'RGS', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (33, 'AFI To Branch', 'AFI To Branch', 'fa fa-arrow-circle-right', 33, 'vehicle_orderafi_tobranch', 'RGS', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (34, 'AFI Recomendation', 'AFI Rekomendasi', 'fa fa-arrow-circle-right', 34, 'vehicle_orderafi_rekomendasi', 'RGS', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (35, 'AFI Document From HO', 'Received STNK/BPKB From HO', 'fa fa-arrow-circle-right', 35, 'vehicle_orderafi_faktur', 'RGS', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (36, 'STNK To Samsat', 'Pengurusan STNK ke Samsat', 'fa fa-arrow-circle-right', 36, 'vehicle_orderafi_samsat', 'RGS', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (37, 'BPKB To Polda', 'Pengurusan BPKB ke Polda', 'fa fa-arrow-circle-right', 37, 'vehicle_orderafi_polda', 'RGS', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (38, 'Received STNK/BPKB', 'Terima STNK (Samsat) & BPKB (Polda)', 'fa fa-arrow-circle-right', 38, 'vehicle_orderafi_receipt', 'RGS', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (39, 'STNK/BPKB to Cust.', 'Penyerahan STNK & BPKB ke Customer', 'fa fa-arrow-circle-right', 39, 'vehicle_orderafi_handover', 'RGS', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (40, 'Owner Invoice Request', 'Owner Invoice Request', 'fa fa-arrow-circle-right', 40, 'vehicle_orderafi_req_pemilik', 'RGS', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (41, 'Accept Owner Invoice', 'Accept Owner Invoice', 'fa fa-arrow-circle-right', 41, 'vehicle_orderafi_receive_pemilik', 'RGS', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (42, 'Activity Plan', 'Modul Task List', 'fa fa-arrow-circle-right', 42, 'activity_plan', 'AP', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (43, 'Complaint Handling', 'Complaint Handling', 'fa fa-arrow-circle-right', 43, 'sales_complaint', 'KH', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (44, 'Stock Sold', 'Stock Sold', 'fa fa-arrow-circle-right', 44, 'sales_stock_sold', 'STK', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (45, 'Stock Current', 'Stock Current', 'fa fa-arrow-circle-right', 45, 'sales_stock_current', 'STK', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (46, 'Stock Distribution', 'Stock Distribution (Mutasi Stock Antar Cabang)', 'fa fa-arrow-circle-right', 46, 'sales_alokasi_stock', 'STK', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (47, 'Process Match', 'Stock Match', 'fa fa-arrow-circle-right', 47, 'sales_match', 'STK', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (48, 'Delivery Request', 'Delivery Request', 'fa fa-arrow-circle-right', 48, 'sales_delivery_request', 'STK', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (49, 'Delivery Request List', 'Delivery Request List', 'fa fa-arrow-circle-right', 49, 'sales_delivery_request_list', 'STK', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (50, 'Stock Swapping', 'Stock Swapping', 'fa fa-arrow-circle-right', 50, 'sales_swapping', 'STK', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (51, 'Unit Tracking', 'Unit Tracking', 'fa fa-arrow-circle-right', 51, 'sales_unit_tracking', 'STK', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (52, 'Sales Vehicle Mutation', 'Sales Vehicle Mutation', 'fa fa-arrow-circle-right', 52, 'sales_vehicle_mutation', 'STK', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (53, 'Cancel Sold', 'Cancel Sold', 'fa fa-arrow-circle-right', 53, 'vehicle_order_cancel_sold', 'STK', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (54, 'AFI Unit Model', 'AFI Unit Model', 'fa fa-arrow-circle-right', 54, 'sales_afi_unitmodel', 'STK', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (55, 'AFI Unit Type', 'AFI Unit Type', 'fa fa-arrow-circle-right', 55, 'sales_afi_unittype', 'STK', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (56, 'Test Drive Stock', 'Test Drive Stock', 'fa fa-arrow-circle-right', 56, 'sales_testdrivestock', 'STK', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (57, 'MDP TAM', 'MDP TAM', 'fa fa-arrow-circle-right', 57, 'sales_mdp', 'MDP', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (58, 'Delivery Order', 'Delivery Order (DO) TAM', 'fa fa-arrow-circle-right', 58, 'sales_do_tam', 'MDP', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (59, 'Accessories Standart', 'Accessories Standart', 'fa fa-arrow-circle-right', 59, 'sales_reguler_acc', 'SCM', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (60, 'PO Custom Type', 'PO Custom Type', 'fa fa-arrow-circle-right', 60, 'sales_po_customtype', 'SCM', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (61, 'Market Share', 'Entry/Upload Market Share', 'fa fa-arrow-circle-right', 61, 'sales_market_share', 'MKS', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (62, 'Country', 'Country', 'fa fa-arrow-circle-right', 62, 'cm_country', 'CM', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (63, 'Province', 'Province', 'fa fa-arrow-circle-right', 63, 'cm_province', 'CM', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (64, 'City', 'City', 'fa fa-arrow-circle-right', 64, 'cm_city', 'CM', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (65, 'City Type', 'City Type', 'fa fa-arrow-circle-right', 65, 'cm_citytype', 'CM', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (66, 'District', 'District', 'fa fa-arrow-circle-right', 66, 'cm_district', 'CM', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (67, 'Sub District', 'Sub District', 'fa fa-arrow-circle-right', 67, 'cm_subdistrict', 'CM', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (68, 'Lead Source', 'Lead Source', 'fa fa-arrow-circle-right', 68, 'leads_source', 'CM', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (69, 'Religion', 'Religion', 'fa fa-arrow-circle-right', 69, 'hr_religion', 'CM', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (70, 'Branch', 'Branch', 'fa fa-arrow-circle-right', 70, 'cm_branch', 'CM', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (71, 'Organizational Unit', 'Organizational Unit', 'fa fa-arrow-circle-right', 71, 'hr_organizational_unit', 'CM', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (72, 'Employee', 'Modul Employee', 'fa fa-arrow-circle-right', 72, 'hr_employee', 'CM', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (73, 'Set Expiration Leads', 'Set Expiration Leads', 'fa fa-arrow-circle-right', 73, 'config_leads', 'CM', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (74, 'Region Code', 'Region Code', 'fa fa-arrow-circle-right', 74, 'cm_region', 'CM', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (75, 'Insurance', 'Insurance', 'fa fa-arrow-circle-right', 75, 'cm_asuransi', 'CM', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (76, 'Insurance Type', 'Insurance Type', 'fa fa-arrow-circle-right', 76, 'cm_insurance_type', 'CM', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (77, 'Vendor', 'Vendor', 'fa fa-arrow-circle-right', 77, 'cm_vendor', 'CM', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (78, 'AFI Model FrameNo', 'AFI Model FrameNo', 'fa fa-arrow-circle-right', 78, 'afi_model', 'CM', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (79, 'Branch Accountbank', 'Branch Accountbank', 'fa fa-arrow-circle-right', 79, 'branch_accountbank', 'CM', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (80, 'Hobby', 'Hobby', 'fa fa-arrow-circle-right', 80, 'cm_hobby', 'CM', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (81, 'Identification Type', 'Identification Type', 'fa fa-arrow-circle-right', 81, 'cm_identification_type', 'CM', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (82, 'Profesi', 'Profesi', 'fa fa-arrow-circle-right', 82, 'cm_profesi', 'CM', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (83, 'Status Married', 'Status Married', 'fa fa-arrow-circle-right', 83, 'cm_status_married', 'CM', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (84, 'Status Stock', 'Status Stock', 'fa fa-arrow-circle-right', 84, 'cm_status_stock', 'CM', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (85, 'Customer', 'Customer', 'fa fa-arrow-circle-right', 85, 'cm_customer', 'CSM', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (86, 'Customer Type', 'Customer Type', 'fa fa-arrow-circle-right', 86, 'sales_customertype', 'MS', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (87, 'Sales Issue', 'Sales Issue', 'fa fa-arrow-circle-right', 87, 'sales_salesissue', 'MS', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (88, 'Vehicle Brand', 'Vehicle Brand', 'fa fa-arrow-circle-right', 88, 'sales_vehiclebrand', 'MS', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (89, 'Vehicle Model', 'Vehicle Model', 'fa fa-arrow-circle-right', 89, 'sales_vehiclemodel', 'MS', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (90, 'Vehicle Price', 'Vehicle Price', 'fa fa-arrow-circle-right', 90, 'sales_vehicleprice', 'MS', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (91, 'Vehicle Type', 'Vehicle Type', 'fa fa-arrow-circle-right', 91, 'sales_vehicletype', 'MS', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (92, 'Vehicle Color', 'Vehicle Color', 'fa fa-arrow-circle-right', 92, 'sales_vehiclecolor', 'MS', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (93, 'Vehicle Unit Color', 'Vehicle Unit Color', 'fa fa-arrow-circle-right', 93, 'sales_unitcolor', 'MS', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (94, 'Custom Type', 'Custom Type', 'fa fa-arrow-circle-right', 94, 'sales_customtype', 'MS', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (95, 'Accessories', 'Accessories', 'fa fa-arrow-circle-right', 95, 'sales_accessories', 'MS', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (96, 'Accessories Type', 'Accessories Type', 'fa fa-arrow-circle-right', 96, 'sales_accessoriestype', 'MS', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (97, 'Preparation', 'Preparation', 'fa fa-arrow-circle-right', 97, 'sales_preparation', 'MS', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (98, 'Preparation Group', 'Preparation Group', 'fa fa-arrow-circle-right', 98, 'sales_preparation_group', 'MS', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (99, 'Transportation Moda', 'Transportation Moda', 'fa fa-arrow-circle-right', 99, 'transportation_moda', 'MS', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (100, 'Modul Bank', 'Modul Bank', 'fa fa-arrow-circle-right', 100, 'cm_bank', 'MS', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (101, 'Follow up Type', 'Modul Sales Follow up Type', 'fa fa-arrow-circle-right', 101, 'sales_futype', 'MS', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (102, 'Follow up Type Group', 'Modul Sales Follow up Type Group', 'fa fa-arrow-circle-right', 102, 'sales_futype_group', 'MS', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (103, 'Follow up Type status', 'Modul Sales Follow up Typestatus', 'fa fa-arrow-circle-right', 103, 'sales_followupstatus', 'MS', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (104, 'Activity Type', 'Modul Activity Type', 'fa fa-arrow-circle-right', 104, 'activity_type', 'MS', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (105, 'Gender', 'Modul Gender', 'fa fa-arrow-circle-right', 105, 'hr_gender', 'MS', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (106, 'Prospect Source', 'Prospect Source', 'fa fa-arrow-circle-right', 106, 'prospect_source', 'MS', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (107, 'Data Flag', 'Data Flag', 'fa fa-arrow-circle-right', 107, 'sales_flag', 'MS', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (108, 'Leasing', 'Modul Leasing', 'fa fa-arrow-circle-right', 108, 'sales_leasing', 'MS', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (109, 'Modul Tenor', 'Modul Tenor', 'fa fa-arrow-circle-right', 109, 'sales_tenor', 'MS', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (110, 'Sales Mutation Price', 'sales_mutation_price', 'fa fa-arrow-circle-right', 110, 'sales_mutation_price', 'MS', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (111, 'Customer Retention', 'Customer Retention', 'fas fa-users', 111, 'customer_retention', 'CSM', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (112, 'Follow Up Customer', 'Follow Up Customer', 'fas fa-user-circle', 112, 'sales_followup_customer', 'CSM', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (113, 'Update Manifest Unit', 'Update Manifest Unit', 'fas fa-arrow-circle-right', 113, 'sales_vehiclemanifest', 'STK', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (114, 'Custom Type Price', 'Custom Type Price', 'far fa-arrow-alt-circle-right', 114, 'sales_vehicleprice_ct', 'SCM', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (115, 'Monitoring Covernote', 'Monitoring Covernote', 'fas fa-arrow-circle-right', 115, 'vehicle_ordercovernote_mtr', 'PK', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (116, 'Sales Report', 'Summary Salesreport', 'fas fa-arrow-circle-right', 116, 'summary_salesreport', 'SMR', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (117, 'Average price', 'Summary Averageprice', 'fas fa-arrow-circle-right', 117, 'summary_averageprice', 'SMR', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (118, 'RS By Model', 'Summary Retail Sales By Model', 'fas fa-arrow-circle-right', 5, 'summary_retailsales_model', 'SMR', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (119, 'RS By Branch', 'Summary Sales By Branch', 'fas fa-arrow-circle-right', 8, 'summary_sales_branch', 'SMR', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (120, 'Cancel Sold', 'Summary Sales Branch Cancel sold', 'fas fa-arrow-circle-right', 120, 'summary_sales_branch_cancelsold', 'SMR', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (121, 'AR By Leasing', 'Summary AR Leasing', 'fas fa-arrow-circle-right', 121, 'summary_ar_leasing', 'SMR', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (122, 'Stock Cut Off', 'Summary Stock Cut Off', 'fas fa-arrow-circle-right', 122, 'summary_stockcutoff', 'SMR', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (123, 'Stock VS SPK', 'Summary Stock VS SPK', 'fas fa-arrow-circle-right', 123, 'summary_stockvsspk', 'SMR', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (124, 'SPK Taking By Type', 'Summary SPK Taking By Type', 'fas fa-arrow-circle-right', 1, 'summary_spk_taking', 'SMR', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (125, 'SPK Valid By Branch', 'Summary SPK Valid By Branch', 'fas fa-arrow-circle-right', 4, 'summary_spkvalid_branch', 'SMR', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (126, 'RS By Salesman', 'Summary Sales By Salesman', 'fas fa-arrow-circle-right', 6, 'summary_sales_bysalesman', 'SMR', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (127, 'SPK Taking By Branch', 'Summary SPK Branch All', 'fas fa-arrow-circle-right', 2, 'summary_spk_branch', 'SMR', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (128, 'Ots Stock By Aging', 'Sum Outstanding Stock', 'fas fa-arrow-circle-right', 128, 'summary_outstanding_stock', 'SMR', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (129, 'AFI By Aging', 'Sum AFI By Aging', 'fas fa-arrow-circle-right', 129, 'summary_registration', 'SMR', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (130, 'Margin', 'Report Margin', 'fas fa-arrow-circle-right', 130, 'summary_margin', 'RPT', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (131, 'Insentif Sales', 'Insentif Sales', 'fas fa-bookmark', 5, 'report_insentifsales', 'RPT', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (132, 'Custom Type Report', 'Custom Type Report', 'fas fa-arrow-circle-right', 132, 'sales_po_customtype_report', 'SCM', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (133, 'Branch Market', 'Branch Market', 'fas fa-arrow-circle-right', 133, 'branch_market', 'CM', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (134, 'AR Sales', 'Report AR Sales', 'fas fa-arrow-circle-right', 7, 'report_ar_sales', 'RPT', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (135, 'Sales', 'Report Sales', 'fas fa-arrow-circle-right', 4, 'report_sales', 'RPT', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (136, 'Stock Custom Type', 'Stock Custom Type', 'fas fa-arrow-circle-right', 136, 'sales_stock_customtype', 'SCM', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (137, 'SPK Verification List', 'SPK Verification List', 'fas fa-arrow-circle-right', 14, 'vehicle_order_verificationlist', 'SPK', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (138, 'Cancel Sold List', 'SPK Cancel Sold List', 'fas fa-ban', 137, 'vehicle_order_cancelsoldlist', 'SPK', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (139, 'Afi Suffix Letter', 'Afi Suffix Letter', 'fas fa-arrow-circle-right', 139, 'afi_suffix_letter', 'RGS', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (140, 'History Sales Program', 'History Sales Program', 'fas fa-history', 140, 'history_proposeddisc', 'SPG', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (141, 'AFI Letter', 'AFI SUFFIX LETTER', 'fas fa-adjust', 141, 'afi_suffix_letter', 'CM', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (142, 'SPK Taking', 'Report SPK Taking', 'fas fa-arrow-circle-right', 1, 'report_spk_taking', 'RPT', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (143, 'Progress Sales', 'Progress Sales', 'fas fa-chart-line', 143, 'summary_progress_sales', 'SMR', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (144, 'Beban Bunga', 'Report Beban Bunga', 'fas fa-arrow-circle-right', 8, 'report_bebanbunga', 'RPT', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (145, 'RS By SPV', 'Summary DO by SPV', 'fas fa-arrow-circle-right', 7, 'summary_sales_spv', 'SMR', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (146, 'SPK Valid By SPV', 'Summary SPK by SPV', 'fas fa-arrow-circle-right', 3, 'summary_spk_byspv', 'SMR', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (147, 'Prospect', 'Prospect', 'fas fa-arrow-circle-right', 1, 'dashboard_prospect', 'DSB', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (148, 'SPK', 'SPK', 'fas fa-arrow-circle-right', 2, 'dashboard_spk', 'DSB', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (149, 'Purchase', 'Report Purchase', 'fas fa-arrow-circle-right', 6, 'report_purchase', 'RPT', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (150, 'Matrik Aging Due Date', 'Matrik Aging Due Date', 'fas fa-arrow-circle-right', 145, 'matriks_aging_duedate', 'MS', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (151, 'SPK Update Price', 'SPK Update Price', 'fas fa-arrow-circle-right', 146, 'vehicle_order_updateprice', 'SPK', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (152, 'Stock Current', 'Report Stock Current', 'fas fa-arrow-circle-right', 3, 'report_stock_current', 'RPT', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (153, 'Sales Level', 'Sales Level', 'fas fa-arrow-circle-right', 148, 'cm_levelsales', 'CM', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (154, 'Manifest', 'Report Manifest', 'fas fa-arrow-circle-right', 9, 'report_manifest', 'RPT', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (155, 'Update Price (SOLD)', 'Update Price (SOLD)', 'fas fa-arrow-circle-right', 150, 'vehicle_order_updateprice_aftersold', 'STK', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (156, 'SPK Outstanding', 'Report SPK Outstanding', 'fas fa-arrow-circle-right', 2, 'report_spk_outstanding', 'RPT', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (157, 'Sales Detail Payment', 'Detail Payment', 'fas fa-arrow-circle-right', 151, 'report_payment', 'RPT', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (158, 'SPK Taking By Model', 'SPK Taking By Type/Model', 'fas fa-arrow-circle-right', 1, 'summary_spk_taking_noncancel', 'SMR', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (159, 'Mutation After SOLD', 'Mutation After SOLD', 'fas fa-arrow-circle-right', 152, 'sales_stock_mutation', 'STK', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (160, 'CustomType Price', 'Price Info', 'far fa-money-bill-alt', 153, 'sales_vehicleprice_ct_list', 'PRC', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (161, 'Standard Price', 'Price Info', 'fas fa-money-bill-alt', 154, 'sales_vehicleprice_list', 'PRC', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (162, 'Release AFI', 'Report Release AFI', 'fas fa-arrow-circle-right', 155, 'report_sales_afi', 'RPT', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (163, 'Release AFI Ots', 'Report Release AFI Ots', 'fas fa-arrow-circle-right', 156, 'report_sales_afi_outstanding', 'RPT', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (164, 'Covernote', 'Report Covernote', 'fas fa-arrow-circle-right', 157, 'report_covernote', 'RPT', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (165, 'Aftersales', 'Report Aftersales', 'fas fa-arrow-circle-right', 158, 'report_aftersales', 'RPT', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (166, 'Report CRP', 'Report CRP', 'fas fa-arrow-circle-right', 159, 'report_crp', 'RPT', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (167, 'Cancel Sold', 'Report Cancel Sold', 'fas fa-arrow-circle-right', 160, 'report_cancelsold', 'RPT', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (168, 'Defect Type', 'Defect Type', 'fas fa-arrow-circle-right', 161, 'cm_defect_type', 'CM', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (169, 'Reason AR', 'Reason AR', 'fas fa-arrow-circle-right', 162, 'reason_ar', 'AR', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (170, 'Report PPATK', 'Report PPATK', 'fas fa-arrow-circle-right', 163, 'report_ppatk', 'RPT', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (171, 'Report Leadtime STNK', 'Report Leadtime STNK', 'fas fa-arrow-circle-right', 164, 'report_leadtime_stnk', 'RPT', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (172, 'Cfg Lead Time STNK', 'Config Lead Time STNK', 'fas fa-arrow-circle-right', 165, 'config_leadtime_stnk', 'CM', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (173, 'Stock Discontinue', 'Stock Discontinue', 'fas fa-arrow-circle-right', 166, 'sales_stock_discontinue', 'STK', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (174, 'Revenue', 'Revenue', 'fas fa-arrow-circle-right', 167, 'report_revenue', 'RPT', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (175, 'DR List', 'DR List', 'fas fa-arrow-circle-right', 168, 'report_delivery_request', 'RPT', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (176, 'Sales Paid Off', 'Sales Paid Off', 'fas fa-arrow-circle-right', 169, 'report_sales_paidoff', 'RPT', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (177, 'Target Sales', 'Target Sales', 'fas fa-arrow-circle-right', 170, 'cm_branchtarget', 'CM', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (178, 'Target VS Actual', 'Target VS Actual', 'fas fa-arrow-circle-right', 171, 'summary_performance', 'SMR', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (179, 'History Proposed Disc', 'History Proposed Disc', 'fas fa-arrow-circle-right', 172, 'report_proposed_disc', 'RPT', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (180, 'Cancel Delivery Request', 'Cancel Delivery Request', 'fas fa-arrow-circle-right', 173, 'sales_delivery_cancel_request', 'STK', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (181, 'Report Leads', 'Report Leads', 'fas fa-arrow-circle-right', 174, 'report_leads', 'RPT', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (182, 'App Cancel SPK', 'App Cancel SPK', 'fas fa-arrow-circle-right', 174, 'vehicle_order_app_cancel', 'SPK', NULL, NULL, NULL);
INSERT INTO `menu` VALUES (183, 'Approval Area', 'Approval Area', 'fas fa-arrow-circle-right', 175, 'vehicle_order_approval_area', 'SPK', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for menu_action
-- ----------------------------
DROP TABLE IF EXISTS `menu_action`;
CREATE TABLE `menu_action`  (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `icon` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `position` int(11) NULL DEFAULT NULL,
  `menu_id` int(20) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of menu_action
-- ----------------------------

-- ----------------------------
-- Table structure for menu_category
-- ----------------------------
DROP TABLE IF EXISTS `menu_category`;
CREATE TABLE `menu_category`  (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `kode` char(6) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `icon` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `position` int(11) NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of menu_category
-- ----------------------------
INSERT INTO `menu_category` VALUES (1, 'AP', 'Activity Plan', 'Activity Plan', 'far fa-registered', 11, NULL, NULL, NULL);
INSERT INTO `menu_category` VALUES (2, 'AR', 'AR', 'AR', 'fas fa-credit-card', 21, NULL, NULL, NULL);
INSERT INTO `menu_category` VALUES (3, 'BL', 'Billing', 'Billing', 'fas fa-sitemap', 7, NULL, NULL, NULL);
INSERT INTO `menu_category` VALUES (4, 'CM', 'Common', 'Common', 'far fa-user', 16, NULL, NULL, NULL);
INSERT INTO `menu_category` VALUES (5, 'CSM', 'CRP', 'CUSTOMER', 'fas fa-users', 19, NULL, NULL, NULL);
INSERT INTO `menu_category` VALUES (6, 'DSB', 'Dashboard', 'Dashboard', 'fas fa-ellipsis-h', 1, NULL, NULL, NULL);
INSERT INTO `menu_category` VALUES (7, 'KH', 'Complaint Handling', 'Complaint Handling', 'fab fa-slideshare', 12, NULL, NULL, NULL);
INSERT INTO `menu_category` VALUES (8, 'LDS', 'Leads', 'Leads', 'fas fa-chart-bar', 4, NULL, NULL, NULL);
INSERT INTO `menu_category` VALUES (9, 'MDP', 'MDP/Demand & Supply', 'MDP / Demand & Supply', 'fas fa-map-signs', 14, NULL, NULL, NULL);
INSERT INTO `menu_category` VALUES (10, 'MKS', 'Market Share', 'Market Share', 'fab fa-trello', 18, NULL, NULL, NULL);
INSERT INTO `menu_category` VALUES (11, 'MS', 'Master Data', 'Master Data', 'fas fa-briefcase', 17, NULL, NULL, NULL);
INSERT INTO `menu_category` VALUES (12, 'PK', 'Credit Process', 'Proses Kredit', 'fab fa-amazon-pay', 8, NULL, NULL, NULL);
INSERT INTO `menu_category` VALUES (13, 'PRC', 'Price Info', 'Price Info', 'far fa-money-bill-alt', 20, NULL, NULL, NULL);
INSERT INTO `menu_category` VALUES (14, 'PRS', 'Prospect', 'Prospect', 'far fa-address-card', 5, NULL, NULL, NULL);
INSERT INTO `menu_category` VALUES (15, 'RGS', 'Registration', 'Registration', 'far fa-money-bill-alt', 10, NULL, NULL, NULL);
INSERT INTO `menu_category` VALUES (16, 'RPT', 'Report', 'Report', 'fas  fa-file-archive-o', 3, NULL, NULL, NULL);
INSERT INTO `menu_category` VALUES (17, 'SCM', 'SCM', 'SCM', 'fas fa-boxes', 15, NULL, NULL, NULL);
INSERT INTO `menu_category` VALUES (18, 'SMR', 'Summary', 'Summary', 'fas fa-bookmark', 2, NULL, NULL, NULL);
INSERT INTO `menu_category` VALUES (19, 'SPG', 'Sales Program', 'Sales Program', 'fas fa-id-card', 9, NULL, NULL, NULL);
INSERT INTO `menu_category` VALUES (20, 'SPK', 'SPK', 'SPK', 'fas fa-archive', 6, NULL, NULL, NULL);
INSERT INTO `menu_category` VALUES (21, 'STK', 'Stock', 'Stock', 'fas fa-exclamation-circle', 13, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `version` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `class` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `group` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `namespace` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` int(11) NOT NULL,
  `batch` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 36 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '20181211100537', 'IonAuth\\Database\\Migrations\\Migration_Install_ion_auth', '', 'App', 1617601361, 1);
INSERT INTO `migrations` VALUES (2, '2020-01-10-112925', 'App\\Database\\Migrations\\CreateBlogTable', 'default', 'App', 1617601361, 1);
INSERT INTO `migrations` VALUES (3, '2021-04-01_105301', 'App\\Database\\Migrations\\Createcommon_bankTable', 'default', 'App', 1617601361, 1);
INSERT INTO `migrations` VALUES (4, '2021-04-01_105301', 'App\\Database\\Migrations\\Createcommon_branchTable', 'default', 'App', 1617601361, 1);
INSERT INTO `migrations` VALUES (5, '2021-04-01_105301', 'App\\Database\\Migrations\\Createcommon_cityTable', 'default', 'App', 1617601361, 1);
INSERT INTO `migrations` VALUES (6, '2021-04-01_105301', 'App\\Database\\Migrations\\Createcommon_countryTable', 'default', 'App', 1617601361, 1);
INSERT INTO `migrations` VALUES (7, '2021-04-01_105301', 'App\\Database\\Migrations\\Createcommon_customerTable', 'default', 'App', 1617601361, 1);
INSERT INTO `migrations` VALUES (8, '2021-04-01_105301', 'App\\Database\\Migrations\\Createcommon_districtTable', 'default', 'App', 1617601361, 1);
INSERT INTO `migrations` VALUES (9, '2021-04-01_105301', 'App\\Database\\Migrations\\Createcommon_provinceTable', 'default', 'App', 1617601361, 1);
INSERT INTO `migrations` VALUES (10, '2021-04-01_105301', 'App\\Database\\Migrations\\Createhr_bloodtypeTable', 'default', 'App', 1617601361, 1);
INSERT INTO `migrations` VALUES (11, '2021-04-01_105301', 'App\\Database\\Migrations\\Createhr_genderTable', 'default', 'App', 1617601361, 1);
INSERT INTO `migrations` VALUES (12, '2021-04-01_105301', 'App\\Database\\Migrations\\Createhr_maritalstatusTable', 'default', 'App', 1617601361, 1);
INSERT INTO `migrations` VALUES (13, '2021-04-01_105301', 'App\\Database\\Migrations\\Createhr_positionTable', 'default', 'App', 1617601361, 1);
INSERT INTO `migrations` VALUES (14, '2021-04-01_105301', 'App\\Database\\Migrations\\Createhr_religionTable', 'default', 'App', 1617601361, 1);
INSERT INTO `migrations` VALUES (15, '2021-04-01_105301', 'App\\Database\\Migrations\\Createhr_salutationTable', 'default', 'App', 1617601361, 1);
INSERT INTO `migrations` VALUES (16, '2021-04-01_105301', 'App\\Database\\Migrations\\Createhr_unitgroupTable', 'default', 'App', 1617601361, 1);
INSERT INTO `migrations` VALUES (17, '2021-04-01_105301', 'App\\Database\\Migrations\\CreatemenuTable', 'default', 'App', 1617601361, 1);
INSERT INTO `migrations` VALUES (18, '2021-04-01_105301', 'App\\Database\\Migrations\\Createmenu_actionTable', 'default', 'App', 1617601361, 1);
INSERT INTO `migrations` VALUES (19, '2021-04-01_105301', 'App\\Database\\Migrations\\Createmenu_categoryTable', 'default', 'App', 1617601361, 1);
INSERT INTO `migrations` VALUES (20, '2021-04-01_105301', 'App\\Database\\Migrations\\CreatepermissionTable', 'default', 'App', 1617601361, 1);
INSERT INTO `migrations` VALUES (21, '2021-04-01_105301', 'App\\Database\\Migrations\\Createpermission_actionTable', 'default', 'App', 1617601361, 1);
INSERT INTO `migrations` VALUES (22, '2021-04-01_105301', 'App\\Database\\Migrations\\Createsales_vehiclebrandTable', 'default', 'App', 1617601362, 1);
INSERT INTO `migrations` VALUES (23, '2021-04-01_105301', 'App\\Database\\Migrations\\Createsales_vehiclemodelTable', 'default', 'App', 1617601362, 1);
INSERT INTO `migrations` VALUES (24, '2021-04-01_105301', 'App\\Database\\Migrations\\Createsales_vehicletypeTable', 'default', 'App', 1617601362, 1);
INSERT INTO `migrations` VALUES (25, '2021-04-01_105302', 'App\\Database\\Migrations\\Createdepo_partTable', 'default', 'App', 1617601362, 1);
INSERT INTO `migrations` VALUES (26, '2021-04-01_105302', 'App\\Database\\Migrations\\Createdepo_stockcardTable', 'default', 'App', 1617601362, 1);
INSERT INTO `migrations` VALUES (27, '2021-04-01_105302', 'App\\Database\\Migrations\\Createsales_vehiclecolorTable', 'default', 'App', 1617601362, 1);
INSERT INTO `migrations` VALUES (28, '2021-04-01_105302', 'App\\Database\\Migrations\\Createsales_vehiclefullmodelTable', 'default', 'App', 1617601362, 1);
INSERT INTO `migrations` VALUES (29, '2021-04-01_105302', 'App\\Database\\Migrations\\Createsales_vehicleproductTable', 'default', 'App', 1617601362, 1);
INSERT INTO `migrations` VALUES (30, '2021-04-01_105302', 'App\\Database\\Migrations\\Createsales_vehicleunitTable', 'default', 'App', 1617601362, 1);
INSERT INTO `migrations` VALUES (31, '2021-04-01_105302', 'App\\Database\\Migrations\\Createsales_vehicleunitcolorTable', 'default', 'App', 1617601362, 1);
INSERT INTO `migrations` VALUES (32, '2021-04-01_105302', 'App\\Database\\Migrations\\Createservice_jobTable', 'default', 'App', 1617601362, 1);
INSERT INTO `migrations` VALUES (33, '2021-04-01_105302', 'App\\Database\\Migrations\\Createservice_joborderTable', 'default', 'App', 1617601362, 1);
INSERT INTO `migrations` VALUES (34, '2021-04-01_105302', 'App\\Database\\Migrations\\Createservice_joborderstatuslogTable', 'default', 'App', 1617601362, 1);
INSERT INTO `migrations` VALUES (35, '2021-04-01_105302', 'App\\Database\\Migrations\\Createservice_workorderpartTable', 'default', 'App', 1617601362, 1);

-- ----------------------------
-- Table structure for permission
-- ----------------------------
DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `menu_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of permission
-- ----------------------------

-- ----------------------------
-- Table structure for permission_action
-- ----------------------------
DROP TABLE IF EXISTS `permission_action`;
CREATE TABLE `permission_action`  (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `permission_id` int(20) NOT NULL,
  `menu_action_id` int(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of permission_action
-- ----------------------------

-- ----------------------------
-- Table structure for sales_vehiclebrand
-- ----------------------------
DROP TABLE IF EXISTS `sales_vehiclebrand`;
CREATE TABLE `sales_vehiclebrand`  (
  `Oid` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `BrandName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `GCRecord` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`Oid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sales_vehiclebrand
-- ----------------------------

-- ----------------------------
-- Table structure for sales_vehiclecolor
-- ----------------------------
DROP TABLE IF EXISTS `sales_vehiclecolor`;
CREATE TABLE `sales_vehiclecolor`  (
  `Oid` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ColorCode` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ColorNameEn` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ColorNameId` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `GCRecord` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`Oid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sales_vehiclecolor
-- ----------------------------

-- ----------------------------
-- Table structure for sales_vehiclefullmodel
-- ----------------------------
DROP TABLE IF EXISTS `sales_vehiclefullmodel`;
CREATE TABLE `sales_vehiclefullmodel`  (
  `VehicleFullModelId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ToyotaFullModelId` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FullModelId` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `VehicleFullModelName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `VehicleModel` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ReleasedYear` datetime(0) NULL DEFAULT NULL,
  `FuelType` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `isDifferentPriceByColor` tinyint(4) NULL DEFAULT NULL,
  `CylinderVolume` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Discontinue` tinyint(4) NULL DEFAULT NULL,
  `SegmentDesignBase` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SegmentCategory` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SegmentPrice` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SegmentPurpose` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Brand` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `OptimisticLockField` int(11) NULL DEFAULT NULL,
  `GCRecord` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`VehicleFullModelId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sales_vehiclefullmodel
-- ----------------------------

-- ----------------------------
-- Table structure for sales_vehiclemodel
-- ----------------------------
DROP TABLE IF EXISTS `sales_vehiclemodel`;
CREATE TABLE `sales_vehiclemodel`  (
  `Oid` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `VehicleModelName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Photo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SalesPoint` double NULL DEFAULT NULL,
  `Brand` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Design` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Category` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Sub_Category` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Price` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Sub_Price` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Discontinue` tinyint(1) NULL DEFAULT NULL,
  `GCRecord` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`Oid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sales_vehiclemodel
-- ----------------------------

-- ----------------------------
-- Table structure for sales_vehicleproduct
-- ----------------------------
DROP TABLE IF EXISTS `sales_vehicleproduct`;
CREATE TABLE `sales_vehicleproduct`  (
  `Oid` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ProductName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `VehicleFullModel` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Color` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Picture` longblob NULL,
  `LargePicture` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `Discontinue` tinyint(4) NULL DEFAULT NULL,
  `OptimisticLockField` int(11) NULL DEFAULT NULL,
  `GCRecord` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`Oid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sales_vehicleproduct
-- ----------------------------

-- ----------------------------
-- Table structure for sales_vehicletype
-- ----------------------------
DROP TABLE IF EXISTS `sales_vehicletype`;
CREATE TABLE `sales_vehicletype`  (
  `Oid` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `UnitTypeId` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `TypeIdTam` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `UnitTypeName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `UnitModel` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ReleasedYear` int(11) NULL DEFAULT NULL,
  `UnitColorId` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AFIUnitType` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `AFIUnitModel` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Discontinue` tinyint(1) NULL DEFAULT NULL,
  `GCRecord` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`Oid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sales_vehicletype
-- ----------------------------

-- ----------------------------
-- Table structure for sales_vehicleunit
-- ----------------------------
DROP TABLE IF EXISTS `sales_vehicleunit`;
CREATE TABLE `sales_vehicleunit`  (
  `ActiveServiceBooking` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DefaultContact` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Oid` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `RRN` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `VinCode` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `VehicleRegistrationNumber` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `RegistrationDate` datetime(0) NULL DEFAULT NULL,
  `RegistrationExpiryDate` datetime(0) NULL DEFAULT NULL,
  `EngineSerialNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FrameSerialNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `LicensePlateNumber` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `VehicleDeliveryDate` datetime(0) NULL DEFAULT NULL,
  `ProductionYear` int(11) NULL DEFAULT NULL,
  `TWC` datetime(0) NULL DEFAULT NULL,
  `DailyAverageMileage` int(11) NULL DEFAULT NULL,
  `LastMileageRecord` int(11) NULL DEFAULT NULL,
  `LastServiceDate` datetime(0) NULL DEFAULT NULL,
  `ExpectedNextServiceDate` datetime(0) NULL DEFAULT NULL,
  `NextServiceMileage` int(11) NULL DEFAULT NULL,
  `NextService` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `RegistrationType` int(11) NULL DEFAULT NULL,
  `FieldAction` tinyint(4) NULL DEFAULT NULL,
  `FieldActionName` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PassiveCustomer` tinyint(4) NULL DEFAULT NULL,
  `Blacklisted` tinyint(4) NULL DEFAULT NULL,
  `Branch` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `VehicleNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `KeyNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Dealer` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `VehicleProduct` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Customer` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Partner` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Leasing` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SalesRemark` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `TestDriveUnitStock` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `VehicleOrder` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Route` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `LatestLocation` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Insurance` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ExpectedNextServiceByMilage` int(11) NULL DEFAULT NULL,
  `PeriodicServiceDeadline` datetime(0) NULL DEFAULT NULL,
  `CalculatedService` datetime(0) NULL DEFAULT NULL,
  `OptimisticLockField` int(11) NULL DEFAULT NULL,
  `GCRecord` int(11) NULL DEFAULT NULL,
  `ServiceRemark` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `PolisNo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Oid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sales_vehicleunit
-- ----------------------------

-- ----------------------------
-- Table structure for sales_vehicleunitcolor
-- ----------------------------
DROP TABLE IF EXISTS `sales_vehicleunitcolor`;
CREATE TABLE `sales_vehicleunitcolor`  (
  `Oid` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `UnitType` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Color` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `LargePicture` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `Discontinue` tinyint(4) NULL DEFAULT NULL,
  `OptimisticLockField` int(11) NULL DEFAULT NULL,
  `GCRecord` int(11) NULL DEFAULT NULL,
  `InteriorColor` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`Oid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sales_vehicleunitcolor
-- ----------------------------

-- ----------------------------
-- Table structure for service_job
-- ----------------------------
DROP TABLE IF EXISTS `service_job`;
CREATE TABLE `service_job`  (
  `Oid` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JobName` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `JobDescription` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ServiceUnitPrice` decimal(19, 0) NULL DEFAULT NULL,
  `TechnicianJobRate` double NULL DEFAULT NULL,
  `RequiredSkill` int(11) NULL DEFAULT NULL,
  `ApprovalInsurance` tinyint(4) NULL DEFAULT NULL,
  `DefaultProgram` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ServiceUnitRate` double NULL DEFAULT NULL,
  `NextService` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `IsFirstService` tinyint(4) NULL DEFAULT NULL,
  `Branch` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `RepairSubType` int(11) NULL DEFAULT NULL,
  `WithContract` tinyint(4) NULL DEFAULT NULL,
  `VehicleModel` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `VehicleFullModel` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `RequirementServiceCertificate` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ParentJob` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `JobAccountCategory` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `SalesAccount` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DiscountAccount` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ReturnAccount` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ExpenseAccount` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PurchaseReturnAccount` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Mileage` int(11) NULL DEFAULT NULL,
  `SubletJob` tinyint(4) NULL DEFAULT NULL,
  `RequirePO` tinyint(4) NULL DEFAULT NULL,
  `Supplier` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `OptimisticLockField` int(11) NULL DEFAULT NULL,
  `GCRecord` int(11) NULL DEFAULT NULL,
  `SubletVendorPrice` decimal(19, 0) NULL DEFAULT NULL,
  PRIMARY KEY (`Oid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of service_job
-- ----------------------------

-- ----------------------------
-- Table structure for service_joborder
-- ----------------------------
DROP TABLE IF EXISTS `service_joborder`;
CREATE TABLE `service_joborder`  (
  `WorkOrder` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Job` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CustomJobDescription` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `IsReturnJob` tinyint(4) NULL DEFAULT NULL,
  `SPKApproved` tinyint(4) NULL DEFAULT NULL,
  `FinalInspectionUnSolvedJob` tinyint(4) NULL DEFAULT NULL,
  `FinalInspectionJobSolvedBy` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Technician` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Stall` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `JobServiceRate` double NULL DEFAULT NULL,
  `ServiceUnitPrice` decimal(19, 0) NULL DEFAULT NULL,
  `WithholdingTax` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `VATCode` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Program` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ProgramPercentage` double NULL DEFAULT NULL,
  `ProgramAmount` decimal(19, 0) NULL DEFAULT NULL,
  `DiscountApproved` tinyint(4) NULL DEFAULT NULL,
  `Discount` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DiscountPercentage` double NULL DEFAULT NULL,
  `Insurance` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PartnerStatus` int(11) NULL DEFAULT NULL,
  `FinishTime` datetime(0) NULL DEFAULT NULL,
  `EstimatedFinishTime` datetime(0) NULL DEFAULT NULL,
  `ServiceStartOn` datetime(0) NULL DEFAULT NULL,
  `ActualServiceRate` double NULL DEFAULT NULL,
  `RequirePO` tinyint(4) NULL DEFAULT NULL,
  `JobStatus` int(11) NULL DEFAULT NULL,
  `IsActive` tinyint(4) NULL DEFAULT NULL,
  `SubletInvoiceCreated` tinyint(4) NULL DEFAULT NULL,
  `Billed` tinyint(4) NULL DEFAULT NULL,
  `ProgramBilled` tinyint(4) NULL DEFAULT NULL,
  `AllDay` tinyint(4) NULL DEFAULT NULL,
  `EndOn` datetime(0) NULL DEFAULT NULL,
  `Label` int(11) NULL DEFAULT NULL,
  `Location` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Status` int(11) NULL DEFAULT NULL,
  `Type` int(11) NULL DEFAULT NULL,
  `OptimisticLockField` int(11) NULL DEFAULT NULL,
  `GCRecord` int(11) NULL DEFAULT NULL,
  `TotalJobReDo` int(11) NULL DEFAULT NULL,
  `ServiceInvoice` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `IsJobReDo` tinyint(4) NULL DEFAULT NULL,
  `JobReDoCount` int(11) NULL DEFAULT NULL,
  `JobOrderId` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `JobInsuranceAmount` decimal(19, 0) NULL DEFAULT NULL,
  `ExcludeInLeadTimeCalc` tinyint(4) NULL DEFAULT NULL,
  PRIMARY KEY (`JobOrderId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of service_joborder
-- ----------------------------

-- ----------------------------
-- Table structure for service_joborderstatuslog
-- ----------------------------
DROP TABLE IF EXISTS `service_joborderstatuslog`;
CREATE TABLE `service_joborderstatuslog`  (
  `Oid` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `Job` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `JobStatus` int(11) NULL DEFAULT NULL,
  `Paused` tinyint(4) NULL DEFAULT NULL,
  `ClockOn` datetime(0) NULL DEFAULT NULL,
  `ClockOff` datetime(0) NULL DEFAULT NULL,
  `Remark` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `ClockOnBy` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Technician` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `OptimisticLockField` int(11) NULL DEFAULT NULL,
  `GCRecord` int(11) NULL DEFAULT NULL,
  `JobOrder` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `BPJobAction` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `BPAction` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`Oid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of service_joborderstatuslog
-- ----------------------------

-- ----------------------------
-- Table structure for service_workorderpart
-- ----------------------------
DROP TABLE IF EXISTS `service_workorderpart`;
CREATE TABLE `service_workorderpart`  (
  `QuantitySupplied` int(11) NULL DEFAULT NULL,
  `Oid` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `WorkOrder` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Job` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Part` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Remark` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Quantity` int(11) NULL DEFAULT NULL,
  `ServiceRate` double NULL DEFAULT NULL,
  `UnitPrice` decimal(19, 0) NULL DEFAULT NULL,
  `Program` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ProgramPercentage` double NULL DEFAULT NULL,
  `ProgramAmount` decimal(19, 0) NULL DEFAULT NULL,
  `Discount` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DiscountPercentage` double NULL DEFAULT NULL,
  `VATCode` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `WithholdingTax` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Billed` tinyint(4) NULL DEFAULT NULL,
  `ProgramBilled` tinyint(4) NULL DEFAULT NULL,
  `PartStatus` int(11) NULL DEFAULT NULL,
  `ETA` datetime(0) NULL DEFAULT NULL,
  `Insurance` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `InsuranceApproved` tinyint(4) NULL DEFAULT NULL,
  `OrderProcessed` tinyint(4) NULL DEFAULT NULL,
  `PartIsPicked` tinyint(4) NULL DEFAULT NULL,
  `ServicePartBackOrder` char(36) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DiscountApproved` tinyint(4) NULL DEFAULT NULL,
  `OptimisticLockField` int(11) NULL DEFAULT NULL,
  `GCRecord` int(11) NULL DEFAULT NULL,
  `PartInsuranceAmount` decimal(19, 0) NULL DEFAULT NULL,
  PRIMARY KEY (`Oid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of service_workorderpart
-- ----------------------------

-- ----------------------------
-- Table structure for status
-- ----------------------------
DROP TABLE IF EXISTS `status`;
CREATE TABLE `status`  (
  `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `status` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of status
-- ----------------------------

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `username` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `activation_selector` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `activation_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `forgotten_password_selector` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `forgotten_password_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED NULL DEFAULT NULL,
  `remember_selector` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `remember_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED NULL DEFAULT NULL,
  `active` tinyint(1) UNSIGNED NULL DEFAULT NULL,
  `first_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `last_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `company` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `email`(`email`) USING BTREE,
  UNIQUE INDEX `activation_selector`(`activation_selector`) USING BTREE,
  UNIQUE INDEX `forgotten_password_selector`(`forgotten_password_selector`) USING BTREE,
  UNIQUE INDEX `remember_selector`(`remember_selector`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, '127.0.0.1', 'administrator', '$2y$12$dOKLNDGHhKXG0EwtvTZ/q.Pn8PVfrNj6Uex7xOO9IiDhlzsRFAJpq', 'admin@admin.com', NULL, '', NULL, NULL, NULL, NULL, NULL, 1268889823, 1617932048, 1, 'Admin', 'istrator', 'ADMIN', '0');

-- ----------------------------
-- Table structure for users_groups
-- ----------------------------
DROP TABLE IF EXISTS `users_groups`;
CREATE TABLE `users_groups`  (
  `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` mediumint(8) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `users_groups_user_id_foreign`(`user_id`) USING BTREE,
  INDEX `users_groups_group_id_foreign`(`group_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of users_groups
-- ----------------------------
INSERT INTO `users_groups` VALUES (1, 1, 1);
INSERT INTO `users_groups` VALUES (2, 1, 2);

SET FOREIGN_KEY_CHECKS = 1;
