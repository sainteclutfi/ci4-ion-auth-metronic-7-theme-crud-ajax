<form id="form-edit" accept-charset="utf-8">
   <div class="form-group">
      <input type="hidden" value="<?php echo $main['id']; ?>" name="id" class="form-control">
      <label for="name">Name</label>
      <input type="text" name="name" value="<?= !empty($main['name']) ? $main['name'] : '' ?>" class="form-control" />
   </div>
   <div class="form-group">
      <input type="hidden" value="<?php echo $main['id']; ?>" name="id" class="form-control">
      <label for="icon">Icon</label>
      <input type="text" name="icon" value="<?= !empty($main['icon']) ? $main['icon'] : '' ?>" class="form-control" />
   </div>
   <div class="form-group">
      <input type="hidden" value="<?php echo $main['id']; ?>" name="id" class="form-control">
      <label for="position">Position</label>
      <input type="text" name="position" value="<?= !empty($main['position']) ? $main['position'] : '' ?>" class="form-control" />
   </div>
   <div class="form-group">
      <input type="hidden" value="<?php echo $main['id']; ?>" name="id" class="form-control">
      <label for="menu_id">Menu Id</label>
      <select name="menu_id" class="custom-select">
         <?php foreach($data_menu as $menus => $row): ?>
         <option value="<?= $row['id'] ?>" <?= !empty($row['menu_id']) && $row['menu_id'] == $row['id'] ? 'selected' : '' ?>><?= $row['id'] ?></option>
         <?php endforeach ?>
      </select>
   </div>
   </div>
   </div>
   </div>
   <div class="form-group">
      <button type="submit" class="submit btn btn-primary">Save</button>
      <button type="button" class="closed btn btn-secondary" data-dismiss="modal">Close</button>
      <label for="error"></label>
   </div>
</form>