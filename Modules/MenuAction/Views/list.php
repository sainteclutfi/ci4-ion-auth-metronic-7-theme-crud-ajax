
<?= $this->extend('layout/main') ?>
<?= $this->section('content') ?>
   <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
        <div class="subheader py-3 py-lg-8 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h2 class="d-flex align-items-center text-dark font-weight-bold my-1 mr-3">Module MenuAction</h2>
                        <!--end::Page Title-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Card-->
                <div class="card card-custom">
                    <div class="card-header">
                        <div class="card-title">
                            <span class="card-icon">
                                <i class="flaticon2-delivery-package text-primary"></i>
                            </span>
                            <h3 class="card-label">Module MenuAction</h3>
                        </div>
                        <div class="card-toolbar">
                            <!--begin::Button-->
                            <a id="create" class="btn btn-primary font-weight-bolder">New Record</a>
                            <!--end::Button-->
                        </div>
                    </div>
                    <div class="card-body">
                        <!--begin: Search Form-->
                        <!--begin: Datatable-->
                        <table class="table table-bordered table-hover table-checkable" id="data-table-book">
                            <thead>
                                <tr role="row">
                                    <th>No</th>
                                      <th>Name</th>
                                      <th>Icon</th>
                                      <th>Position</th>
                                      <th>Menu Id</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                        <!--end: Datatable-->
                    </div>
                </div>
                <!--end::Card-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
         
    <!-- /.row -->
<?= $this->endSection() ?>

<!--begin::Page Vendors Styles(used by this page)-->
<?= $this->section('page-vendors-styles-js') ?>
<link href="<?= base_url() ?>/theme/assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
<?= $this->endSection() ?>

<!--begin::Page Vendors(used by this page)-->
<?= $this->section('page-vendors-js') ?>
<script src="<?= base_url() ?>/theme/assets/plugins/custom/datatables/datatables.bundle.js"></script>
<?= $this->endSection() ?>

<!--begin::Page Scripts(used by this page)-->
<?= $this->section('extra-js') ?>
<!-- <script src="/theme/assets/js/pages/crud/datatables/search-options/advanced-search.js"></script> -->
<script src="<?= base_url() ?>/js/MenuAction/MenuAction.js"></script>
<script>Page.init();</script>
<?= $this->endSection() ?>

