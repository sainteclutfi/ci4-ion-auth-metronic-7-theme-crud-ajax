<?php 
      if(!isset($routes))
      { 
          $routes = \Config\Services::routes(true);
      }
      
      $routes->group('menuaction', ['namespace' => 'Modules\MenuAction\Controllers'], function($routes)
      {
          $routes->get('/', 'MenuAction::index');
          $routes->get('edit', 'MenuAction::load_form_edit');
          $routes->get('add', 'MenuAction::load_form_add');
          $routes->post('updated', 'MenuAction::updated');
          $routes->post('addnew', 'MenuAction::new');
          $routes->post('data', 'MenuAction::data');
          $routes->resource('resources', [
              'controller' => 'MenuAction',
          ]);
      });
      