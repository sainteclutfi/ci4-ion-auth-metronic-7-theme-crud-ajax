<?php
namespace Modules\MenuAction\Models;
use CodeIgniter\Model;

class MenuActionModel extends Model{
   protected $table      = 'menu_action';
   protected $primaryKey = 'id';
   protected $allowedFields = ['name', 'icon', 'position', 'menu_id', 'created_at', 'updated_at', 'menu_action.deleted_at'];
   protected $searchFields = ['name', 'icon', 'position', 'menu_id', 'created_at'];

   public function filter($search = null, $limit = null, $start = null, $orderField = null, $orderDir = null){
      $builder = $this->table($this->table);

      $i = 0;
      foreach ($this->searchFields as $column)
      {
            if($search)
            {
               if($i == 0)
               {
                  $builder->groupStart()
                          ->like($column, $search);
               }
               else
               {
                  $builder->orLike($column, $search);
               }

               if(count($this->searchFields) - 1 == $i) $builder->groupEnd();

            }
            $i++;
      }

      // Secara bawaan menampilkan data sebanyak kurang dari
      // atau sama dengan 7 kolom pertama.
      $builder->select('menu_action.id, menu_action.name, menu_action.icon, menu_action.position, menu_action.menu_id, menu_action.created_at, menu_action.updated_at')
              ->join('menu', 'menu.id = menu_action.menu_id')
              ->where('menu_action.deleted_at is NULL')
              ->orderBy($orderField, $orderDir)
              ->limit($limit, $start);

      $query = $builder;

      return $query;
   }

   public function countTotal(){
      return $this->table($this->table)
                  ->join('menu', 'menu.id = menu_action.menu_id')
                  ->countAll();
   }

   public function countFilter($search){
      $builder = $this->table($this->table);

      $i = 0;
      foreach ($this->searchFields as $column)
      {
            if($search)
            {
               if($i == 0)
               {
                  $builder->groupStart()
                          ->like($column, $search);
               }
               else
               {
                  $builder->orLike($column, $search);
               }

               if(count($this->searchFields) - 1 == $i) $builder->groupEnd();

            }
            $i++;
      }

      return $builder->join('menu', 'menu.id = menu_action.menu_id')
                     ->countAllResults();
   }

}