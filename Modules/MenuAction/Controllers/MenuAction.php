<?php
namespace Modules\MenuAction\Controllers;

use CodeIgniter\Controller;
use CodeIgniter\API\ResponseTrait;
use App\Controllers\BaseController;
use Modules\MenuAction\Models\MenuActionModel;
use Modules\Menu\Models\MenuModel;
class MenuAction extends BaseController{
  use ResponseTrait;

  protected $MenuAction;

  public function __construct() {
      $this->validation = \Config\Services::validation();
     $this->MenuActionModel = new MenuActionModel;
     $this->MenuModel = new MenuModel;

  }

  function index(){
     $data = [
        'title' => 'Data Menu Action',
        'host' => site_url('menuaction/')
     ];
     echo view('Modules\MenuAction\Views\list', $data);
  }

  public function data(){

      $where = "";
      $request = esc($this->request->getPost());
      $search = $request['search']['value'];
      $limit = $request['length'];
      $start = $request['start'];

      $orderIndex = $request['order'][0]['column'];
      $orderFields = $request['columns'][$orderIndex]['data'];
      $orderDir = $request['order'][0]['dir'];

      $list = $this->MenuActionModel->filter($search, $limit, $start, $orderFields, $orderDir)->get()->getResultObject();

      $data = array();
      $no = $start;
      foreach ($list as $r) {
            $no++;
            $row = array();
            $btn_actions = '<div class="btn-group btn-group-sm">
                                    <button class="btn btn-primary btn-edit" data-id="'.$r->id.'"><i class="fas fa-pencil-alt"></i></button>
                                    <button class="btn btn-danger btn-delete" data-id="'.$r->id.'"><i class="fas fa-trash"></i></button>
                                </div>';
            $row[] = $no;
            $row[] = $r->name;
            $row[] = $r->icon;
            $row[] = $r->position;
            $row[] = $r->menu_id;
            $row[] = $btn_actions;
            $data[] = $row;

      }

      return $this->respond([
         'draw'            => $this->request->getPost('draw'),
         'recordsTotal'    => $this->MenuActionModel->countTotal(),
         'recordsFiltered' => $this->MenuActionModel->countFilter($search),
         'data'            => $data,
     ]);

  }

  public function load_form_add()
  {

   $data = [
        'data_menu' => $this->MenuModel->findAll(),
   ];
   return view('Modules\MenuAction\Views\add', $data);
  }

  public function new()
  {
      $request = $this->request->getPost();
      $this->rules();
      if ($this->validation->run($request) != TRUE) {
         return $this->respond([
            'status' => 400,
            'error' => 400,
            'messages' => $this->validation->listErrors()
         ], 400);
 
      }else{
         $data = [
               'name' => $this->request->getPost('name'),
               'icon' => $this->request->getPost('icon'),
               'position' => $this->request->getPost('position'),
               'menu_id' => $this->request->getPost('menu_id'),
         ];

         if ($this->MenuActionModel->insert($data)) {
            return $this->respondCreated();
         }
         return $this->fail($this->MenuActionModel->errors());
      }
  }

  public function load_form_edit()
   {
      $id = $this->request->getGet('id');
      $data = [
        'data_menu' => $this->MenuModel->findAll(),
      ];
      $data['main'] = $this->MenuActionModel->find($id);
      return view('Modules\MenuAction\Views\edit', $data);
   }

   public function updated()
    {
      $id = $this->request->getPost('id');
      $request = $this->request->getPost();
      $this->rules();
      if ($this->validation->run($request) != TRUE) {
         return $this->respond([
            'status' => 400,
            'error' => 400,
            'messages' => $this->validation->listErrors()
         ], 400);
 
      }else{
         $data = [
   
               'name' => $this->request->getPost('name'),
               'icon' => $this->request->getPost('icon'),
               'position' => $this->request->getPost('position'),
               'menu_id' => $this->request->getPost('menu_id'),
         ];
         if ($this->MenuActionModel->update($id, $data)) {
               return $this->respondCreated();
         }
         return $this->fail($this->MenuActionModel->errors());
      }
    }

    public function delete($id)
    {
        $data = ['deleted_at' => date("Y-m-d h:i:s")];

        if ($found = $this->MenuActionModel->update($id, $data)) {
            return $this->respondDeleted($found);
        }

        return $this->fail('Fail deleted');
    }

    private function rules(){
      $this->validation->setRules([
         'name' => [
            'label' => 'Name',
            'rules' => 'required|string|max_length[255]'
         ],
         'icon' => [
            'label' => 'Icon',
            'rules' => 'required|string|max_length[50]'
         ],
         'position' => [
            'label' => 'Position',
            'rules' => 'required|numeric|max_length[11]'
         ],
         'menu_id' => [
            'label' => 'Menu Id',
            'rules' => 'required|numeric|max_length[20]'
         ],
      ]);
   }

}