<?php
namespace Modules\Reception\Controllers;

use CodeIgniter\Controller;
use CodeIgniter\API\ResponseTrait;
use App\Controllers\BaseController;
use Modules\Reception\Models\ReceptionModel;
class Reception extends BaseController{
  use ResponseTrait;

  protected $Reception;

  public function __construct() {
      helper('system_helper');
      $this->validation = \Config\Services::validation();
      $this->ReceptionModel = new ReceptionModel;

  }

  function index(){
     $data = [
        'title' => 'Data Reception',
        'host' => site_url('reception/')
     ];
     
     
     echo view('Modules\Reception\Views\list', $data);
  }

  public function loadContent($record = 1) {
   $posts 				= $this->ReceptionModel;
   $data['page'] 		= $record>0? $record : 1;
   $data['per_page'] 	= 15;
   $data['offset'] 	= ($data['page'] - 1) * $data['per_page'];
   $total 				= $posts->countAll();
   $total_pages 		= ceil($total/$data['per_page']);
   $data['adjacent'] = 10;
   $reload 			= base_url('reception/loadContent');

   $db      = \Config\Database::connect();
   $builder = $db->table('ajx_posts as sec');
   $builder->select("sec.*");
   $builder->orderBy('nom_post', 'asc');
   $builder->limit($data['per_page'], $data['offset']);
   $query 			= $builder->get();
   $list_search 	= $query->getResult();

   $data['search'] = "";

   if ($list_search) {
      foreach ($list_search as $post) {
         $data['search'] .= '<div class="col-md-4 col-sm-6 col-xs-6 mb-2 d-flex">
            <div class="card mb-2">
                  <a href="'.base_url('reception/'.$post->slug_post).'">
                     <img src="'.base_url('images/default_post.png').'" class="card-img-top">
                  </a>
                  <div class="card-body font-weight-bold ">
                     <a class="text-dark" href="'.base_url('reception/'.$post->slug_post).'">
                        '.$post->nom_post.'
                     </a>
                     <div>
                        <small class="float float-right"><span class="byline-author-label">By</span>
                           <a class="byline-author-name-link" href="#" title="DMS Service">DMS Service</a></small>
                        <small>'.$post->fc_post.'</small>
                     </div>
                  </div>
               </div>
         </div>';
      }
      $data['pagination'] = '<span class="pull-right">'.paginate($reload, $data['page'], $total_pages, $data['adjacent']).'</span>';
   }else{
      $data['search']='<div class="col-md-12 text-center"><div class="alert alert-danger" role="alert"><i class="fas fa-search"></i> No Result.</div></div>';
   }
   echo json_encode($data);
}

  

}