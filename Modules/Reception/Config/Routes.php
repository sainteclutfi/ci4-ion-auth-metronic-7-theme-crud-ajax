<?php 
      if(!isset($routes))
      { 
          $routes = \Config\Services::routes(true);
      }
      
      $routes->group('reception', ['namespace' => 'Modules\Reception\Controllers'], function($routes)
      {
          $routes->get('/', 'Reception::index');
          $routes->post('data/(:num)', 'Reception::loadContent/$1');
          $routes->resource('resources', [
              'controller' => 'Reception',
          ]);
      });
      