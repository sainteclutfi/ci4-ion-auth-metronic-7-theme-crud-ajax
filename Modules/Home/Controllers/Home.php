<?php

namespace Modules\Home\Controllers;

use App\Controllers\BaseController;

class Home extends BaseController
{

    public function __construct()
    {
    }

    /**
     * Tampilkan daftar index.
     *
     * @return \CodeIgniter\Http\Response
     */
    public function index()
    {
        if (!$this->isAuthorized()) {
            return redirect()->to('/auth/login');
        }

        
        return view('Modules\Home\Views\IndexView');
    }
}
