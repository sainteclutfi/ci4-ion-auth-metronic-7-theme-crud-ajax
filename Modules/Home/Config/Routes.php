<?php

/**
 * --------------------------------------------------------------------
 * HMVC Routing
 * --------------------------------------------------------------------
 */
if (!isset($routes)) {
    $routes = \Config\Services::routes(true);
}

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.

$routes->group('home', ['namespace' => 'Modules\Home\Controllers'], function ($routes) {
    $routes->get('/', 'Home::index');
});
