<?php
namespace Modules\Menu\Controllers;

use CodeIgniter\Controller;
use CodeIgniter\API\ResponseTrait;
use App\Controllers\BaseController;
use Modules\Menu\Models\MenuModel;
class Menu extends BaseController{
  use ResponseTrait;

  protected $Menu;

  public function __construct() {
      $this->validation = \Config\Services::validation();
     $this->MenuModel = new MenuModel;

  }

  function index(){
     $data = [
        'title' => 'Data Menu',
        'host' => site_url('menu/')
     ];
     
     //check roles 
     $menu = $this->ionAuth->_get_menu_url($this->request->uri->getSegment(1))->getRow();
     $groupId = $this->ionAuth->getUsersGroups()->getRow()->id;
     if(!$this->ionAuth->hasAccess($menu->id,$groupId))
     echo view('errors\html\error_404'); else
     echo view('Modules\Menu\Views\list', $data);
  }

  public function data(){

      $where = "";
      $request = esc($this->request->getPost());
      $search = $request['search']['value'];
      $limit = $request['length'];
      $start = $request['start'];

      $orderIndex = $request['order'][0]['column'];
      $orderFields = $request['columns'][$orderIndex]['data'];
      $orderDir = $request['order'][0]['dir'];

      $list = $this->MenuModel->filter($search, $limit, $start, $orderFields, $orderDir)->get()->getResultObject();

      $data = array();
      $no = $start;
      foreach ($list as $r) {
            $no++;
            $row = array();
            $btn_actions = '<div class="btn-group btn-group-sm">
                                    <button class="btn btn-primary btn-edit" data-id="'.$r->id.'"><i class="fas fa-pencil-alt"></i></button>
                                    <button class="btn btn-danger btn-delete" data-id="'.$r->id.'"><i class="fas fa-trash"></i></button>
                                </div>';
            $row[] = $no;
            $row[] = $r->name;
            $row[] = $r->description;
            $row[] = $r->icon;
            $row[] = $r->position;
            $row[] = $r->url;
            $row[] = $r->category;
            $row[] = $btn_actions;
            $data[] = $row;

      }

      return $this->respond([
         'draw'            => $this->request->getPost('draw'),
         'recordsTotal'    => $this->MenuModel->countAllResults(),
         'recordsFiltered' => $this->MenuModel->countFilter($search),
         'data'            => $data,
     ]);

  }

  public function load_form_add()
  {

   $data = [
   ];
   $data['menu_category'] = $this->ionAuth->getMenuCategory()->getResult();
   return view('Modules\Menu\Views\add', $data);
  }

  public function new()
  {
      $request = $this->request->getPost();
      $this->rules();
      if ($this->validation->run($request) != TRUE) {
         return $this->respond([
            'status' => 400,
            'error' => 400,
            'messages' => $this->validation->listErrors()
         ], 400);
 
      }else{
         $data = [
               'name' => $this->request->getPost('name'),
               'description' => $this->request->getPost('description'),
               'icon' => $this->request->getPost('icon'),
               'position' => $this->request->getPost('position'),
               'url' => $this->request->getPost('url'),
               'category' => $this->request->getPost('category'),
         ];

         if ($this->MenuModel->insert($data)) {
            return $this->respondCreated();
         }
         return $this->fail($this->MenuModel->errors());
      }
  }

  public function load_form_edit()
   {
      $id = $this->request->getGet('id');
      $data = [
      ];
      $data['main'] = $this->MenuModel->find($id);
      $data['menu_category'] = $this->ionAuth->getMenuCategory()->getResult();
      return view('Modules\Menu\Views\edit', $data);
   }

   public function updated()
    {
      $id = $this->request->getPost('id');
      $request = $this->request->getPost();
      $this->rules();
      if ($this->validation->run($request) != TRUE) {
         return $this->respond([
            'status' => 400,
            'error' => 400,
            'messages' => $this->validation->listErrors()
         ], 400);
 
      }else{
         $data = [
   
               'name' => $this->request->getPost('name'),
               'description' => $this->request->getPost('description'),
               'icon' => $this->request->getPost('icon'),
               'position' => $this->request->getPost('position'),
               'url' => $this->request->getPost('url'),
               'category' => $this->request->getPost('category'),
         ];
         if ($this->MenuModel->update($id, $data)) {
               return $this->respondCreated();
         }
         return $this->fail($this->MenuModel->errors());
      }
    }

    public function delete($id)
    {
        $data = ['deleted_at' => date("Y-m-d h:i:s")];

      //   if ($found = $this->MenuModel->update($id, $data)) {
         if ($found = $this->MenuModel->where('id', $id)->delete()) {
            return $this->respondDeleted($found);
        }

        return $this->fail('Fail deleted');
    }

    private function rules(){
      $this->validation->setRules([
         'name' => [
            'label' => 'Name',
            'rules' => 'required|string|max_length[255]'
         ],
         'description' => [
            'label' => 'Description',
            'rules' => 'required|string'
         ],
         'icon' => [
            'label' => 'Icon',
            'rules' => 'required|string|max_length[50]'
         ],
         'position' => [
            'label' => 'Position',
            'rules' => 'required|numeric|max_length[11]'
         ],
         'url' => [
            'label' => 'Url',
            'rules' => 'required|string|max_length[255]'
         ],
         'category' => [
            'label' => 'Category',
            'rules' => 'required|string|max_length[50]'
         ],
      ]);
   }

}