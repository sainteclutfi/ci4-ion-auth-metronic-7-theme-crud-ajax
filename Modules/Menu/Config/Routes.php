<?php 
      if(!isset($routes))
      { 
          $routes = \Config\Services::routes(true);
      }
      
      $routes->group('menu', ['namespace' => 'Modules\Menu\Controllers'], function($routes)
      {
          $routes->get('/', 'Menu::index');
          $routes->get('edit', 'Menu::load_form_edit');
          $routes->get('add', 'Menu::load_form_add');
          $routes->post('updated', 'Menu::updated');
          $routes->post('addnew', 'Menu::new');
          $routes->post('data', 'Menu::data');
          $routes->resource('resources', [
              'controller' => 'Menu',
          ]);
      });
      