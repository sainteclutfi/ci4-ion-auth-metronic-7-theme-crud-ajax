<?php
namespace Modules\Menu\Models;
use CodeIgniter\Model;

class MenuModel extends Model{
   protected $table      = 'menu';
   protected $primaryKey = 'id';
   protected $useSoftDeletes = true;

   protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

   protected $allowedFields = ['name', 'description', 'icon', 'position', 'url', 'category', 'created_at', 'updated_at', 'deleted_at'];
   protected $searchFields = ['name', 'description', 'icon', 'position', 'url'];

   public function filter($search = null, $limit = null, $start = null, $orderField = null, $orderDir = null){
      $builder = $this->table($this->table);

      $i = 0;
      foreach ($this->searchFields as $column)
      {
            if($search)
            {
               if($i == 0)
               {
                  $builder->groupStart()
                          ->like($column, $search);
               }
               else
               {
                  $builder->orLike($column, $search);
               }

               if(count($this->searchFields) - 1 == $i) $builder->groupEnd();

            }
            $i++;
      }

      // Secara bawaan menampilkan data sebanyak kurang dari
      // atau sama dengan 7 kolom pertama.
      $builder->select('id, name, description, icon, position, url, category')
              ->where('deleted_at is NULL')
              ->orderBy($orderField, $orderDir)
              ->limit($limit, $start);

      $query = $builder;

      return $query;
   }

   public function countTotal(){
      return $this->table($this->table)
                  ->countAll();
   }

   public function countFilter($search){
      $builder = $this->table($this->table);

      $i = 0;
      foreach ($this->searchFields as $column)
      {
            if($search)
            {
               if($i == 0)
               {
                  $builder->groupStart()
                          ->like($column, $search);
               }
               else
               {
                  $builder->orLike($column, $search);
               }

               if(count($this->searchFields) - 1 == $i) $builder->groupEnd();

            }
            $i++;
      }

      return $builder->countAllResults();
   }

}