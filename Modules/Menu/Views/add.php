<form id="form-add" accept-charset="utf-8">
   <div class="form-group">
      <label for="name">Name</label>
      <input type="text" name="name" class="form-control" />
   </div>
   <div class="form-group">
      <label for="description">Description</label>
      <textarea type="text" name="description" class="form-control" ></textarea>
   </div>
   <div class="col-md-4">
      <div id="convert_example_1"></div>
   </div>
   <div class="form-group">
      <label for="icon">Icon</label>
      <input type="text" id="console" name="icon" class="form-control" >
   </div>
   <div class="form-group">
      <label for="position">Position</label>
      <input type="number" name="position" class="form-control" />
   </div>
   <div class="form-group">
      <label for="url">Url</label>
      <input type="text" name="url" class="form-control" />
   </div>
   <div class="form-group">
      <label for="category">Category</label>
      <select name="category" class="form-control">
         <?php foreach($menu_category as $r){?>
         <option value="<?php echo $r->kode ?>"><?php echo $r->name ?></option>
         <?php } ?>
      </select>
   </div>
   <div class="form-group">
      <button type="submit" class="submit btn btn-primary">Save</button>
      <button type="button" class="closed btn btn-secondary" data-dismiss="modal">Close</button>
      <label for="error"></label>
   </div>
</form>