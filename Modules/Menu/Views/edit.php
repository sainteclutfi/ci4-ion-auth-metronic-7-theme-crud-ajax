<form id="form-edit" accept-charset="utf-8">
   <div class="form-group">
      <input type="hidden" value="<?php echo $main['id']; ?>" name="id" class="form-control">
      <label for="name">Name</label>
      <input type="text" name="name" value="<?= !empty($main['name']) ? $main['name'] : '' ?>" class="form-control" />
   </div>
   <div class="form-group">
      <label for="description">Description</label>
      <textarea type="text" name="description" class="form-control" ><?= !empty($main['description']) ? $main['description'] : '' ?></textarea>
   </div>
   <div class="col-md-4">
      <div id="convert_example_1"></div>
   </div>
   <div class="form-group">
      <label for="icon">Icon</label>
      <input type="text" id="console" name="icon" value="<?= !empty($main['icon']) ? $main['icon'] : '' ?>" class="form-control" />
   </div>
   <div class="form-group">
      <label for="position">Position</label>
      <input type="text" name="position" value="<?= !empty($main['position']) ? $main['position'] : '' ?>" class="form-control" />
   </div>
   <div class="form-group">
      <label for="url">Url</label>
      <input type="text" name="url" value="<?= !empty($main['url']) ? $main['url'] : '' ?>" class="form-control" />
   </div>
   <div class="form-group">
      <label for="category">Category</label>
      <select name="category" class="form-control">
         <?php foreach($menu_category as $r){?>
         <option <?= $main['category'] == $r->kode ? 'selected' : '' ?> value="<?php echo $r->kode ?>"><?php echo $r->name ?></option>
         <?php } ?>
      </select>
   </div>
   <div class="form-group">
      <button type="submit" class="submit btn btn-primary">Save</button>
      <button type="button" class="closed btn btn-secondary" data-dismiss="modal">Close</button>
      <label for="error"></label>
   </div>
</form>