<?php
namespace Modules\Groups\Controllers;

use CodeIgniter\Controller;
use CodeIgniter\API\ResponseTrait;
use App\Controllers\BaseController;
use Modules\Groups\Models\GroupsModel;
class Groups extends BaseController{
  use ResponseTrait;

  protected $Groups;

  public function __construct() {
      $this->validation = \Config\Services::validation();
     $this->GroupsModel = new GroupsModel;
     $this->db = \Config\Database::connect();

  }

  function index(){
     $data = [
        'title' => 'Data Groups',
        'host' => site_url('groups/')
     ];
     echo view('Modules\Groups\Views\list', $data);
  }

  public function data(){

      $where = "";
      $request = esc($this->request->getPost());
      $namegroups = $request['namegroups'];
      $description = $request['description'];

      $cols = array();
      if (!empty($namegroups)) { $cols['name'] = $namegroups; }
      if (!empty($description)) { $cols['description'] = $description; }

      $search = $request['search']['value'];
      $limit = $request['length'];
      $start = $request['start'];

      $orderIndex = $request['order'][0]['column'];
      $orderFields = $request['columns'][$orderIndex]['data'];
      $orderDir = $request['order'][0]['dir'];

      $list = $this->GroupsModel->filter($cols, $search, $limit, $start, $orderFields, $orderDir)->get()->getResultObject();

      $data = array();
      $no = $start;
      foreach ($list as $r) {
            $no++;
            $row = array();
            $btn_actions = '<div class="btn-group btn-group-sm">
                                    <button class="btn btn-primary btn-edit" data-id="'.$r->id.'"><i class="fas fa-tasks"></i></button>
                                    <button class="btn btn-danger btn-delete" data-id="'.$r->id.'"><i class="fas fa-trash"></i></button>
                                </div>';
            $row[] = $no;
            $row[] = $r->name;
            $row[] = $r->description;
            $row[] = $btn_actions;
            $data[] = $row;

      }

      return $this->respond([
         'draw'            => $this->request->getPost('draw'),
         'recordsTotal'    => $this->GroupsModel->countTotal(),
         'recordsFiltered' => $this->GroupsModel->countFilter($search),
         'data'            => $data,
     ]);

  }

  public function load_form_add()
  {
   $data = [
   ];
   return view('Modules\Groups\Views\add', $data);
  }

  public function new()
  {
      $request = $this->request->getPost();
      $this->rules();
      if ($this->validation->run($request) != TRUE) {
         return $this->respond([
            'status' => 400,
            'error' => 400,
            'messages' => $this->validation->listErrors()
         ], 400);
 
      }else{
         $data = [
               'name' => $this->request->getPost('name'),
               'description' => $this->request->getPost('description'),
         ];

         if ($this->GroupsModel->insert($data)) {
            return $this->respondCreated();
         }
         return $this->fail($this->GroupsModel->errors());
      }
  }

  public function load_form_edit()
   {
      $id = $this->request->getGet('id');
      $data = [
      ];
      $data['menu'] = $this->ionAuth->getMenu()->getResult();
      $data['menu_category'] = $this->ionAuth->getMenuCategory()->getResult();
      $data['permission'] = $this->ionAuth->getPermission($id)->getResult();
      $data['main'] = $this->GroupsModel->find($id);
      return view('Modules\Groups\Views\edit', $data);
   }

   public function updated()
    {
      $id = $this->request->getPost('id');
      $request = $this->request->getPost();
      $this->rules();
      if ($this->validation->run($request) != TRUE) {
         return $this->respond([
            'status' => 400,
            'error' => 400,
            'messages' => $this->validation->listErrors()
         ], 400);
 
      }else{
         $data = [
   
               'name' => $this->request->getPost('name'),
               'description' => $this->request->getPost('description'),
         ];
         if ($this->GroupsModel->update($id, $data)) {
               $permission = $this->request->getPost('permission');

               if (isset($permission) && !empty($permission)) {

                  $this->ionAuth->removeFromPermission('', $id);
                  foreach ($permission as $key => $value) {
                     $this->ionAuth->addToPermission($key, $id);

                     $removeAction = $this->db->table('permission_action')
                                 ->delete(['permission_id' => (int)$key, 'group_id' => $id]);
                     if(is_array($value)){
                        foreach ($value as $key_action) {
                           
                           $this->db->table('permission_action')->insert([
                              'permission_id' => (float)$key,
                              'group_id'  => (float)$id,
                              'menu_action_id'  => (float)$key_action,
                                ]);
                        }
                     }
                  }

               }
               return $this->respondCreated();
         }
         return $this->fail($this->GroupsModel->errors());
      }
    }

    public function delete($id)
    {
        if ($found = $this->GroupsModel->delete($id)) {
            return $this->respondDeleted($found);
        }

        return $this->fail('Fail deleted');
    }

    private function rules(){
      $this->validation->setRules([
         'name' => [
            'label' => 'Name',
            'rules' => 'required|string|max_length[20]'
         ],
         'description' => [
            'label' => 'Description',
            'rules' => 'required|string|max_length[100]'
         ],
      ]);
   }

}