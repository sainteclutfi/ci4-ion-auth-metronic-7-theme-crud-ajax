<?php
namespace Modules\Groups\Models;
use CodeIgniter\Model;

class GroupsModel extends Model{
   protected $table      = 'groups';
   protected $primaryKey = 'id';
   protected $allowedFields = ['name', 'description'];
   protected $searchFields = ['name', 'description', ];

   public function filter($cols = null, $search = null, $limit = null, $start = null, $orderField = null, $orderDir = null){
      $builder = $this->table($this->table);

      $i = 0;
      foreach ($this->searchFields as $column)
      {
            if($search)
            {
               if($i == 0)
               {
                  $builder->groupStart()
                          ->like($column, $search);
               }
               else
               {
                  $builder->orLike($column, $search);
               }

               if(count($this->searchFields) - 1 == $i) $builder->groupEnd();

            }
            $i++;
      }

      if(!empty($cols)){
         foreach ($cols as $col => $value)
         {
            $builder->like($col, $value);
         }
      }

      // Secara bawaan menampilkan data sebanyak kurang dari
      // atau sama dengan 7 kolom pertama.
      $builder->select('id, name, description, ')
              ->orderBy($orderField, $orderDir)
              ->limit($limit, $start);

      $query = $builder;

      return $query;
   }

   public function countTotal(){
      return $this->table($this->table)
                  ->countAll();
   }

   public function countFilter($search){
      $builder = $this->table($this->table);

      $i = 0;
      foreach ($this->searchFields as $column)
      {
            if($search)
            {
               if($i == 0)
               {
                  $builder->groupStart()
                          ->like($column, $search);
               }
               else
               {
                  $builder->orLike($column, $search);
               }

               if(count($this->searchFields) - 1 == $i) $builder->groupEnd();

            }
            $i++;
      }

      return $builder->countAllResults();
   }

}