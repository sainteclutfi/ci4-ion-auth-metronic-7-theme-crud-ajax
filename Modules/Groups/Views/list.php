
<?= $this->extend('layout/main') ?>
<?= $this->section('content') ?>

   <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
        <div class="subheader py-5 py-lg-10 gutter-b subheader-transparent" id="kt_subheader" style="background-color: #663259; background-position: right bottom; background-size: auto 100%; background-repeat: no-repeat; background-image: url(<?php echo base_url(); ?>/theme/assets/media/svg/patterns/taieri.svg)">
            <div class="container d-flex flex-column">
                <!--begin::Title-->
                <div class="d-flex align-items-sm-end flex-column flex-sm-row mb-5">
                    <h2 class="d-flex align-items-center text-white mr-5 mb-0">Search</h2>
                    <span class="text-white opacity-60 font-weight-bold">Module Groups Management</span>
                </div>
                <!--end::Title-->
                <!--begin::Search Bar-->
                <div class="d-flex align-items-md-center mb-2 flex-column flex-md-row">
                    <div class="bg-white rounded p-4 d-flex flex-grow-1 flex-sm-grow-0">
                        <!--begin::Form-->
                        <form id="form-filter" class="form d-flex align-items-md-center flex-sm-row flex-column flex-grow-1 flex-sm-grow-0">
                            <!--begin::Input-->
                            <div class="d-flex align-items-center py-3 py-sm-0 px-sm-3">
                                <span class="svg-icon svg-icon-lg">
                                    <!--begin::Svg Icon | path:/metronic/theme/html/demo3/dist/assets/media/svg/icons/General/Search.svg-->
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <rect x="0" y="0" width="24" height="24"></rect>
                                            <path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                            <path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" fill="#000000" fill-rule="nonzero"></path>
                                        </g>
                                    </svg>
                                    <!--end::Svg Icon-->
                                </span>
                                <input type="text" id="namegroups" class="form-control border-0 font-weight-bold pl-2" placeholder="Name">
                            </div>
                            <!--end::Input-->
                            <!--begin::Input-->
                            <div class="d-flex align-items-center py-3 py-sm-0 px-sm-3">
                                <span class="svg-icon svg-icon-lg">
                                    <!--begin::Svg Icon | path:/metronic/theme/html/demo3/dist/assets/media/svg/icons/General/Search.svg-->
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <rect x="0" y="0" width="24" height="24"></rect>
                                            <path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                            <path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" fill="#000000" fill-rule="nonzero"></path>
                                        </g>
                                    </svg>
                                    <!--end::Svg Icon-->
                                </span>
                                <input type="text" id="description" class="form-control border-0 font-weight-bold pl-2" placeholder="Description">
                            </div>
                            <!--end::Input-->
                            <button type="button" id="kt_search" class="btn btn-dark font-weight-bold btn-hover-light-primary mt-3 mt-sm-0 px-7">Search</button>
                        </form>
                        <!--end::Form-->
                    </div>
                </div>
                <!--end::Search Bar-->
            </div>
        </div>
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Card-->
                <div class="card card-custom">
                    <div class="card-header">
                        <div class="card-title">
                            <span class="card-icon">
                                <i class="flaticon2-delivery-package text-primary"></i>
                            </span>
                            <h3 class="card-label">Module Groups</h3>
                        </div>
                        <div class="card-toolbar">
                            <!--begin::Button-->
                            <a id="create" class="btn btn-primary font-weight-bolder">New Record</a>
                            <!--end::Button-->
                        </div>
                    </div>
                    <div class="card-body">
                        <!--begin: Search Form-->
                        <!--begin: Datatable-->
                        <table class="table table-bordered table-hover table-checkable" id="data-table-book">
                            <thead>
                                <tr role="row">
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                        <!--end: Datatable-->
                    </div>
                </div>
                <!--end::Card-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
        <ul class="sticky-toolbar nav flex-column pl-2 pr-2 pt-3 pb-3 mt-4">
			<!--begin::Item-->
			<li class="nav-item mb-2" id="kt_demo_panel_toggle" data-toggle="tooltip" title="" data-placement="right" data-original-title="Advanced Filter">
				<a class="btn btn-sm btn-icon btn-bg-light btn-icon-success btn-hover-success" href="#">
					<i class="flaticon2-search-1 text-success"></i>
				</a>
			</li>
			<!--end::Item-->
		</ul>
        <div id="kt_demo_panel" class="offcanvas offcanvas-right p-10">
			<!--begin::Header-->
			<div class="offcanvas-header d-flex align-items-center justify-content-between pb-7" kt-hidden-height="46" style="">
				<h4 class="font-weight-bold m-0">Advanced Filter</h4>
				<a class="btn btn-xs btn-icon btn-light btn-hover-primary" id="kt_demo_panel_close">
					<i class="ki ki-close icon-xs text-muted"></i>
				</a>
			</div>
			<!--end::Header-->
			<!--begin::Content-->
			<div class="offcanvas-content">
				<!--begin::Wrapper-->
				<div class="offcanvas-wrapper mb-5 scroll-pull scroll ps ps--active-y" style="height: 491px; overflow: hidden;">
				    <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
                        <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
                    </div>
                    <div class="ps__rail-y" style="top: 0px; height: 491px; right: -2px;">
                        <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 40px;"></div>
                    </div>
                </div>
				<!--end::Wrapper-->
				<!--begin::Purchase-->
				<div class="offcanvas-footer" kt-hidden-height="38" style="">
					<a href="https://1.envato.market/EA4JP" target="_blank" class="btn btn-block btn-danger btn-shadow font-weight-bolder text-uppercase">Search</a>
				</div>
				<!--end::Purchase-->
			</div>
			<!--end::Content-->
		</div>
    </div>
         
    <!-- /.row -->
<?= $this->endSection() ?>

<!--begin::Page Vendors Styles(used by this page)-->
<?= $this->section('page-vendors-styles-js') ?>
<link href="<?= base_url() ?>/theme/assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
<?= $this->endSection() ?>

<!--begin::Page Vendors(used by this page)-->
<?= $this->section('page-vendors-js') ?>
<script src="<?= base_url() ?>/theme/assets/plugins/custom/datatables/datatables.bundle.js"></script>
<?= $this->endSection() ?>

<!--begin::Page Scripts(used by this page)-->
<?= $this->section('extra-js') ?>
<!-- <script src="/theme/assets/js/pages/crud/datatables/search-options/advanced-search.js"></script> -->
<script src="<?= base_url() ?>/js/Groups/Groups.js"></script>
<script>Page.init();</script>
<?= $this->endSection() ?>

