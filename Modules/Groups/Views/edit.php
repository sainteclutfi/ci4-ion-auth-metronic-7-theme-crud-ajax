<form id="form-edit" accept-charset="utf-8">
   <div class="form-group">
      <input type="hidden" value="<?php echo $main['id']; ?>" name="id" class="form-control">
      <label for="name">Name</label>
      <input type="text" name="name" value="<?= !empty($main['name']) ? $main['name'] : '' ?>" class="form-control" />
   </div>
   <div class="form-group">
      <input type="hidden" value="<?php echo $main['id']; ?>" name="id" class="form-control">
      <label for="description">Description</label>
      <input type="text" name="description" value="<?= !empty($main['description']) ? $main['description'] : '' ?>" class="form-control" />
   </div>
   
   <div class="form-group">
         <?php
         foreach($menu_category as $r){
            $trans = "category ='".$r->kode."'";
            $kode = $r->kode;
            ?>
            <div class="row">
               <?php if (count($menu) >= 1) { ?>
                  <div class="col-md-12">
                     <div class="accordion accordion-toggle-arrow" id="<?php echo $kode; ?>">
                        <div class="card">
                           <div class="card-header">
                              <div class="card-title collapsed" data-toggle="collapse" data-target="#<?php echo $kode; ?>1" aria-expanded="false"><i class="<?php echo $r->icon; ?>"></i> <?php echo $r->name; ?></div>
                           </div>
                           <div id="<?php echo $kode; ?>1" class="collapse" data-parent="#<?php echo $kode; ?>" style="">
                              <div class="card-body">
                              <div class="checkbox-inline">
                              
                                 <?php foreach ($menu as $row) { 
                                       if($row->category == $kode){
                                          $gID=$row->id;
                                          $checked = "";
                                    ?>

                                    <?php foreach($permission as $current_permission) {
                                             if ($gID == $current_permission->menu_id) {
                                                $checked= "checked";
                                             break;}
                                             }    ?>
                                    <label class="checkbox checkbox-outline checkbox-success">
                                       <input type="checkbox" name="<?php echo 'permission['.$row->id.']' ?>" <?php echo $checked; ?> value="<?php echo $row->id ?>">
                                    <span></span>&nbsp; <i class="<?php echo $row->icon; ?>"></i> &nbsp;<?php echo $row->name ?>
                                    </label>
                                    <div class="col-9 col-form-label">
                                       <?php 
                                          $groupId = $main['id'];
                                          $menuId = $row->id;
                                          $this->ionAuth = new \IonAuth\Libraries\IonAuth();
                                          $menuAction = $this->ionAuth->getMenuAction('menu_id = "'.$row->id.'"')->getResult();
                                          
                                          $n=1; foreach ($menuAction as $ma) {
                                             $checkedAct = "";
                                             $ActID = $ma->id;
                                             $cekcurrentAction = $this->ionAuth->getPermissionAction($menuId, $ActID, $groupId)->countAllResults();
                                             
                                        ?>
                                        <?php     
                                                if ($cekcurrentAction > 0){
                                                   $currentAction = $this->ionAuth->getPermissionAction($menuId, $ActID, $groupId)->get()->getRow();
                                                      if ($ActID == $currentAction->menu_action_id) {
                                                         $checkedAct = "checked";
                                                      }else{
                                                         $checkedAct = "";
                                                      }
                                                }
                                          
                                          ?>
                                           <label class="checkbox checkbox-outline checkbox-success">
                                             <input type="checkbox" <?php echo $checkedAct; ?> name="<?php echo 'permission['.$row->id.']['.$ma->id.']' ?>" value="<?php echo $ma->id; ?>">
                                             <span></span>&nbsp; <i class="<?php echo $ma->icon; ?>"></i> &nbsp;<?php echo $ma->name; ?>
                                          </label>
                                       <?php $n++; } ?>
                                    </div>
                                    <?php } ?>
                                 <?php } ?>
                              </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               <?php } ?>
            </div>
            <br>
         <?php } ?>
   </div>
   <div class="form-group">
      <button type="submit" class="submit btn btn-primary font-weight-bold">Save</button>
      <button type="button" class="closed btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
      <label for="error"></label>
   </div>
</form>