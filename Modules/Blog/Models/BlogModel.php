<?php

namespace Modules\Blog\Models;

// use App\Entities\BlogEntity;
use CodeIgniter\Model;

class BlogModel extends Model
{
    protected $table = 'blog';
    protected $primaryKey = 'id';
    // protected $returnType = BlogEntity::class;
    protected $useSoftDeletes = true;
    protected $allowedFields = ['title', 'author', 'description', 'status_id'];
    protected $useTimestamps = true;
    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $deletedField = 'deleted_at';

    protected $validationRules = [
        'status_id'   => 'required|numeric',
        'title'       => 'required|min_length[10]|max_length[60]',
        'author'      => 'required',
        'description' => 'required|min_length[10]|max_length[200]',
    ];
    protected $validationMessages = [];
    protected $skipValidation = false;

    const ORDERABLE = [
        1 => 'title',
        2 => 'author',
        3 => 'description',
        4 => 'status',
    ];

    public $orderable = ['title', 'author', 'description', 'status'];

    /**
     * Get resource data.
     *
     * @param string $search
     *
     * @return \CodeIgniter\Database\BaseBuilder
     */
    public function getResource(string $search = '')
    {
        $builder = $this->builder()
            ->select('blog.id, blog.title, blog.author, blog.description, blog.created_at, status.status')
            ->join('status', 'blog.status_id = status.id');
            // $builder->where($where);
            $condition = empty($search)
            ? $builder
            : $builder->groupStart()
            ->like('title', $search)
            ->orLike('author', $search)
            ->orLike('description', $search)
            ->orLike('status', $search)
            ->groupEnd();

        return $condition->where([
            'blog.deleted_at'  => null,
            'status.deleted_at' => null,
        ]);
    }
}
