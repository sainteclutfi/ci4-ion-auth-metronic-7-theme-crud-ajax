<?php namespace Modules\Blog\Controllers;

use Modules\Blog\Models\BlogModel;
use CodeIgniter\API\ResponseTrait;
use CodeIgniter\Controller;
use App\Controllers\BaseController;
use RouterOS\Client;
use RouterOS\Query;

class Blog extends BaseController
{

	use ResponseTrait;
	
	/**
     * @var Blogmodel
     */
    protected $blogmodel;

    public function __construct()
    {
        $this->blogmodel = new BlogModel();
    }

    /**
     * Tampilkan daftar index.
     *
     * @return \CodeIgniter\Http\Response
     */
    public function index()
    {
        if (! $this->isAuthorized())
		{
			return redirect()->to('/');
		}
        return view('Modules\Blog\Views\BlogView');
    }

    public function routeros()
    {
        

        // Initiate client with config object
        $client = new Client([
            'host' => '182.23.8.101',
            'user' => 'lutfi',
            'pass' => 'testm1kr0t1k'
        ]);
        
        // cara get user by id
        $pprofile = (new Query('/ip/hotspot/user/print'))
                        ->where('name', 'vektorlutfi12345');
                        $getprofile = $client->query($pprofile)->read();
                        // $idp = $getprofile[0]['.id'];

        print("<pre>".print_r($getprofile ,true)."</pre>");

        // cara ganti password by id
        $query =(new Query('/ip/hotspot/user/set'))
        ->equal('.id', '*2')
        ->equal('password', 'fandyupdatepass');
        $update = $client->query($query)->read();

        print("<pre>".print_r($update ,true)."</pre>");

        
        // cara tambah user
        // $username = 'vektorlutfi2';
        // $password = 'tes123';
        // $adduser = $client->query([
        //     '/ip/hotspot/user/add',  
        //     '=name='.$username,
        //     '=password='.$password
        //     ])->read();
        //     print("<pre>".print_r($adduser,true)."</pre>");
        

        
        // cara hapus user
        // $client->query(['/ip/hotspot/user/remove', '=.id=*4'])->read();

        

        $data = $client->query('/ip/hotspot/user/print')->read();
        // $user = collect($data)->except(['0'])->toArray(); 
        // $aktif = $client->query('/ip/hotspot/active/print')->read();

        print("<pre>".print_r($data,true)."</pre>");
        // print("<pre>".print_r($aktif,true)."</pre>");
        // print("<pre>".print_r($data,true)."</pre>");
    }

    /**
     * Function datatable.
     *
     * @return CodeIgniter\Http\Response
     */
    public function datatables()
    {
        if ($this->request->isAJAX()) {
            $where = "blog.status_id = '1'";

            $start = $this->request->getPost('start');
            $length = $this->request->getPost('length');
            $search = $this->request->getPost('search[value]');
            $order = BlogModel::ORDERABLE[$this->request->getPost('order[0][column]')];
            $dir = $this->request->getPost('order[0][dir]');

            $list = $this->blogmodel->getResource($search)
                                    ->where($where)
                                    ->orderBy($order, $dir)
                                    ->limit($length, $start)
                                    ->get()->getResultObject();
            
            $data = array();
            $no = $start;
            foreach ($list as $r) {
                $no++;
                $row = array();
                $btn_actions = '<div class="btn-group btn-group-sm">
                                    <button class="btn btn-primary btn-edit" data-id="'.$r->id.'"><i class="fas fa-pencil-alt"></i></button>
                                    <button class="btn btn-danger btn-delete" data-id="'.$r->id.'"><i class="fas fa-trash"></i></button>
                                </div>';
                $status = '';
                if($r->status == 'Publish'){
                    $status = '<span class="right badge badge-success">'.$r->status.'</span>';
                }else if($r->status == 'Pending'){
                    $status = '<span class="right badge badge-info">'.$r->status.'</span>';
                }else if($r->status == 'Draft'){
                    $status = '<span class="right badge badge-warning">'.$r->status.'</span>';
                }

                $row[] = $no;
                $row[] = $r->title;
                $row[] = $r->author;
                $row[] = $r->description;
                $row[] = $status;
                $row[] = $btn_actions;
                $data[] = $row;
            }
        
            return $this->respond([
                'draw'            => $this->request->getPost('draw'),
                'recordsTotal'    => $this->blogmodel->getResource()->where($where)->countAllResults(),
                'recordsFiltered' => $this->blogmodel->getResource($search)->where($where)->countAllResults(),
                'data'            => $data,
            ]);
        }

        return $this->respondNoContent();
    }

    public function load_form_add()
    {
        return view('Modules\Blog\Views\add');
    }

    public function add_new()
    {
        $id = $this->request->getPost('id');
        $data = [
            'title' => $this->request->getPost('title'),
            'author' => $this->request->getPost('author'),
            'description' => $this->request->getPost('description'),
            'status_id' => $this->request->getPost('status_id')
        ];

        if ($this->blogmodel->insert($data)) {
            return $this->respondCreated();
        }
        return $this->fail($this->blogmodel->errors());
    }

    public function load_form_edit()
    {
        $id = $this->request->getGet('id');
        $data['main'] = $this->blogmodel->find($id);
        return view('Modules\Blog\Views\edit',$data);
    }

    public function simpan()
    {
        $id = $this->request->getPost('id');
        $data = [
            'title' => $this->request->getPost('title'),
            'author' => $this->request->getPost('author'),
            'description' => $this->request->getPost('description'),
            'status_id' => $this->request->getPost('status_id')
        ];

        if ($this->blogmodel->update($id, $data)) {
            return $this->respondCreated();
        }
        return $this->fail($this->blogmodel->errors());
    }

    /**
     * Hapus resource spesifik ke database.
     *
     * @param int $id
     *
     * @return CodeIgniter\Http\Response
     */
    public function delete($id)
    {
        if ($found = $this->blogmodel->delete($id)) {
            return $this->respondDeleted($found);
        }

        return $this->fail('Fail deleted');
    }

    

}
