<?php 
/**
 * --------------------------------------------------------------------
 * HMVC Routing
 * --------------------------------------------------------------------
 */
if(!isset($routes))
{ 
    $routes = \Config\Services::routes(true);
}

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.

$routes->group('blog', ['namespace' => 'Modules\Blog\Controllers'], function($routes)
{
    $routes->get('/', 'Blog::index');
    $routes->add('routeros', 'Blog::routeros');
    $routes->get('edit', 'Blog::load_form_edit');
    $routes->get('add', 'Blog::load_form_add');
    $routes->post('simpan', 'Blog::simpan');
    $routes->post('addnew', 'Blog::add_new');
    $routes->post('datatables', 'Blog::datatables', ['as' => 'datatables']);
    $routes->resource('resources', [
        'controller' => 'Blog',
    ]);
});

