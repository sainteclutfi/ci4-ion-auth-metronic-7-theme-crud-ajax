<form id="form-add-books">
    <div class="kt-portlet__body">
		<div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <input type="text" class="form-control" name="title" placeholder="Title of book">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="author" placeholder="Author for book">
                </div>
                <div class="form-group">
                    <select class="form-control" id="status" name="status_id">
                        <option id="publish" value="1">Publish</option>
                        <option id="pending" value="2">Pending</option>
                        <option id="draft" value="3">Draft</option>
                    </select>
                </div>
                <div class="form-group">
                    <textarea class="form-control" name="description" placeholder="Enter ..."></textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
		<button type="button" class="closed btn btn-clean btn-bold btn-upper btn-font-md" data-dismiss="modal">Close</button>
		<button type="submit" class="submit btn btn-primary btn-bold btn-upper btn-font-md">Save</button>
	</div>
</form>
