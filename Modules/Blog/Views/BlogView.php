<?= $this->extend('layout/main') ?>

<?= $this->section('content') ?>
    <?php //echo view('Modules\Blog\Views\parts\modals'); ?>
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
        <div class="subheader py-3 py-lg-8 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h2 class="d-flex align-items-center text-dark font-weight-bold my-1 mr-3">Advanced Search Examples</h2>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold my-2 p-0">
                            <li class="breadcrumb-item">
                                <a href="" class="text-muted">Datatables.net</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="" class="text-muted">Search Options</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="" class="text-muted">Advanced Search</a>
                            </li>

                            <?php $request = \Config\Services::request();

                            $uri = $request->uri;

                            // echo $uri->getScheme();         // http
                            // echo $uri->getAuthority();      // snoopy:password@example.com:88
                            // echo $uri->getUserInfo();       // snoopy:password
                            // echo $uri->getHost();           // example.com
                            // echo $uri->getPort();           // 88
                            // echo $uri->getPath();           // /path/to/page
                            // echo $uri->getQuery();          // foo=bar&bar=baz
                            //echo $uri->getSegments();       // ['path', 'to', 'page']
                            echo $uri->getSegment(1);       // 'path'
                            // echo $uri->getTotalSegments();  // 3 
                            // $router = service('router'); 
                            // echo $controller  = $router->controllerName();
                            ?>

                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
            </div>
        </div>
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Notice-->
                <div class="alert alert-custom alert-white alert-shadow gutter-b" role="alert">
                    <div class="alert-icon">
                        <span class="svg-icon svg-icon-primary svg-icon-xl">
                            <!--begin::Svg Icon | path:assets/media/svg/icons/Tools/Compass.svg-->
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect x="0" y="0" width="24" height="24" />
                                    <path d="M7.07744993,12.3040451 C7.72444571,13.0716094 8.54044565,13.6920474 9.46808594,14.1079953 L5,23 L4.5,18 L7.07744993,12.3040451 Z M14.5865511,14.2597864 C15.5319561,13.9019016 16.375416,13.3366121 17.0614026,12.6194459 L19.5,18 L19,23 L14.5865511,14.2597864 Z M12,3.55271368e-14 C12.8284271,3.53749572e-14 13.5,0.671572875 13.5,1.5 L13.5,4 L10.5,4 L10.5,1.5 C10.5,0.671572875 11.1715729,3.56793164e-14 12,3.55271368e-14 Z" fill="#000000" opacity="0.3" />
                                    <path d="M12,10 C13.1045695,10 14,9.1045695 14,8 C14,6.8954305 13.1045695,6 12,6 C10.8954305,6 10,6.8954305 10,8 C10,9.1045695 10.8954305,10 12,10 Z M12,13 C9.23857625,13 7,10.7614237 7,8 C7,5.23857625 9.23857625,3 12,3 C14.7614237,3 17,5.23857625 17,8 C17,10.7614237 14.7614237,13 12,13 Z" fill="#000000" fill-rule="nonzero" />
                                </g>
                            </svg>
                            <!--end::Svg Icon-->
                        </span>
                    </div>
                    <div class="alert-text">This example is almost identical to text based individual column example and provides the same functionality. With server-side processing enabled, all paging, searching, ordering actions that DataTables performs are handed off to a server where an SQL engine (or similar) can perform these actions on the large data set. See official documentation
                    <a class="font-weight-bold" href="https://datatables.net/examples/api/multi_filter_select.html" target="_blank">here</a>.</div>
                </div>
                <!--end::Notice-->
                <!--begin::Card-->
                <div class="card card-custom">
                    <div class="card-header">
                        <div class="card-title">
                            <span class="card-icon">
                                <i class="flaticon2-delivery-package text-primary"></i>
                            </span>
                            <h3 class="card-label">Advanced Search Form</h3>
                        </div>
                        <div class="card-toolbar">
                            <!--begin::Dropdown-->
                            <div class="dropdown dropdown-inline mr-2">
                                <button type="button" class="btn btn-light-primary font-weight-bolder dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="svg-icon svg-icon-md">
                                    <!--begin::Svg Icon | path:assets/media/svg/icons/Design/PenAndRuller.svg-->
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <rect x="0" y="0" width="24" height="24" />
                                            <path d="M3,16 L5,16 C5.55228475,16 6,15.5522847 6,15 C6,14.4477153 5.55228475,14 5,14 L3,14 L3,12 L5,12 C5.55228475,12 6,11.5522847 6,11 C6,10.4477153 5.55228475,10 5,10 L3,10 L3,8 L5,8 C5.55228475,8 6,7.55228475 6,7 C6,6.44771525 5.55228475,6 5,6 L3,6 L3,4 C3,3.44771525 3.44771525,3 4,3 L10,3 C10.5522847,3 11,3.44771525 11,4 L11,19 C11,19.5522847 10.5522847,20 10,20 L4,20 C3.44771525,20 3,19.5522847 3,19 L3,16 Z" fill="#000000" opacity="0.3" />
                                            <path d="M16,3 L19,3 C20.1045695,3 21,3.8954305 21,5 L21,15.2485298 C21,15.7329761 20.8241635,16.200956 20.5051534,16.565539 L17.8762883,19.5699562 C17.6944473,19.7777745 17.378566,19.7988332 17.1707477,19.6169922 C17.1540423,19.602375 17.1383289,19.5866616 17.1237117,19.5699562 L14.4948466,16.565539 C14.1758365,16.200956 14,15.7329761 14,15.2485298 L14,5 C14,3.8954305 14.8954305,3 16,3 Z" fill="#000000" />
                                        </g>
                                    </svg>
                                    <!--end::Svg Icon-->
                                </span>Export</button>
                                <!--begin::Dropdown Menu-->
                                <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                                    <!--begin::Navigation-->
                                    <ul class="navi flex-column navi-hover py-2">
                                        <li class="navi-header font-weight-bolder text-uppercase font-size-sm text-primary pb-2">Choose an option:</li>
                                        <li class="navi-item">
                                            <a href="#" class="navi-link">
                                                <span class="navi-icon">
                                                    <i class="la la-print"></i>
                                                </span>
                                                <span class="navi-text">Print</span>
                                            </a>
                                        </li>
                                        <li class="navi-item">
                                            <a href="#" class="navi-link">
                                                <span class="navi-icon">
                                                    <i class="la la-copy"></i>
                                                </span>
                                                <span class="navi-text">Copy</span>
                                            </a>
                                        </li>
                                        <li class="navi-item">
                                            <a href="#" class="navi-link">
                                                <span class="navi-icon">
                                                    <i class="la la-file-excel-o"></i>
                                                </span>
                                                <span class="navi-text">Excel</span>
                                            </a>
                                        </li>
                                        <li class="navi-item">
                                            <a href="#" class="navi-link">
                                                <span class="navi-icon">
                                                    <i class="la la-file-text-o"></i>
                                                </span>
                                                <span class="navi-text">CSV</span>
                                            </a>
                                        </li>
                                        <li class="navi-item">
                                            <a href="#" class="navi-link">
                                                <span class="navi-icon">
                                                    <i class="la la-file-pdf-o"></i>
                                                </span>
                                                <span class="navi-text">PDF</span>
                                            </a>
                                        </li>
                                    </ul>
                                    <!--end::Navigation-->
                                </div>
                                <!--end::Dropdown Menu-->
                            </div>
                            <!--end::Dropdown-->
                            <!--begin::Button-->
                            <a id="create-books" class="btn btn-primary font-weight-bolder">New Record</a>
                            <!--end::Button-->
                        </div>
                    </div>
                    <div class="card-body">
                        <!--begin: Search Form-->
                        <form class="mb-15">
                            <div class="row mb-6">
                                <div class="col-lg-3 mb-lg-0 mb-6">
                                    <label>RecordID:</label>
                                    <input type="text" class="form-control datatable-input" placeholder="E.g: 4590" data-col-index="0" />
                                </div>
                                <div class="col-lg-3 mb-lg-0 mb-6">
                                    <label>OrderID:</label>
                                    <input type="text" class="form-control datatable-input" placeholder="E.g: 37000-300" data-col-index="1" />
                                </div>
                                <div class="col-lg-3 mb-lg-0 mb-6">
                                    <label>Country:</label>
                                    <select class="form-control datatable-input" data-col-index="2">
                                        <option value="">Select</option>
                                    </select>
                                </div>
                                <div class="col-lg-3 mb-lg-0 mb-6">
                                    <label>Agent:</label>
                                    <input type="text" class="form-control datatable-input" placeholder="Agent ID or name" data-col-index="4" />
                                </div>
                            </div>
                            <div class="row mb-8">
                                <div class="col-lg-3 mb-lg-0 mb-6">
                                    <label>Ship Date:</label>
                                    <div class="input-daterange input-group" id="kt_datepicker">
                                        <input type="text" class="form-control datatable-input" name="start" placeholder="From" data-col-index="5" />
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-ellipsis-h"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control datatable-input" name="end" placeholder="To" data-col-index="5" />
                                    </div>
                                </div>
                                <div class="col-lg-3 mb-lg-0 mb-6">
                                    <label>Status:</label>
                                    <select class="form-control datatable-input" data-col-index="6">
                                        <option value="">Select</option>
                                    </select>
                                </div>
                                <div class="col-lg-3 mb-lg-0 mb-6">
                                    <label>Type:</label>
                                    <select class="form-control datatable-input" data-col-index="7">
                                        <option value="">Select</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row mt-8">
                                <div class="col-lg-12">
                                <button class="btn btn-primary btn-primary--icon" id="kt_search">
                                    <span>
                                        <i class="la la-search"></i>
                                        <span>Search</span>
                                    </span>
                                </button>&#160;&#160;
                                <button class="btn btn-secondary btn-secondary--icon" id="kt_reset">
                                    <span>
                                        <i class="la la-close"></i>
                                        <span>Reset</span>
                                    </span>
                                </button></div>
                            </div>
                        </form>
                        <!--begin: Datatable-->
                        <!--begin: Datatable-->
                        <table class="table table-bordered table-hover table-checkable" id="data-table-book">
                            <thead>
                                <tr role="row">
                                    <th>No</th>
                                    <th>Title</th>
                                    <th>Author</th>
                                    <th>Description</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                        <!--end: Datatable-->
                    </div>
                </div>
                <!--end::Card-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>

<!-- /.row -->
<?= $this->endSection() ?>

<!--begin::Page Vendors Styles(used by this page)-->
<?= $this->section('page-vendors-styles-js') ?>
<link href="<?= base_url() ?>/theme/assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
<?= $this->endSection() ?>

<!--begin::Page Vendors(used by this page)-->
<?= $this->section('page-vendors-js') ?>
<script src="<?= base_url() ?>/theme/assets/plugins/custom/datatables/datatables.bundle.js"></script>
<?= $this->endSection() ?>

<!--begin::Page Scripts(used by this page)-->
<?= $this->section('extra-js') ?>
<!-- <script src="/theme/assets/js/pages/crud/datatables/search-options/advanced-search.js"></script> -->
<script src="<?= base_url() ?>/js/blog/blog.js"></script>
<script>Page.init();</script>
<?= $this->endSection() ?>