<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Createservice_workorderpartTable extends Migration {

	public function up() {

		## Create Table service_workorderpart
		$this->forge->addField(array(
			'QuantitySupplied' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'Oid' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => FALSE,

			),
			'WorkOrder' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'Job' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'Part' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'Remark' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'Quantity' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'ServiceRate' => array(
				'type' => 'DOUBLE',
				'null' => TRUE,

			),
			'UnitPrice' => array(
				'type' => 'DECIMAL',
				'constraint' => 19,4,
				'null' => TRUE,

			),
			'Program' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'ProgramPercentage' => array(
				'type' => 'DOUBLE',
				'null' => TRUE,

			),
			'ProgramAmount' => array(
				'type' => 'DECIMAL',
				'constraint' => 19,4,
				'null' => TRUE,

			),
			'Discount' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'DiscountPercentage' => array(
				'type' => 'DOUBLE',
				'null' => TRUE,

			),
			'VATCode' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'WithholdingTax' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'Billed' => array(
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			),
			'ProgramBilled' => array(
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			),
			'PartStatus' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'ETA' => array(
				'type' => 'DATETIME',
				'null' => TRUE,

			),
			'Insurance' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'InsuranceApproved' => array(
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			),
			'OrderProcessed' => array(
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			),
			'PartIsPicked' => array(
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			),
			'ServicePartBackOrder' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'DiscountApproved' => array(
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			),
			'OptimisticLockField' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'GCRecord' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'PartInsuranceAmount' => array(
				'type' => 'DECIMAL',
				'constraint' => 19,4,
				'null' => TRUE,

			),
		));
		$this->forge->addPrimaryKey("Oid",true);
		$this->forge->createTable("service_workorderpart", false,['ENGINE' => 'InnoDB'] );

	 }

	public function down()	{
		### Drop table service_workorderpart ##
		$this->forge->dropTable("service_workorderpart", TRUE);

	}
}