<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreatemenuTable extends Migration {

	public function up() {

		## Create Table menu
		$this->forge->addField(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'name' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null' => TRUE,

			),
			'description' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'icon' => array(
				'type' => 'VARCHAR',
				'constraint' => 50,
				'null' => TRUE,

			),
			'position' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'url' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null' => TRUE,

			),
			'category' => array(
				'type' => 'VARCHAR',
				'constraint' => 50,
				'null' => TRUE,

			),
			'created_at' => array(
				'type' => 'DATETIME',
				'null' => TRUE,

			),
			'updated_at' => array(
				'type' => 'DATETIME',
				'null' => TRUE,

			),
			'deleted_at' => array(
				'type' => 'DATETIME',
				'null' => TRUE,

			),
		));
		$this->forge->addPrimaryKey("id",true);
		$this->forge->createTable("menu", false,['ENGINE' => 'InnoDB'] );

	 }

	public function down()	{
		### Drop table menu ##
		$this->forge->dropTable("menu", TRUE);

	}
}