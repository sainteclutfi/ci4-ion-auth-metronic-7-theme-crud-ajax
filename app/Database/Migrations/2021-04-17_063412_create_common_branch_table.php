<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Createcommon_branchTable extends Migration {

	public function up() {

		## Create Table common_branch
		$this->forge->addField(array(
			'BranchId' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => FALSE,

			),
			'BranchName' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'ShortBranchName2' => array(
				'type' => 'VARCHAR',
				'constraint' => 5,
				'null' => TRUE,

			),
			'BranchType' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'LocationStatus' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'BranchAddress' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'PhoneNo' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'PhoneNoSMSGateway' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'Email' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'IsSPLD' => array(
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			),
			'ReceptionLeadTime' => array(
				'type' => 'DOUBLE',
				'null' => TRUE,

			),
			'ServiceLeadTime' => array(
				'type' => 'DOUBLE',
				'null' => TRUE,

			),
			'PartPickingLeadTime' => array(
				'type' => 'DOUBLE',
				'null' => TRUE,

			),
			'FinalInspectionLeadTime' => array(
				'type' => 'DOUBLE',
				'null' => TRUE,

			),
			'DocumentCheckingLeadTime' => array(
				'type' => 'DOUBLE',
				'null' => TRUE,

			),
			'InvoicingLeadTime' => array(
				'type' => 'DOUBLE',
				'null' => TRUE,

			),
			'CallCustomerNotificationLeadTime' => array(
				'type' => 'DOUBLE',
				'null' => TRUE,

			),
			'WashingLeadTime' => array(
				'type' => 'DOUBLE',
				'null' => TRUE,

			),
			'DeliveryLeadTime' => array(
				'type' => 'DOUBLE',
				'null' => TRUE,

			),
			'ClockOnLeadTime' => array(
				'type' => 'DOUBLE',
				'null' => TRUE,

			),
			'ClockOffLeadTime' => array(
				'type' => 'DOUBLE',
				'null' => TRUE,

			),
			'DADDataRange' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'DeadStock' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'DemandFrequency' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'DemandDuration' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'StockPolicyPriceLimit' => array(
				'type' => 'DECIMAL',
				'constraint' => 19,4,
				'null' => TRUE,

			),
			'ReturnDaysLimit' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'ReturnPriceLimit' => array(
				'type' => 'DECIMAL',
				'constraint' => 19,4,
				'null' => TRUE,

			),
			'BinningLimitDays' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'Region' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'TaxCodeOnSales' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'TaxCodeOnPurchase' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'WithHoldingTaxCode' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'IsRootBranch' => array(
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			),
			'DefaultPettyCashAccount' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'CashierStartingAmount' => array(
				'type' => 'DECIMAL',
				'constraint' => 19,4,
				'null' => TRUE,

			),
			'CashierBalanceClear' => array(
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			),
			'DeadlinePeriodicService' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'MaximumReturnDownPayment' => array(
				'type' => 'DOUBLE',
				'null' => TRUE,

			),
			'ParentBranch' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'OptimisticLockField' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'GCRecord' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'DeliveryCode' => array(
				'type' => 'VARCHAR',
				'constraint' => 5,
				'null' => TRUE,

			),
			'PartInTransitAccount' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'ShortBranchNameDataTransfer' => array(
				'type' => 'VARCHAR',
				'constraint' => 4,
				'null' => TRUE,

			),
			'ShortBranchNameDataTransferOracle' => array(
				'type' => 'VARCHAR',
				'constraint' => 4,
				'null' => TRUE,

			),
			'ShortBranchName' => array(
				'type' => 'VARCHAR',
				'constraint' => 4,
				'null' => TRUE,

			),
			'ToyotaBranchCode' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'ToyotaBranchCodeAFI' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'ToyotaAuthorized' => array(
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			),
			'ParentToyotaBranchCode' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'PrincipalAuthorized' => array(
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			),
			'PrincipalBranchCode' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'ParentPrincipalBranchCode' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'DefaultLocation' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'DefaultIntransitLocation' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'OrganizationalUnitTypeForTransferRequest' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'Province' => array(
				'type' => 'VARCHAR',
				'constraint' => 50,
				'null' => TRUE,

			),
			'City' => array(
				'type' => 'VARCHAR',
				'constraint' => 50,
				'null' => TRUE,

			),
			'MatriksDR' => array(
				'type' => 'INT',
				'constraint' => 2,
				'null' => TRUE,

			),
			'OutlateCode' => array(
				'type' => 'VARCHAR',
				'constraint' => 10,
				'null' => TRUE,

			),
		));
		$this->forge->addPrimaryKey("BranchId",true);
		$this->forge->addKey('blog_name');
		$this->forge->addKey('DefaultPettyCashAccount');
		$this->forge->addKey('DeliveryCode');
		$this->forge->addKey('GCRecord');
		$this->forge->addKey('ParentBranch');
		$this->forge->addKey('PartInTransitAccount');
		$this->forge->addKey('Region');
		$this->forge->addKey('TaxCodeOnPurchase');
		$this->forge->addKey('TaxCodeOnSales');
		$this->forge->addKey('WithHoldingTaxCode');
		$this->forge->addKey('DefaultLocation');
		$this->forge->addKey('DefaultIntransitLocation');	
		$this->forge->createTable("common_branch", false,['ENGINE' => 'InnoDB'] );

	 }

	public function down()	{
		### Drop table common_branch ##
		$this->forge->dropTable("common_branch", TRUE);

	}
}