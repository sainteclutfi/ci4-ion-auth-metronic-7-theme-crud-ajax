<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Createhr_organizationalunitTable extends Migration {

	public function up() {

		## Create Table hr_organizationalunit
		$this->forge->addField(array(
			'OrganizationalUnitId' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => FALSE,

			),
			'UnitName' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'OrganizationalUnitType' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'UnitGroup' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'Position' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'Description' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'Branch' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'NoOfNeeds' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'ParentOrganizationalUnit' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'HeadOfOrganizationalUnit' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'Goal' => array(
				'type' => 'LONGTEXT',
				'null' => TRUE,

			),
			'Resposibility' => array(
				'type' => 'LONGTEXT',
				'null' => TRUE,

			),
			'IndicatorOfSuccess' => array(
				'type' => 'LONGTEXT',
				'null' => TRUE,

			),
			'FinanceRelated' => array(
				'type' => 'LONGTEXT',
				'null' => TRUE,

			),
			'NonFinanceRelated' => array(
				'type' => 'LONGTEXT',
				'null' => TRUE,

			),
			'InternalRelation' => array(
				'type' => 'LONGTEXT',
				'null' => TRUE,

			),
			'ExternalRelation' => array(
				'type' => 'LONGTEXT',
				'null' => TRUE,

			),
			'JobEnvironment' => array(
				'type' => 'LONGTEXT',
				'null' => TRUE,

			),
			'JobRisk' => array(
				'type' => 'LONGTEXT',
				'null' => TRUE,

			),
			'JobBarrier' => array(
				'type' => 'LONGTEXT',
				'null' => TRUE,

			),
			'Tools' => array(
				'type' => 'LONGTEXT',
				'null' => TRUE,

			),
			'OptimisticLockField' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'GCRecord' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
		));
		$this->forge->addPrimaryKey("OrganizationalUnitId",true);
		$this->forge->addKey('GCRecord');
		$this->forge->addKey('UnitGroup');
		$this->forge->addKey('Position');
		$this->forge->addKey('Branch');
		$this->forge->addKey('ParentOrganizationalUnit');
		$this->forge->addKey('HeadOfOrganizationalUnit');
		$this->forge->createTable("hr_organizationalunit", false,['ENGINE' => 'InnoDB'] );

	 }

	public function down()	{
		### Drop table hr_organizationalunit ##
		$this->forge->dropTable("hr_organizationalunit", TRUE);

	}
}