<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Createcommon_districtTable extends Migration {

	public function up() {

		## Create Table common_district
		$this->forge->addField(array(
			'Oid' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'DistrictName' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'City' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'OptimisticLockField' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'GCRecord' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'rowguid' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => FALSE,

			),
		));
		$this->forge->addPrimaryKey("Oid",true);
		$this->forge->createTable("common_district", false,['ENGINE' => 'InnoDB'] );

	 }

	public function down()	{
		### Drop table common_district ##
		$this->forge->dropTable("common_district", TRUE);

	}
}