<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Createsales_vehicleunitTable extends Migration {

	public function up() {

		## Create Table sales_vehicleunit
		$this->forge->addField(array(
			'ActiveServiceBooking' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'DefaultContact' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'Oid' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => FALSE,

			),
			'RRN' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'VinCode' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'VehicleRegistrationNumber' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'RegistrationDate' => array(
				'type' => 'DATETIME',
				'null' => TRUE,

			),
			'RegistrationExpiryDate' => array(
				'type' => 'DATETIME',
				'null' => TRUE,

			),
			'EngineSerialNo' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'FrameSerialNo' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'LicensePlateNumber' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'VehicleDeliveryDate' => array(
				'type' => 'DATETIME',
				'null' => TRUE,

			),
			'ProductionYear' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'TWC' => array(
				'type' => 'DATETIME',
				'null' => TRUE,

			),
			'DailyAverageMileage' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'LastMileageRecord' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'LastServiceDate' => array(
				'type' => 'DATETIME',
				'null' => TRUE,

			),
			'ExpectedNextServiceDate' => array(
				'type' => 'DATETIME',
				'null' => TRUE,

			),
			'NextServiceMileage' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'NextService' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'RegistrationType' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'FieldAction' => array(
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			),
			'FieldActionName' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'PassiveCustomer' => array(
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			),
			'Blacklisted' => array(
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			),
			'Branch' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'VehicleNo' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'KeyNo' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'Dealer' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'VehicleProduct' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'Customer' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'Partner' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'Leasing' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'SalesRemark' => array(
				'type' => 'LONGTEXT',
				'null' => TRUE,

			),
			'TestDriveUnitStock' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'VehicleOrder' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'Route' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'LatestLocation' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'Insurance' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'ExpectedNextServiceByMilage' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'PeriodicServiceDeadline' => array(
				'type' => 'DATETIME',
				'null' => TRUE,

			),
			'CalculatedService' => array(
				'type' => 'DATETIME',
				'null' => TRUE,

			),
			'OptimisticLockField' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'GCRecord' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'ServiceRemark' => array(
				'type' => 'LONGTEXT',
				'null' => TRUE,

			),
			'PolisNo' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
		));
		$this->forge->addPrimaryKey("Oid",true);
		$this->forge->addKey('GCRecord');
		$this->forge->addKey('ActiveServiceBooking');
		$this->forge->addKey('DefaultContact');
		$this->forge->addKey('NextService');
		$this->forge->addKey('FieldActionName');
		$this->forge->addKey('Branch');
		$this->forge->addKey('Dealer');
		$this->forge->addKey('VehicleProduct');
		$this->forge->addKey('Customer');
		$this->forge->addKey('Partner');
		$this->forge->addKey('Leasing');
		$this->forge->addKey('TestDriveUnitStock');
		$this->forge->addKey('VehicleOrder');
		$this->forge->addKey('Route');
		$this->forge->addKey('LatestLocation');
		$this->forge->addKey('Insurance');
		$this->forge->createTable("sales_vehicleunit", false,['ENGINE' => 'InnoDB'] );

	 }

	public function down()	{
		### Drop table sales_vehicleunit ##
		$this->forge->dropTable("sales_vehicleunit", TRUE);

	}
}