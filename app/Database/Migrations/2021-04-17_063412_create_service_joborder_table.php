<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Createservice_joborderTable extends Migration {

	public function up() {

		## Create Table service_joborder
		$this->forge->addField(array(
			'WorkOrder' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'Job' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'CustomJobDescription' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'IsReturnJob' => array(
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			),
			'SPKApproved' => array(
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			),
			'FinalInspectionUnSolvedJob' => array(
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			),
			'FinalInspectionJobSolvedBy' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'Technician' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'Stall' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'JobServiceRate' => array(
				'type' => 'DOUBLE',
				'null' => TRUE,

			),
			'ServiceUnitPrice' => array(
				'type' => 'DECIMAL',
				'constraint' => 19,4,
				'null' => TRUE,

			),
			'WithholdingTax' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'VATCode' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'Program' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'ProgramPercentage' => array(
				'type' => 'DOUBLE',
				'null' => TRUE,

			),
			'ProgramAmount' => array(
				'type' => 'DECIMAL',
				'constraint' => 19,4,
				'null' => TRUE,

			),
			'DiscountApproved' => array(
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			),
			'Discount' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'DiscountPercentage' => array(
				'type' => 'DOUBLE',
				'null' => TRUE,

			),
			'Insurance' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'PartnerStatus' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'FinishTime' => array(
				'type' => 'DATETIME',
				'null' => TRUE,

			),
			'EstimatedFinishTime' => array(
				'type' => 'DATETIME',
				'null' => TRUE,

			),
			'ServiceStartOn' => array(
				'type' => 'DATETIME',
				'null' => TRUE,

			),
			'ActualServiceRate' => array(
				'type' => 'DOUBLE',
				'null' => TRUE,

			),
			'RequirePO' => array(
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			),
			'JobStatus' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'IsActive' => array(
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			),
			'SubletInvoiceCreated' => array(
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			),
			'Billed' => array(
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			),
			'ProgramBilled' => array(
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			),
			'AllDay' => array(
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			),
			'EndOn' => array(
				'type' => 'DATETIME',
				'null' => TRUE,

			),
			'Label' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'Location' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'Status' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'Type' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'OptimisticLockField' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'GCRecord' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'TotalJobReDo' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'ServiceInvoice' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'IsJobReDo' => array(
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			),
			'JobReDoCount' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'JobOrderId' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => FALSE,

			),
			'JobInsuranceAmount' => array(
				'type' => 'DECIMAL',
				'constraint' => 19,4,
				'null' => TRUE,

			),
			'ExcludeInLeadTimeCalc' => array(
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			),
		));
		$this->forge->addPrimaryKey("JobOrderId",true);
		$this->forge->createTable("service_joborder", false,['ENGINE' => 'InnoDB'] );

	 }

	public function down()	{
		### Drop table service_joborder ##
		$this->forge->dropTable("service_joborder", TRUE);

	}
}