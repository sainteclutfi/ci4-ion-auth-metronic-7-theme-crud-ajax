<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Createservice_receptionqueueTable extends Migration {

	public function up() {

		## Create Table service_receptionqueue
		$this->forge->addField(array(
			'Oid' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => FALSE,

			),
			'NewCustomer' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'VehicleUnit' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'QueueNo' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'IsBooking' => array(
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			),
			'ClockOn' => array(
				'type' => 'DATETIME',
				'null' => TRUE,

			),
			'ClockOff' => array(
				'type' => 'DATETIME',
				'null' => TRUE,

			),
			'ArrivalTime' => array(
				'type' => 'DATETIME',
				'null' => TRUE,

			),
			'Branch' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'ServiceAdvisor' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'BookingOnTimeStatus' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'Status' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'InvalidData' => array(
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			),
			'VisitReason' => array(
				'type' => 'VARCHAR',
				'constraint' => 3,
				'null' => TRUE,

			),
			'Note' => array(
				'type' => 'LONGTEXT',
				'null' => TRUE,

			),
			'Desk' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'InCall' => array(
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			),
			'OptimisticLockField' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'GCRecord' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
		));
		$this->forge->addPrimaryKey("Oid",true);
		$this->forge->addKey('GCRecord');
		$this->forge->addKey('VehicleUnit');
		$this->forge->addKey('Branch');
		$this->forge->addKey('ServiceAdvisor');
		$this->forge->addKey('VisitReason');
		$this->forge->addKey('Desk');		
		$this->forge->createTable("service_receptionqueue", false,['ENGINE' => 'InnoDB'] );

	 }

	public function down()	{
		### Drop table service_receptionqueue ##
		$this->forge->dropTable("service_receptionqueue", TRUE);

	}
}