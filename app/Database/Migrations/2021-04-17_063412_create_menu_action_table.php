<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Createmenu_actionTable extends Migration {

	public function up() {

		## Create Table menu_action
		$this->forge->addField(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 20,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'name' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null' => TRUE,

			),
			'icon' => array(
				'type' => 'VARCHAR',
				'constraint' => 50,
				'null' => TRUE,

			),
			'position' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'menu_id' => array(
				'type' => 'INT',
				'constraint' => 20,
				'null' => TRUE,

			),
			'created_at' => array(
				'type' => 'DATETIME',
				'null' => TRUE,

			),
			'updated_at' => array(
				'type' => 'DATETIME',
				'null' => TRUE,

			),
			'deleted_at' => array(
				'type' => 'DATETIME',
				'null' => TRUE,

			),
		));
		$this->forge->addPrimaryKey("id",true);
		$this->forge->createTable("menu_action", false,['ENGINE' => 'InnoDB'] );

	 }

	public function down()	{
		### Drop table menu_action ##
		$this->forge->dropTable("menu_action", TRUE);

	}
}