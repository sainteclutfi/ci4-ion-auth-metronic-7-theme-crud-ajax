<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Createhr_genderTable extends Migration {

	public function up() {

		## Create Table hr_gender
		$this->forge->addField(array(
			'Oid' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => FALSE,

			),
			'Description' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'OptimisticLockField' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'GCRecord' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
		));
		$this->forge->addPrimaryKey("Oid",true);
		$this->forge->createTable("hr_gender", false,['ENGINE' => 'InnoDB'] );

	 }

	public function down()	{
		### Drop table hr_gender ##
		$this->forge->dropTable("hr_gender", TRUE);

	}
}