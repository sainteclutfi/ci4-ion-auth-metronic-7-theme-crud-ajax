<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Createservice_repairtypeTable extends Migration {

	public function up() {

		## Create Table service_repairtype
		$this->forge->addField(array(
			'RepairTypeId' => array(
				'type' => 'BIGINT',
				'constraint' => 20,
				'null' => FALSE,

			),
			'RepairTypeDescription' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'Branch' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'OptimisticLockField' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'GCRecord' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
		));
		$this->forge->addPrimaryKey("RepairTypeId",true);
		$this->forge->createTable("service_repairtype", false,['ENGINE' => 'InnoDB'] );

	 }

	public function down()	{
		### Drop table service_repairtype ##
		$this->forge->dropTable("service_repairtype", TRUE);

	}
}