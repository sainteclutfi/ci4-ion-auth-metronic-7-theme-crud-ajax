<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Createcommon_provinceTable extends Migration {

	public function up() {

		## Create Table common_province
		$this->forge->addField(array(
			'Oid' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'ProvinceName' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'Country' => array(
				'type' => 'VARCHAR',
				'constraint' => 3,
				'null' => TRUE,

			),
			'OptimisticLockField' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'GCRecord' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'rowguid' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => FALSE,

			),
		));
		$this->forge->addPrimaryKey("Oid",true);
		$this->forge->createTable("common_province", false,['ENGINE' => 'InnoDB'] );

	 }

	public function down()	{
		### Drop table common_province ##
		$this->forge->dropTable("common_province", TRUE);

	}
}