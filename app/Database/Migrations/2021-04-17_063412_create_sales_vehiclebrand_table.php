<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Createsales_vehiclebrandTable extends Migration {

	public function up() {

		## Create Table sales_vehiclebrand
		$this->forge->addField(array(
			'Oid' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => FALSE,

			),
			'BrandName' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'GCRecord' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
		));
		$this->forge->addPrimaryKey("Oid",true);
		$this->forge->createTable("sales_vehiclebrand", false,['ENGINE' => 'InnoDB'] );

	 }

	public function down()	{
		### Drop table sales_vehiclebrand ##
		$this->forge->dropTable("sales_vehiclebrand", TRUE);

	}
}