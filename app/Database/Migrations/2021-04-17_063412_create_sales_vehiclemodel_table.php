<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Createsales_vehiclemodelTable extends Migration {

	public function up() {

		## Create Table sales_vehiclemodel
		$this->forge->addField(array(
			'Oid' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => FALSE,

			),
			'VehicleModelName' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'Photo' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'SalesPoint' => array(
				'type' => 'DOUBLE',
				'null' => TRUE,

			),
			'Brand' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'Design' => array(
				'type' => 'VARCHAR',
				'constraint' => 50,
				'null' => TRUE,

			),
			'Category' => array(
				'type' => 'VARCHAR',
				'constraint' => 50,
				'null' => TRUE,

			),
			'Sub_Category' => array(
				'type' => 'VARCHAR',
				'constraint' => 50,
				'null' => TRUE,

			),
			'Price' => array(
				'type' => 'VARCHAR',
				'constraint' => 50,
				'null' => TRUE,

			),
			'Sub_Price' => array(
				'type' => 'VARCHAR',
				'constraint' => 50,
				'null' => TRUE,

			),
			'Discontinue' => array(
				'type' => 'TINYINT',
				'constraint' => 1,
				'null' => TRUE,

			),
			'GCRecord' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
		));
		$this->forge->addPrimaryKey("Oid",true);
		$this->forge->addUniqueKey('VehicleModelName');
		$this->forge->createTable("sales_vehiclemodel", false,['ENGINE' => 'InnoDB'] );

	 }

	public function down()	{
		### Drop table sales_vehiclemodel ##
		$this->forge->dropTable("sales_vehiclemodel", TRUE);

	}
}