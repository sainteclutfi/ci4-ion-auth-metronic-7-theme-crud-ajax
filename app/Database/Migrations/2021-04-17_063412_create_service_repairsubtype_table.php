<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Createservice_repairsubtypeTable extends Migration {

	public function up() {

		## Create Table service_repairsubtype
		$this->forge->addField(array(
			'Oid' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'Description' => array(
				'type' => 'VARCHAR',
				'constraint' => 30,
				'null' => TRUE,

			),
			'RepairType' => array(
				'type' => 'BIGINT',
				'constraint' => 20,
				'null' => TRUE,

			),
			'JobType' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'OptimisticLockField' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'GCRecord' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
		));
		$this->forge->addPrimaryKey("Oid",true);
		$this->forge->createTable("service_repairsubtype", false,['ENGINE' => 'InnoDB'] );

	 }

	public function down()	{
		### Drop table service_repairsubtype ##
		$this->forge->dropTable("service_repairsubtype", TRUE);

	}
}