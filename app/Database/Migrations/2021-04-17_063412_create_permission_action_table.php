<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Createpermission_actionTable extends Migration {

	public function up() {

		## Create Table permission_action
		$this->forge->addField(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 20,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'permission_id' => array(
				'type' => 'INT',
				'constraint' => 20,
				'null' => FALSE,

			),
			'menu_action_id' => array(
				'type' => 'INT',
				'constraint' => 20,
				'null' => TRUE,

			),
			'group_id' => array(
				'type' => 'INT',
				'constraint' => 20,
				'null' => TRUE,

			),
		));
		$this->forge->addPrimaryKey("id",true);
		$this->forge->createTable("permission_action", false,['ENGINE' => 'InnoDB'] );

	 }

	public function down()	{
		### Drop table permission_action ##
		$this->forge->dropTable("permission_action", TRUE);

	}
}