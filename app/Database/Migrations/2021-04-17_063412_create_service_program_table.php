<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Createservice_programTable extends Migration {

	public function up() {

		## Create Table service_program
		$this->forge->addField(array(
			'Oid' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => FALSE,

			),
			'ProgramName' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'ValidFrom' => array(
				'type' => 'DATETIME',
				'null' => TRUE,

			),
			'ValidThru' => array(
				'type' => 'DATETIME',
				'null' => TRUE,

			),
			'DiscountType' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'ProgramValueType' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'PercentageValue' => array(
				'type' => 'DOUBLE',
				'null' => TRUE,

			),
			'AmountValue' => array(
				'type' => 'DECIMAL',
				'constraint' => 19,4,
				'null' => TRUE,

			),
			'CreditLimit' => array(
				'type' => 'DECIMAL',
				'constraint' => 19,4,
				'null' => TRUE,

			),
			'PartnerStatus' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'ApplyToAllCustomer' => array(
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			),
			'AttachedDocument' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'Partner' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'DiscountForPartner' => array(
				'type' => 'DOUBLE',
				'null' => TRUE,

			),
			'OverridePriceFromLandedCost' => array(
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			),
			'TermOfPayment' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'ReceivableAccount' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'Invoiceable' => array(
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			),
			'OptimisticLockField' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'GCRecord' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'Branch' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
		));
		$this->forge->addPrimaryKey("Oid",true);
		$this->forge->createTable("service_program", false,['ENGINE' => 'InnoDB'] );

	 }

	public function down()	{
		### Drop table service_program ##
		$this->forge->dropTable("service_program", TRUE);

	}
}