<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Createusers_usage_rateTable extends Migration {

	public function up() {

		## Create Table users_usage_rate
		$this->forge->addField(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 250,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'ip' => array(
				'type' => 'VARCHAR',
				'constraint' => 250,
				'null' => FALSE,

			),
			'perangkat' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null' => TRUE,

			),
			'browser' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null' => TRUE,

			),
			'pengunjung' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null' => TRUE,

			),
			'user_id' => array(
				'type' => 'INT',
				'constraint' => 250,
				'null' => TRUE,

			),
			'`update_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP',
			'create_at' => array(
				'type' => 'DATETIME',
				'null' => TRUE,

			),
		));
		$this->forge->addPrimaryKey("id",true);
		$this->forge->createTable("users_usage_rate", false,['ENGINE' => 'InnoDB'] );

	 }

	public function down()	{
		### Drop table users_usage_rate ##
		$this->forge->dropTable("users_usage_rate", TRUE);

	}
}