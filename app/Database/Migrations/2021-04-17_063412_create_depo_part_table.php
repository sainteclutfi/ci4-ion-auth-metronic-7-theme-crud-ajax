<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Createdepo_partTable extends Migration {

	public function up() {

		## Create Table depo_part
		$this->forge->addField(array(
			'Oid' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => FALSE,

			),
			'PartNo' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'PartCode' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'PartName' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'CostingMethod' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'RegisteredDate' => array(
				'type' => 'DATETIME',
				'null' => TRUE,

			),
			'SerialNoRequired' => array(
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			),
			'PartGroup' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'OldPart' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'NewPart' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'Discontinue' => array(
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			),
			'StopSalesCode' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'PartFlag' => array(
				'type' => 'VARCHAR',
				'constraint' => 2,
				'null' => TRUE,

			),
			'Length' => array(
				'type' => 'DOUBLE',
				'null' => TRUE,

			),
			'Width' => array(
				'type' => 'DOUBLE',
				'null' => TRUE,

			),
			'Height' => array(
				'type' => 'DOUBLE',
				'null' => TRUE,

			),
			'Weight' => array(
				'type' => 'DOUBLE',
				'null' => TRUE,

			),
			'IsPWC' => array(
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			),
			'MaximumSupply' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'VehicleModel' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'UnitPrice' => array(
				'type' => 'DECIMAL',
				'constraint' => 19,4,
				'null' => TRUE,

			),
			'RetailPrice' => array(
				'type' => 'DECIMAL',
				'constraint' => 19,4,
				'null' => TRUE,

			),
			'QuantityPerSet' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'Unit' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'Program' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'WarrantyPeriod' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'MileageWarranty' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'OptimisticLockField' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'GCRecord' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'VehicleFullModel' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'MarginDiscount' => array(
				'type' => 'DECIMAL',
				'constraint' => 19,4,
				'null' => TRUE,

			),
			'MaxRetailDiscount' => array(
				'type' => 'DECIMAL',
				'constraint' => 19,4,
				'null' => TRUE,

			),
			'ModelCode' => array(
				'type' => 'LONGTEXT',
				'null' => TRUE,

			),
			'LandedCost' => array(
				'type' => 'DECIMAL',
				'constraint' => 19,4,
				'null' => TRUE,

			),
			'PriceList' => array(
				'type' => 'DECIMAL',
				'constraint' => 19,4,
				'null' => TRUE,

			),
			'AutoUpdatePrice' => array(
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			),
		));
		$this->forge->addPrimaryKey("Oid",true);
		$this->forge->createTable("depo_part", false,['ENGINE' => 'InnoDB'] );

	 }

	public function down()	{
		### Drop table depo_part ##
		$this->forge->dropTable("depo_part", TRUE);

	}
}