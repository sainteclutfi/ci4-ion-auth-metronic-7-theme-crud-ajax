<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Createsales_vehiclefullmodelTable extends Migration {

	public function up() {

		## Create Table sales_vehiclefullmodel
		$this->forge->addField(array(
			'VehicleFullModelId' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => FALSE,

			),
			'ToyotaFullModelId' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'FullModelId' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'VehicleFullModelName' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'VehicleModel' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'ReleasedYear' => array(
				'type' => 'DATETIME',
				'null' => TRUE,

			),
			'FuelType' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'isDifferentPriceByColor' => array(
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			),
			'CylinderVolume' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'Discontinue' => array(
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			),
			'SegmentDesignBase' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'SegmentCategory' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'SegmentPrice' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'SegmentPurpose' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'Brand' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'ProductName' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'Color' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'Picture' => array(
				'type' => 'LONGBLOB',
				'null' => TRUE,

			),
			'LargePicture' => array(
				'type' => 'LONGTEXT',
				'null' => TRUE,

			),
			'OptimisticLockField' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'GCRecord' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
		));
		$this->forge->addPrimaryKey("VehicleFullModelId",true);
		$this->forge->addKey('GCRecord');
		$this->forge->addKey('VehicleModel');
		$this->forge->addKey('FuelType');
		$this->forge->addKey('CylinderVolume');
		$this->forge->addKey('SegmentDesignBase');
		$this->forge->addKey('SegmentCategory');
		$this->forge->addKey('SegmentPrice');
		$this->forge->addKey('SegmentPurpose');
		$this->forge->addKey('Brand');
		$this->forge->addKey('Color');
		$this->forge->createTable("sales_vehiclefullmodel", false,['ENGINE' => 'InnoDB'] );

	 }

	public function down()	{
		### Drop table sales_vehiclefullmodel ##
		$this->forge->dropTable("sales_vehiclefullmodel", TRUE);

	}
}