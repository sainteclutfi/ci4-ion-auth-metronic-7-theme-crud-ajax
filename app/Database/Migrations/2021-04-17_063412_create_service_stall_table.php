<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Createservice_stallTable extends Migration {

	public function up() {

		## Create Table service_stall
		$this->forge->addField(array(
			'Color' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'Oid' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => FALSE,

			),
			'StallName' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'Description' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'Branch' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'CarryOverStall' => array(
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			),
			'ExtendedStatusForBodyPaintRepair' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'RepairType' => array(
				'type' => 'BIGINT',
				'constraint' => 20,
				'null' => TRUE,

			),
			'Caption' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'OptimisticLockField' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'GCRecord' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'BPAction' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'BPEstimation' => array(
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			),
		));
		$this->forge->addPrimaryKey("Oid",true);
		$this->forge->addKey('Branch');
		$this->forge->addKey('RepairType');
		$this->forge->addKey('BPAction');
		$this->forge->createTable("service_stall", false,['ENGINE' => 'InnoDB'] );

	 }

	public function down()	{
		### Drop table service_stall ##
		$this->forge->dropTable("service_stall", TRUE);

	}
}