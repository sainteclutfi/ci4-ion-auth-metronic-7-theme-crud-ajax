<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Createhr_unitgroupTable extends Migration {

	public function up() {

		## Create Table hr_unitgroup
		$this->forge->addField(array(
			'Oid' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'UnitGroupId' => array(
				'type' => 'VARCHAR',
				'constraint' => 4,
				'null' => TRUE,

			),
			'UnitGroupName' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'OptimisticLockField' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'GCRecord' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
		));
		$this->forge->addPrimaryKey("Oid",true);
		$this->forge->createTable("hr_unitgroup", false,['ENGINE' => 'InnoDB'] );

	 }

	public function down()	{
		### Drop table hr_unitgroup ##
		$this->forge->dropTable("hr_unitgroup", TRUE);

	}
}