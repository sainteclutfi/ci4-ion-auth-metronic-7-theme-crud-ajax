<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Createsales_vehicletypeTable extends Migration {

	public function up() {

		## Create Table sales_vehicletype
		$this->forge->addField(array(
			'Oid' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => FALSE,

			),
			'UnitTypeId' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'TypeIdTam' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'UnitTypeName' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'UnitModel' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'ReleasedYear' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'UnitColorId' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'AFIUnitType' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'AFIUnitModel' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'Discontinue' => array(
				'type' => 'TINYINT',
				'constraint' => 1,
				'null' => TRUE,

			),
			'GCRecord' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
		));
		$this->forge->addPrimaryKey("Oid",true);
		$this->forge->addKey('UnitModel');
  		$this->forge->addKey('Discontinue');
		$this->forge->createTable("sales_vehicletype", false,['ENGINE' => 'InnoDB'] );

	 }

	public function down()	{
		### Drop table sales_vehicletype ##
		$this->forge->dropTable("sales_vehicletype", TRUE);

	}
}