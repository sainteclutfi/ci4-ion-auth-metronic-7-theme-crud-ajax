<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Createservice_customerrequestTable extends Migration {

	public function up() {

		## Create Table service_customerrequest
		$this->forge->addField(array(
			'Oid' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => FALSE,

			),
			'RequestJob' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'DiagnoseResult' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'WorkOrder' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'OptimisticLockField' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'GCRecord' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
		));
		$this->forge->addPrimaryKey("Oid",true);
		$this->forge->createTable("service_customerrequest", false,['ENGINE' => 'InnoDB'] );

	 }

	public function down()	{
		### Drop table service_customerrequest ##
		$this->forge->dropTable("service_customerrequest", TRUE);

	}
}