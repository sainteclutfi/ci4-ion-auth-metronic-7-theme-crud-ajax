<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Createcommon_customerTable extends Migration {

	public function up() {

		## Create Table common_customer
		$this->forge->addField(array(
			'Oid' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => FALSE,

			),
			'Salutation' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'FirstName' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'LastName' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'CustomerId' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'TaxRegistrationNo' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'Branch' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'CustomerAddress' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'OfficeAddress' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
			'PhoneNo' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'MobileNo' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'DateOfBirth' => array(
				'type' => 'DATE',
				'null' => TRUE,

			),
			'PlaceOfBirth' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'IdentificationType' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'IdentificationNo' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'Religion' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'ContactSource' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'Occupation' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'CustomerType' => array(
				'type' => 'VARCHAR',
				'constraint' => 50,
				'null' => TRUE,

			),
			'GCRecord' => array(
				'type' => 'VARCHAR',
				'constraint' => 11,
				'null' => TRUE,

			),
			'Gender' => array(
				'type' => 'VARCHAR',
				'constraint' => 40,
				'null' => TRUE,

			),
			'RetailFleet' => array(
				'type' => 'ENUM("0","1")',
				'null' => TRUE,

			),
			'Email' => array(
				'type' => 'VARCHAR',
				'constraint' => 50,
				'null' => TRUE,

			),
			'Country' => array(
				'type' => 'CHAR',
				'constraint' => 5,
				'null' => TRUE,

			),
			'Province' => array(
				'type' => 'INT',
				'constraint' => 10,
				'null' => TRUE,

			),
			'City' => array(
				'type' => 'INT',
				'constraint' => 10,
				'null' => TRUE,

			),
			'District' => array(
				'type' => 'INT',
				'constraint' => 10,
				'null' => TRUE,

			),
			'SubDistrict' => array(
				'type' => 'INT',
				'constraint' => 10,
				'null' => TRUE,

			),
			'PostalCode' => array(
				'type' => 'CHAR',
				'constraint' => 10,
				'null' => TRUE,

			),
			'RT' => array(
				'type' => 'CHAR',
				'constraint' => 4,
				'null' => TRUE,

			),
			'RW' => array(
				'type' => 'CHAR',
				'constraint' => 4,
				'null' => TRUE,

			),
			'SIMCardNo' => array(
				'type' => 'VARCHAR',
				'constraint' => 50,
				'null' => TRUE,

			),
			'MarriedStatus' => array(
				'type' => 'CHAR',
				'constraint' => 3,
				'null' => TRUE,

			),
			'IncomePerMonth' => array(
				'type' => 'DECIMAL',
				'constraint' => 11,0,
				'null' => TRUE,

			),
			'ExpensesPerMonth' => array(
				'type' => 'DECIMAL',
				'constraint' => 11,0,
				'null' => TRUE,

			),
			'Hobby' => array(
				'type' => 'VARCHAR',
				'constraint' => 40,
				'null' => TRUE,

			),
			'Referensi' => array(
				'type' => 'ENUM("0","1")',
				'null' => TRUE,

			),
			'FavoriteFood' => array(
				'type' => 'VARCHAR',
				'constraint' => 40,
				'null' => TRUE,

			),
			'FavoriteDrink' => array(
				'type' => 'VARCHAR',
				'constraint' => 40,
				'null' => TRUE,

			),
			'BusinessFields' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'RegisteredDate' => array(
				'type' => 'DATETIME',
				'null' => TRUE,

			),
			'VehiclePhoneNo' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'VehicleRelationship' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'VehicleUsers' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'InActive' => array(
				'type' => 'TINYINT',
				'constraint' => 1,
				'null' => TRUE,

			),
			'ReasonActivation' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null' => TRUE,

			),
			'AppInActive' => array(
				'type' => 'TINYINT',
				'constraint' => 2,
				'null' => TRUE,

			),
		));
		$this->forge->addPrimaryKey("Oid",true);
		$this->forge->addUniqueKey('IdentificationNo');
		$this->forge->createTable("common_customer", false,['ENGINE' => 'InnoDB'] );

	 }

	public function down()	{
		### Drop table common_customer ##
		$this->forge->dropTable("common_customer", TRUE);

	}
}