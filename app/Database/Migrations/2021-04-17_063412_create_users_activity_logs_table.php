<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Createusers_activity_logsTable extends Migration {

	public function up() {

		## Create Table users_activity_logs
		$this->forge->addField(array(
			'activity_id' => array(
				'type' => 'INT',
				'constraint' => 1,
				'unsigned' => TRUE,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'user_id' => array(
				'type' => 'INT',
				'constraint' => 1,
				'unsigned' => TRUE,
				'null' => FALSE,

			),
			'activity_type' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => FALSE,

			),
			'activity_parameter' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'activity_time' => array(
				'type' => 'DATETIME',
				'null' => FALSE,

			),
			'activity_ip_address' => array(
				'type' => 'VARCHAR',
				'constraint' => 15,
				'null' => TRUE,

			),
			'activity_browser' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null' => TRUE,

			),
			'deleted' => array(
				'type' => 'TINYINT',
				'constraint' => 1,
				'null' => FALSE,
				'default' => '0',

			),
			'url' => array(
				'type' => 'TEXT',
				'null' => TRUE,

			),
		));
		$this->forge->addPrimaryKey("activity_id",true);
		$this->forge->createTable("users_activity_logs", false,['ENGINE' => 'InnoDB'] );

	 }

	public function down()	{
		### Drop table users_activity_logs ##
		$this->forge->dropTable("users_activity_logs", TRUE);

	}
}