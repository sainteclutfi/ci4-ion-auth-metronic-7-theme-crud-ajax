<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Createcommon_countryTable extends Migration {

	public function up() {

		## Create Table common_country
		$this->forge->addField(array(
			'Oid' => array(
				'type' => 'VARCHAR',
				'constraint' => 3,
				'null' => FALSE,

			),
			'CountryName' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'PhoneCode' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'OptimisticLockField' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'GCRecord' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'rowguid' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => FALSE,

			),
		));
		$this->forge->addPrimaryKey("Oid",true);
		$this->forge->createTable("common_country", false,['ENGINE' => 'InnoDB'] );

	 }

	public function down()	{
		### Drop table common_country ##
		$this->forge->dropTable("common_country", TRUE);

	}
}