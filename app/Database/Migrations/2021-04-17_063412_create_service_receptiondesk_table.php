<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Createservice_receptiondeskTable extends Migration {

	public function up() {

		## Create Table service_receptiondesk
		$this->forge->addField(array(
			'Oid' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'CurrentQueue' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'DeskName' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'Branch' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'OptimisticLockField' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'GCRecord' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
		));
		$this->forge->addPrimaryKey("Oid",true);
		$this->forge->createTable("service_receptiondesk", false,['ENGINE' => 'InnoDB'] );

	 }

	public function down()	{
		### Drop table service_receptiondesk ##
		$this->forge->dropTable("service_receptiondesk", TRUE);

	}
}