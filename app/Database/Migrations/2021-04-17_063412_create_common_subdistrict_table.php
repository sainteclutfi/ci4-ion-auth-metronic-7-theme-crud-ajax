<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Createcommon_subdistrictTable extends Migration {

	public function up() {

		## Create Table common_subdistrict
		$this->forge->addField(array(
			'Oid' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'SubDistrictName' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'PostalCode' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'District' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'OptimisticLockField' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'GCRecord' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'rowguid' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
		));
		$this->forge->addPrimaryKey("Oid",true);
		$this->forge->createTable("common_subdistrict", false,['ENGINE' => 'InnoDB'] );

	 }

	public function down()	{
		### Drop table common_subdistrict ##
		$this->forge->dropTable("common_subdistrict", TRUE);

	}
}