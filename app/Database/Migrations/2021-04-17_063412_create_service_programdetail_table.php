<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Createservice_programdetailTable extends Migration {

	public function up() {

		## Create Table service_programdetail
		$this->forge->addField(array(
			'Oid' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => FALSE,

			),
			'ValidFrom' => array(
				'type' => 'DATETIME',
				'null' => TRUE,

			),
			'ValidThru' => array(
				'type' => 'DATETIME',
				'null' => TRUE,

			),
			'Notes' => array(
				'type' => 'LONGTEXT',
				'null' => TRUE,

			),
			'Program' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'VehicleUnit' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'OptimisticLockField' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'GCRecord' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
		));
		$this->forge->addPrimaryKey("Oid",true);
		$this->forge->createTable("service_programdetail", false,['ENGINE' => 'InnoDB'] );

	 }

	public function down()	{
		### Drop table service_programdetail ##
		$this->forge->dropTable("service_programdetail", TRUE);

	}
}