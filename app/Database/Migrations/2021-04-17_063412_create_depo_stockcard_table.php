<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Createdepo_stockcardTable extends Migration {

	public function up() {

		## Create Table depo_stockcard
		$this->forge->addField(array(
			'Oid' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => FALSE,

			),
			'IdentificationNo' => array(
				'type' => 'VARCHAR',
				'constraint' => 200,
				'null' => TRUE,

			),
			'ReferenceType' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'ReferenceId' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'ReferenceNo' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'TransactionDate' => array(
				'type' => 'DATETIME',
				'null' => TRUE,

			),
			'Part' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'Warehouse' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'Branch' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'InQuantity' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'InUnitPrice' => array(
				'type' => 'DECIMAL',
				'constraint' => 19,4,
				'null' => TRUE,

			),
			'OutQuantity' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'OutUnitPrice' => array(
				'type' => 'DECIMAL',
				'constraint' => 19,4,
				'null' => TRUE,

			),
			'BalanceQuantity' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'BalanceStockValue' => array(
				'type' => 'DECIMAL',
				'constraint' => 19,4,
				'null' => TRUE,

			),
			'PreviousStockCard' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'IsActive' => array(
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			),
			'Marker' => array(
				'type' => 'DATETIME',
				'null' => TRUE,

			),
			'OptimisticLockField' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'GCRecord' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'Remark' => array(
				'type' => 'LONGTEXT',
				'null' => TRUE,

			),
		));
		$this->forge->addPrimaryKey("Oid",true);
		$this->forge->createTable("depo_stockcard", false,['ENGINE' => 'InnoDB'] );

	 }

	public function down()	{
		### Drop table depo_stockcard ##
		$this->forge->dropTable("depo_stockcard", TRUE);

	}
}