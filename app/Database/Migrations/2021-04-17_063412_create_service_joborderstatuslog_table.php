<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Createservice_joborderstatuslogTable extends Migration {

	public function up() {

		## Create Table service_joborderstatuslog
		$this->forge->addField(array(
			'Oid' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => FALSE,

			),
			'Job' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'JobStatus' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'Paused' => array(
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			),
			'ClockOn' => array(
				'type' => 'DATETIME',
				'null' => TRUE,

			),
			'ClockOff' => array(
				'type' => 'DATETIME',
				'null' => TRUE,

			),
			'Remark' => array(
				'type' => 'LONGTEXT',
				'null' => TRUE,

			),
			'ClockOnBy' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'Technician' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'OptimisticLockField' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'GCRecord' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'JobOrder' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'BPJobAction' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'BPAction' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
		));
		$this->forge->addPrimaryKey("Oid",true);
		$this->forge->createTable("service_joborderstatuslog", false,['ENGINE' => 'InnoDB'] );

	 }

	public function down()	{
		### Drop table service_joborderstatuslog ##
		$this->forge->dropTable("service_joborderstatuslog", TRUE);

	}
}