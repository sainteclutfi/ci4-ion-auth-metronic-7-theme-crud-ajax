<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Createsales_vehicleunitcolorTable extends Migration {

	public function up() {

		## Create Table sales_vehicleunitcolor
		$this->forge->addField(array(
			'Oid' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => FALSE,

			),
			'UnitType' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'Color' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'LargePicture' => array(
				'type' => 'LONGTEXT',
				'null' => TRUE,

			),
			'Discontinue' => array(
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			),
			'OptimisticLockField' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'GCRecord' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'InteriorColor' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
		));
		$this->forge->addPrimaryKey("Oid",true);
		$this->forge->addUniqueKey(['Color', 'UnitType']);
		$this->forge->addKey('InteriorColor');
		$this->forge->addKey('UnitType');
		$this->forge->createTable("sales_vehicleunitcolor", false,['ENGINE' => 'InnoDB'] );

	 }

	public function down()	{
		### Drop table sales_vehicleunitcolor ##
		$this->forge->dropTable("sales_vehicleunitcolor", TRUE);

	}
}