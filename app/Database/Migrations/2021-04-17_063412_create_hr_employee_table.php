<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Createhr_employeeTable extends Migration {

	public function up() {

		## Create Table hr_employee
		$this->forge->addField(array(
			'Oid' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => FALSE,

			),
			'EmployeeNo' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'Salutation' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'FirstName' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'MiddleName' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'LastName' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'NickName' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'Initial' => array(
				'type' => 'VARCHAR',
				'constraint' => 3,
				'null' => TRUE,

			),
			'PreTitle' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'PostTitle' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'ClothingSize' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'Height' => array(
				'type' => 'DOUBLE',
				'null' => TRUE,

			),
			'Weight' => array(
				'type' => 'DOUBLE',
				'null' => TRUE,

			),
			'PassportNo' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'IssuedAt' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'ExpiryDate' => array(
				'type' => 'DATETIME',
				'null' => TRUE,

			),
			'IdentificationNo' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'IdentificationExpiryDate' => array(
				'type' => 'DATETIME',
				'null' => TRUE,

			),
			'TaxRegistrationNo' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'PlaceOfBirth' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'DateOfBirth' => array(
				'type' => 'DATETIME',
				'null' => TRUE,

			),
			'Gender' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'Ethnic' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'BloodType' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'MaritalStatus' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'MaritalDate' => array(
				'type' => 'DATETIME',
				'null' => TRUE,

			),
			'NoOfChildren' => array(
				'type' => 'SMALLINT',
				'constraint' => 6,
				'null' => TRUE,

			),
			'StatusOnTax' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'DrivingLicenseNo' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'DrivingLicenseType' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'Hobby' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'Nationality' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'Religion' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'Address1' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'PhoneNo1' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'Address2' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'PhoneNo2' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'MobileNo1' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'MobileNo2' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'EMail1' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'EMail2' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'MotherName' => array(
				'type' => 'VARCHAR',
				'constraint' => 50,
				'null' => TRUE,

			),
			'EmergencyContactName1' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'EmergencyContactRelation1' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'EmergencyAddress1' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'EmergencyPhoneNo1' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'EmergencyContactName2' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'EmergencyContactRelation2' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'EmergencyAddress2' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'EmergencyPhoneNo2' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'JoinedDate' => array(
				'type' => 'DATETIME',
				'null' => TRUE,

			),
			'ConfirmationDate' => array(
				'type' => 'DATETIME',
				'null' => TRUE,

			),
			'EmploymentPeriod' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'ResignDate' => array(
				'type' => 'DATETIME',
				'null' => TRUE,

			),
			'LastPromotionDate' => array(
				'type' => 'DATETIME',
				'null' => TRUE,

			),
			'SalaryPaymentMethod' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'TaxCalculationMode' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'OrganizationalUnit' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'Branch' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'WorkStatus' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'Workgroup' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'Grade' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'NonEmployee' => array(
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			),
			'AutomaticOvertime' => array(
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			),
			'PhoneExt' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'PaymentPeriod' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'EmployeeEducation' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'Qualification' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'GPA' => array(
				'type' => 'VARCHAR',
				'constraint' => 10,
				'null' => TRUE,

			),
			'BankAccountNo' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'AccountOwner' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'Photo' => array(
				'type' => 'LONGBLOB',
				'null' => TRUE,

			),
			'PrivateKey' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'IsActive' => array(
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			),
			'UserName' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'ChangePasswordOnFirstLogon' => array(
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			),
			'StoredPassword' => array(
				'type' => 'LONGTEXT',
				'null' => TRUE,

			),
			'SalesForce' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'Technician' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'Sequence' => array(
				'type' => 'BIGINT',
				'constraint' => 20,
				'null' => TRUE,

			),
			'EnrollNo' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'OptimisticLockField' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'GCRecord' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'BankName' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
			'PersonalGrade' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'Homebase' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
		));
		$this->forge->addPrimaryKey("Oid",true);
		$this->forge->addKey('GCRecord');
		$this->forge->addKey('Salutation');
		$this->forge->addKey('ClothingSize');
		$this->forge->addKey('Gender');
		$this->forge->addKey('Ethnic');
		$this->forge->addKey('BloodType');
		$this->forge->addKey('MaritalStatus');
		$this->forge->addKey('DrivingLicenseType');
		$this->forge->addKey('Nationality');
		$this->forge->addKey('Religion');
		$this->forge->addKey('Address1');
		$this->forge->addKey('Address2');
		$this->forge->addKey('EmergencyAddress1');
		$this->forge->addKey('EmergencyAddress2');
		$this->forge->addKey('SalaryPaymentMethod');
		$this->forge->addKey('OrganizationalUnit');
		$this->forge->addKey('Branch');
		$this->forge->addKey('WorkStatus');
		$this->forge->addKey('Workgroup');
		$this->forge->addKey('Grade');
		$this->forge->addKey('PaymentPeriod');
		$this->forge->addKey('EmployeeEducation');
		$this->forge->addKey('Qualification');
		$this->forge->addKey('BankName');
		$this->forge->addKey('SalesForce');
		$this->forge->addKey('Technician');
		$this->forge->addKey('Homebase');
		$this->forge->createTable("hr_employee", false,['ENGINE' => 'InnoDB'] );

	 }

	public function down()	{
		### Drop table hr_employee ##
		$this->forge->dropTable("hr_employee", TRUE);

	}
}