<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreatepermissionTable extends Migration {

	public function up() {

		## Create Table permission
		$this->forge->addField(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'group_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'menu_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
		));
		$this->forge->addPrimaryKey("id",true);
		$this->forge->createTable("permission", false,['ENGINE' => 'InnoDB'] );

	 }

	public function down()	{
		### Drop table permission ##
		$this->forge->dropTable("permission", TRUE);

	}
}