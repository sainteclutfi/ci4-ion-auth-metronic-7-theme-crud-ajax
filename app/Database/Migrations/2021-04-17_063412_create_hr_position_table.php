<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Createhr_positionTable extends Migration {

	public function up() {

		## Create Table hr_position
		$this->forge->addField(array(
			'Oid' => array(
				'type' => 'INT',
				'constraint' => 36,
				'null' => FALSE,
				'auto_increment' => TRUE
			),
			'Description' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'NoOfNeeds' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'CalculateInMPP' => array(
				'type' => 'TINYINT',
				'constraint' => 4,
				'null' => TRUE,

			),
			'CompetencyPercentage' => array(
				'type' => 'DOUBLE',
				'null' => TRUE,

			),
			'KeyPerformanceIndexPercentage' => array(
				'type' => 'DOUBLE',
				'null' => TRUE,

			),
			'OptimisticLockField' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'GCRecord' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
		));
		$this->forge->addPrimaryKey("Oid",true);
		$this->forge->createTable("hr_position", false,['ENGINE' => 'InnoDB'] );

	 }

	public function down()	{
		### Drop table hr_position ##
		$this->forge->dropTable("hr_position", TRUE);

	}
}