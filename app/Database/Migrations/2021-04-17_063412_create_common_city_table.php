<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Createcommon_cityTable extends Migration {

	public function up() {

		## Create Table common_city
		$this->forge->addField(array(
			'Oid' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,

			),
			'CityType' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'CityName' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => TRUE,

			),
			'Province' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'OptimisticLockField' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'GCRecord' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE,

			),
			'rowguid' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => TRUE,

			),
		));
		$this->forge->addPrimaryKey("Oid",true);
		$this->forge->addKey('GCRecord');
		$this->forge->addKey('CityType');
		$this->forge->addKey('Province');
		$this->forge->createTable("common_city", false,['ENGINE' => 'InnoDB'] );

	 }

	public function down()	{
		### Drop table common_city ##
		$this->forge->dropTable("common_city", TRUE);

	}
}