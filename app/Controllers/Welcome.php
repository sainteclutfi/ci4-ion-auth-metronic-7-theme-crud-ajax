<?php namespace App\Controllers;

class Welcome extends BaseController
{
	public function index()
	{
		if (!$this->isAuthorized()) {
            return redirect()->to('/auth/login');
        }
		return view('welcome');
	}

	//--------------------------------------------------------------------

}
