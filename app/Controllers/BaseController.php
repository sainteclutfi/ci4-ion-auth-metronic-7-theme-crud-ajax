<?php
namespace App\Controllers;

/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 *
 * @package CodeIgniter
 */

use CodeIgniter\Controller;

class BaseController extends Controller
{

	/**
	 * IonAuth library
	 *
	 * @var \IonAuth\Libraries\IonAuth
	 */
	protected $ionAuth;

	/**
	 * An array of helpers to be loaded automatically upon
	 * class instantiation. These helpers will be available
	 * to all other controllers that extend BaseController.
	 *
	 * @var array
	 */
	protected $helpers = [];

	

	/**
	 * Constructor.
	 */
	public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
	{
		// Do Not Edit This Line
		parent::initController($request, $response, $logger);

		//--------------------------------------------------------------------
		// Preload any models, libraries, etc, here.
		//--------------------------------------------------------------------
		// E.g.:
		// $this->session = \Config\Services::session();
		$this->ionAuth = new \IonAuth\Libraries\IonAuth();
		if ($this->ionAuth->loggedIn())
		{
			$this->user = $this->ionAuth->user()->row();
		}

		$this->session = \Config\Services::session();
	}

	/**
	 * Check if user is logged in is admin
	 *
	 * @return boolean
	 */
	protected function isAuthorized(): bool
	{
		return $this->ionAuth->loggedIn() && $this->ionAuth->isAdmin();
	}

	/**
	 * Display the $body page inside the main vue
	 *
	 * @param string $body       Body vue
	 * @param string $pageTitle  Page title
	 * @param string $activeMenu Active menu
	 *
	 * @return string
	 */
	protected function render_view(string $body, string $pageTitle = '', string $activeMenu = ''): string
	{
		$mainData = [
			'appName'       => env('appName', 'CI-Admin'),
			'userFirstName' => $this->user->first_name,
			'userLastName'  => $this->user->last_name,
			'pageTitle'     => $pageTitle,
			'leftMenu'      => $this->displayLeftMenu($this->leftMenu, $activeMenu),
			'body'          => $body,

		];
		return view('Admin\main', $mainData);
	}

}
